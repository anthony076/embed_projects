// FROM: CHIP_PIC\esp8266-lcd\mplab\src\main.c

/* 
  根據data_bit的低四位，設置DB7-DB4的內容，
    - 用於4bit-mode下，透過 DB7-D4 設置IR命令，
      RS RW | DB7 DB6 DB5 DB4 
    - 例如，data_bit = 0x08 = 0b0000_1000，則 D7 = 1，D6=D5=D4=0
    - 注意，此函數只有設置 DB7-DB4 的功能，沒有傳送的功能
*/
void Lcd_SetBit(char data_bit) 
{
    if (data_bit & 1)
    {
        D4 = 1;
    }
    else
    {
        D4 = 0;
    }

    //
    if (data_bit & 2)
    {
        D5 = 1;
    }
    else
    {
        D5 = 0;
    }

    //
    if (data_bit & 4)
    {
        D6 = 1;
    }
    else
    {
        D6 = 0;
    }

    if (data_bit & 8)
    {
        D7 = 1;
    }
    else
    {
        D7 = 0;
    }
}

// 將指令的低四位，寫入 IR 中
void Lcd_Cmd(char a)
{
    // 選擇 IR
    RS = 0;
    // 根據 a 的值，設置 D4-D7 的內容
    Lcd_SetBit(a);

    // EN 接腳致能，使 D4-D7 的內容寫入 LCD 中
    EN = 1;
    __delay_ms(4);

    EN = 0;
}

// 透過 4-bit-mode 傳送 1Byte-data 給 DR
void Lcd_Print_Char(char data)
{
    // 將數據拆分為高4位和低四位
    char Lower_Nibble, Upper_Nibble;

    Lower_Nibble = data & 0x0F;
    Upper_Nibble = data & 0xF0;

    // 選擇 DR
    RS = 1;

    // ==== 先傳送高四位 ====
    // 將高四位傳遞給 DB4-DB7
    Lcd_SetBit(Upper_Nibble >> 4);

    // 開始傳送
    EN = 1;
    __delay_us(40);

    // 關閉傳送
    EN = 0;

    // ==== 再傳送低四位 ====
    // 將低四位傳遞給 DB4-DB7
    Lcd_SetBit(Lower_Nibble);

    // 開始傳送
    EN = 1;
    __delay_us(40);

    // 關閉傳送
    EN = 0;
}

// 傳送字串給DR，連續調用 Lcd_Print_Char()
void Lcd_Print_String(char *a)
{
    int i;
    for (i = 0; a[i] != '\0'; i++)
        Lcd_Print_Char(a[i]); // Split the string using pointers and call the Char function
}

void Lcd_Initial()
{
    // 將 RDx 設置為 output
    TRISD = 0x00;
    
    // 將 PORTD 的內容清除為 0
    PORTD = 0x00;

    // LCD 初始化序列 for 4bit-mode，見 HD44780U.md
    //    step1，delay 55ms
    //    step2，DB5=DB4=1 (0x03) + delay 4.1ms
    //    step3，DB5=DB4=1 (0x03)+ delay 100us
    //    step4，DB5=DB4=1
    //    step5，DB5=1，DB4=0
    //    (已進入4bit-mode，以下指令需發送兩次)
    //    step6-，Function-Set 指令、Entry-Mode 指令、其他客製化指令

    // step1
    __delay_ms(55);

    // step2
    Lcd_Cmd(0x03);
    __delay_ms(5);

    // step3
    Lcd_Cmd(0x03);
    __delay_ms(11);

    // step4、step5
    Lcd_Cmd(0x03);
    Lcd_Cmd(0x02); 

    // (alread in 4bit-mode)

    // step6
    // 0x28 = 0b0010_1000 = 0b001(DL)_(N)(F)**
    //    DB5=1=代表Function-Set 指令
    //    DB4=0=4bit-mode
    //    DB3=N=1=雙行顯示
    //    DB2=F=0=5*8
    Lcd_Cmd(0x02); 
    Lcd_Cmd(0x08);

    // 0x0C = 0b0000_1100 = 0b0000_1(D)(C)(B)
    //    DB3=1=代表DisplayOnOff指令
    //    DB2=D=1=DisplayOn
    //    DB1=C=1=CursorOn
    //    DB0=B=0=BlikingOff
    Lcd_Cmd(0x00);
    Lcd_Cmd(0x0C);
    
    // 0x06 = 0b0000_0110 = 0b0000_01(I/D)(S)
    //    DB2=1=代表EntryMode指令
    //    DB1=I/D=1=Increments
    //    DB0=S=0=ScreenOff=只移動游標，不移動螢幕
    Lcd_Cmd(0x00);
    Lcd_Cmd(0x06);
}

void Lcd_Clear()
{
    // ClearDisplay指令 = 0b0000_0001 = 0x01
    // 4bit-mode 需要發送兩次，第一次發送 0x0，第二次發送 0x1
    Lcd_Cmd(0);
    Lcd_Cmd(1);
}

void Lcd_Set_Cursor(char a, char b)
{
    // 設置 DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    ADD  ADD  ADD  ADD  ADD  ADD  ADD

    // LCD螢幕第1行，對應的起始位址為
    // 0     1     2     3     4     5     6     7
    // 0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07

    // 以移動到LCD螢幕第1行第3個位置(0x03) 為例
    // 使用 設置DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    0    0    0    0    0    1    1       = 0x83

    // LCD螢幕第2行，對應的起始位址為
    // 0     1     2     3     4     5     6     7
    // 0x40  0x41  0x42  0x43  0x44  0x45  0x46  0x47
    
    // 以移動到LCD螢幕第2行第3個位置(0x43) 為例
    // 使用 設置DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    1    0    0    0    0    1    1       = 0xC3

    // temp 為實際的 DDRAM 的位址，z 為地址高四位的值，y為地址低四位的值
    char temp, z, y;

    // 計算實際 DDRAM 的位址
    if (a == 1) // a==1，代表移動到第1行
    {
        temp = 0x80 + b - 1; 
    }
    else if (a == 2) // a==1，代表移動到第2行
    {
        temp = 0xC0 + b - 1;
    }

    z = temp >> 4;      // 將 temp 的高4位，移動到低四位
    y = temp & 0x0F;    // 取得 temp 低四位的值

    Lcd_Cmd(z);  // 傳送地址的高四位值
    Lcd_Cmd(y);  // 傳送地址的低四位值
}

void main()
{
    TRISD = 0x00;

    Initialize_UART();

    Lcd_Initial();
    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("Circuit Digest");

    Lcd_Set_Cursor(2, 1);
    Lcd_Print_String("ESP5266 with PIC");

    __delay_ms(1500);
    Lcd_Clear();

    do
    {
        Lcd_Set_Cursor(1, 1);
        Lcd_Print_String("ESP not found");

    } while (!esp8266_isStarted()); // 阻塞，直到收到 ESP8266 回復的 OK

    // 收到 OK 才會繼續往下執行
    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("ESP is connected");
    __delay_ms(1500);
    Lcd_Clear();

    // 將 esp8266 設置為 softAP 模式
    //      發送 AT+CWMODE=2 的命令給ESP8266
    //      阻塞，直到收到 ESP8266 回復的 OK
    esp8266_mode(2);

    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("ESP set as AP");
    __delay_ms(1500);
    Lcd_Clear();

    // 設置 esp8266 要連線的 wifi
    //      發送 AT+CWJAP="hua","fake" 的命令給ESP8266
    //      阻塞，直到收到 ESP8266 回復的 OK
    esp8266_connect("hua", "fake");

    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("AP configured");
    __delay_ms(1500);

    while (1)
    {
        
    }
}