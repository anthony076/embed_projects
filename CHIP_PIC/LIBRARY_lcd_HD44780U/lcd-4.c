// FROM: CHIP_PIC\i2c-rtc-pcf8563\mplab\i2c.c

// LCD pins
//   LCD_E   RD5
//   LCD_RW  RD6
//   LCD_RS  RD7，select instruction-reg(IR) or data-reg(DR) 
//   LCD_D4  RB4
//   LCD_D5  RB5
//   LCD_D6  RB6
//   LCD_D7  RB7

#include <xc.h>
#include "config.h"
#include "lcd.h"

#define LCD_E RD5
#define LCD_RW RD6
#define LCD_REG RD7
#define IR 0
#define DR 1
#define READ 1
#define WRITE 0

void lcd_init(void)
{
    // 進入 4bit-mode: 0x03 -> 0x03 -> 0x03 -> 0x02
    __delay_ms(15);
    
    lcd_cmd(0x03);      
    __delay_ms(5);
    
    lcd_cmd(0x03);      
    __delay_ms(5);
    
    lcd_cmd(0x03);      
    __delay_ms(5); 
    
    lcd_cmd(0x02);      
    __delay_ms(5);
    
    // 設置 LCD

    // 0x28 = 0b0010_1000 = 0b001(DL)_(N)(F)**
    //    DB5=1=代表Function-Set 指令
    //    DB4=0=4bit-mode
    //    DB3=N=1=雙行顯示
    //    DB2=F=0=5*8
    lcd_cmd(0x28);       // 4 bit interface length. 2 rows enabled.
    __delay_ms(5);

    // 0x10 = 0b0001_0000 = 0b0001_(S/C)(R/L)**
    //    DB4=1=代表 Cursor AND display shift 指令
    //    DB3=SC=1=移動滑鼠，畫面不移動
    //    DB2=RL=1=向左移動
    lcd_cmd(0x10);
    __delay_ms(5);
    
    // 0x0C = 0b0000_1100 = 0b0000_1(D)(C)(B)
    //    DB3=1=代表DisplayOnOff指令
    //    DB2=D=1=DisplayOn     開啟顯示
    //    DB1=C=1=CursorOn      開啟游標
    //    DB0=B=0=BlikingOff    關閉閃爍
    lcd_cmd(0x0c);       // Enable display. Cursor off.
    __delay_ms(5);
    
    // 0x06 = 0b0000_0110 = 0b0000_01(I/D)(S)
    //    DB2=1=代表EntryMode指令
    //    DB1=I/D=1=Increments，向右移動
    //    DB0=S=0=ScreenOff=只移動游標，不移動螢幕
    lcd_cmd(0x06);      // Increment cursor position after each byte 
    __delay_ms(5);
    
    // 清除螢幕
    lcd_cmd(0x01);      // clear display
    __delay_ms(5);
    
}


void enable(void)
{
    LCD_E = 1;
    __delay_ms(5);
    LCD_E = 0;
}

void lcd_cmd(unsigned char c)
{
    LCD_REG = IR;
    LCD_RW = WRITE;
    
    // 設置並傳送高四位
    //   - PORTB & 0x0f，將 PORTB 的 RB7-RB4 設置為0，使 RB7-RB4 的輸出可改變 
    //   - 0xf0 & c，取 c 的高4位，
    //   - OR 運算: 用於設置 PORTB 的 RB7-RB4，因為 RB7-RB4 被設置為0，OR 運算可改變 RB7-RB4 的值
    PORTB = (PORTB & 0x0f) | (0xf0 & c);
    enable();
    
    // 設置並傳送低四位
    //   - PORTB & 0x0f，將 PORTB 的 RB7-RB4 設置為0，使 RB7-RB4 的輸出可改變 
    //   - (0x0f & c) << 4，取 c 的低4位後，將低4位移動到高4位
    //   - OR 運算: 用於設置 PORTB 的 RB7-RB4，因為 RB7-RB4 被設置為0，OR 運算可改變 RB7-RB4 的值
    PORTB = (PORTB & 0x0f) | ((0x0f & c) << 4) ;
    enable();
}

void lcd_data(unsigned char d)
{
    LCD_REG = DR;
    LCD_RW = WRITE;
    
    PORTB = (PORTB & 0x0f) | (0xf0 & d);
    enable();
    
    PORTB = (PORTB & 0x0f) | ((0x0f & d) << 4) ;
    enable();
}
