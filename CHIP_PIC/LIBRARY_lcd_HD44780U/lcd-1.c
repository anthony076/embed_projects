

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    1    DL   N    F    0    0   Function Set
// 0    0    0    0    1    0    1    0    0    0   =   0x28
// 4bit-mode / two-lines / 5*8
#define _SET_LINE_DOT 0x28

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    0    1    D    C    B   Display on/off Controll
// 0    0    0    0    0    0    1    1    0    0   =   0x0c
// display-on / cursor-off / no-blinking
#define _SET_DISPLAY_CONTROL 0x0c

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    0    0    1    I/D  S   Entry Mode Set
// 0    0    0    0    0    0    0    1    1    0   =   0x06
// shift-to-right / move-cursor-only
#define _SET_ENTRY_MODE 0x06

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    1   S/C  R/L   -    -  Cursor/Display Shift
// 0    0    0    0    0    1    1    0    0    0   =   0x18
// cursor + display / shift-to-left
#define _SHIFT_DISPLAY_LEFT 0x18

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    1   S/C  R/L   -    -  Cursor/Display Shift
// 0    0    0    0    0    1    1    1    0    0   =   0x1c
// cursor + display / shift-to-right
#define _SHIFT_DISPLAY_RIGHT 0x1c

#define _CLEAR_DISPLAY 0x01
#define _SELECT_COMMAND_REG 0
#define _SELECT_DATA_REG 1
#define _FIRST_LINE 0x80
#define _SECOND_LINE 0xc0

typedef unsigned char uchar_t;

typedef enum
{
    COMMAND,
    DATA,
} LCD_REGISTER_TYPE;

typedef enum
{
    LEFT,
    RIGHT,
} SHIFT_DIRECTION;

sbit RS at PORTB.B0;
sbit EN at PORTB.B1;
sbit D4 at PORTB.B4;
sbit D5 at PORTB.B5;
sbit D6 at PORTB.B6;
sbit D7 at PORTB.B7;



int i;

// ======================
// Send 4byte data into Instruction-Reg or Display-Data-Reg
// @param chunk: 4 bytes of data to send to lcd
// @param location: the register that data will be sent to
// ======================
static void _write_4byte(uchar_t chunk, LCD_REGISTER_TYPE location)
{

    if (location == COMMAND)
    {
        RS = _SELECT_COMMAND_REG;
    }
    else
    {
        RS = _SELECT_DATA_REG;
    }

    // Write Data
    D4 = (chunk & 0x01) ? 1 : 0;
    D5 = (chunk & 0x02) ? 1 : 0;
    D6 = (chunk & 0x04) ? 1 : 0;
    D7 = (chunk & 0x08) ? 1 : 0;

    // trigger EN to low, force LCD start internal operation
    EN = 1;
    delay_us(100);

    EN = 0;
    delay_us(100);
}

// ======================
// Send 8byte data into Instruction-Reg or Display-Data-Reg
// @param chunk: 4 bytes of data to send to lcd
// @param location: the register that data will be sent to
// ======================
static void _write_8byte(uchar_t byte, LCD_REGISTER_TYPE location)
{

    if (location == COMMAND)
    {
        RS = _SELECT_COMMAND_REG;

        // send high-order-bytes
        _write_4byte(byte >> 4, COMMAND);

        // send low-order-bytes
        _write_4byte(byte & 0x0F, COMMAND);
    }
    else
    {
        RS = _SELECT_DATA_REG;

        // send high-order-bytes
        _write_4byte(byte >> 4, DATA);

        // send low-order-bytes
        _write_4byte(byte & 0x0F, DATA);
    }

    // ==== take 8byte into 4byte + 4byte, and send twice ====
}

extern void clear_lcd()
{
    _write_8byte(_CLEAR_DISPLAY, COMMAND);
    delay_ms(5);
}

// ======================
// Set cursor back to home
// ======================
extern void home()
{
    _write_8byte(_FIRST_LINE, COMMAND);
    delay_ms(5);
}

extern void shift(SHIFT_DIRECTION direction)
{
    if (direction == LEFT)
    {
        _write_8byte(_SHIFT_DISPLAY_LEFT, COMMAND);
    }
    else
    {
        _write_8byte(_SHIFT_DISPLAY_RIGHT, COMMAND);
    }

    delay_ms(5);
}

// ======================
// initial lcd procedure
// ======================
extern void lcd_init()
{

    // wait for lcd power on
    delay_ms(15);

    //step1-3, Fake 8bit-mode Function Set, for initial only
    for (i = 0; i <= 2; i++)
    {
        _write_4byte(0b0011, COMMAND);
        delay_ms(5);
    }

    // step4, Fake 4bit-mode Function Set, , for initial only
    _write_4byte(0b0010, COMMAND);
    delay_ms(5);

    // step5, Real Function Set, N=2, F=0
    _write_8byte(_SET_LINE_DOT, COMMAND);
    delay_ms(5);

    // step6, Display on/off Control
    _write_8byte(_SET_DISPLAY_CONTROL, COMMAND);
    delay_ms(5);

    // step8, Entry mode set
    _write_8byte(_SET_ENTRY_MODE, COMMAND);
    delay_ms(5);

    // step7, Clear LCD
    clear_lcd();
}

// ======================
// Print text on LCD
// ======================
extern void print(char *text)
{
    while (*text != '\0')
    {
        _write_8byte(*text++, DATA);
    }
}