/*
 * 注意，本範例使用 4bit-mode 傳輸，且 RW 接腳在電路上直接接地，因此只能寫入LCD，無法從LCD讀取     
 *
 * 注意，寫入DR時，開啟傳輸(EN=1)和開啟傳輸(EN=0)之間，
 * 	   至少需要等待 40us 以上，避免第一個字元無法顯示，詳見 Table6 @ datasheet
 */

#include <xc.h>

#define _XTAL_FREQ 20000000 // 20MHZ

#define RS RD2
#define EN RD3
#define D4 RD4
#define D5 RD5
#define D6 RD6
#define D7 RD7

#pragma config FOSC = HS   // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF  // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON  // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF   // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF   // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF   // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF    // Flash Program Memory Code Protection bit (Code protection off)

// 根據輸入參數，設置 D7(RD7) ~ D4(RD4) 的內容
void Lcd_SetBit(char data) 
{
    if (data & 1)
    {
        D4 = 1;
    }
    else
    {
        D4 = 0;
    }

    //
    if (data & 2)
    {
        D5 = 1;
    }
    else
    {
        D5 = 0;
    }

    //
    if (data & 4)
    {
        D6 = 1;
    }
    else
    {
        D6 = 0;
    }

    if (data & 8)
    {
        D7 = 1;
    }
    else
    {
        D7 = 0;
    }
}

// 取 cmd 的低四位，寫入 IR 中
// 用於初始化流程中，進入4bit-mode前使用
void Lcd_Cmd_4bit(char a)
{
    // 選擇 IR
    RS = 0;
    
    // ==== 設置傳送內容 ====
    // 根據 a 的值，設置 D4-D7 的內容
    Lcd_SetBit(a);

    // EN 接腳致能，使 D4-D7 的內容寫入 LCD 中
    EN = 1;
    
    // 必要，等待數據傳輸完畢
    __delay_us(800);

    EN = 0;
    
}

// 將 8bit 的 cmd，分2次寫入 IR 中
void Lcd_Cmd_8bit(char a)
{
    // 選擇 IR
    RS = 0;
    
    // ==== 傳送高四位 ====
    Lcd_SetBit((a & 0xF0) >> 4);
    EN = 1;
    __delay_us(800);
    EN = 0;

    // ==== 傳送高四位 ====
    Lcd_SetBit(a & 0x0F);
    EN = 1;
    __delay_us(800);
    EN = 0;
    
}

void Lcd_Set_Cursor(char a, char b)
{
    // 設置 DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    ADD  ADD  ADD  ADD  ADD  ADD  ADD

    // LCD螢幕第1行，對應的起始位址為
    // 0     1     2     3     4     5     6     7
    // 0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07

    // 以移動到LCD螢幕第1行第3個位置(0x03) 為例
    // 使用 設置DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    0    0    0    0    0    1    1       = 0x83

    // LCD螢幕第2行，對應的起始位址為
    // 0     1     2     3     4     5     6     7
    // 0x40  0x41  0x42  0x43  0x44  0x45  0x46  0x47
    
    // 以移動到LCD螢幕第2行第3個位置(0x43) 為例
    // 使用 設置DDRAM位址指令
    // RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    // 0    0  | 1    1    0    0    0    0    1    1       = 0xC3

    // temp 為實際的 DDRAM 的位址，z 為地址高四位的值，y為地址低四位的值
    char temp, z, y;

    // 計算實際 DDRAM 的位址
    if (a == 1) // a==1，代表移動到第1行
    {
        temp = 0x80 + b; 
    }
    else if (a == 2) // a==1，代表移動到第2行
    {
        temp = 0xC0 + b;
    }

    z = temp >> 4;      // 將 temp 的高4位，移動到低四位
    y = temp & 0x0F;    // 取得 temp 低四位的值

    Lcd_Cmd_4bit(z);  // 傳送地址的高四位值
    Lcd_Cmd_4bit(y);  // 傳送地址的低四位值
}

// 傳送字元給DR，
// 使用此函數前，需要使用 Lcd_Set_Cursor 設置 DDRAM 的位址
void Lcd_Print_Char(char data)
{

    
    // 將數據拆分為高4位和低四位
    char Lower_Nibble, Upper_Nibble;
    
    RS = 1; // 選擇 DR
    
    // ==== 先傳送高四位 ====
    Upper_Nibble = (data & 0xF0) >> 4;
    
    // 將高四位傳遞給 DB4-DB7
    Lcd_SetBit(Upper_Nibble);

    EN = 1; // 開始傳送
    __delay_us(40);
    EN = 0; // 關閉傳送

    // ==== 再傳送低四位 ====
    Lower_Nibble = data & 0x0F;
    
    // 將低四位傳遞給 DB4-DB7
    Lcd_SetBit(Lower_Nibble);

    EN = 1; // 開始傳送
    __delay_us(40);
    EN = 0; // 關閉傳送
}

// 傳送字串給DR，連續調用 Lcd_Print_Char()
void Lcd_Print_String(char *a)
{
    for ( int i = 0; a[i] != '\0'; ++i)
        Lcd_Print_Char(a[i]); 
}

void Lcd_Init()
{    
    // 將 RDx 設置為 output
    TRISD = 0x00;
    
    // 將 PORTD 的內容清除為 0
    PORTD = 0x00;
    
    // 將 DB4-DB7設置為0
    //Lcd_SetBit(0x00);

    // LCD 初始化序列 for 4bit-mode，見 HD44780U.md
    //    step1，delay 55ms
    //    step2，DB5=DB4=1 + delay 4.1ms
    //    step3，DB5=DB4=1 + delay 100us
    //    step4，DB5=DB4=1
    //    step5，DB5=1，DB4=0
    //    (已進入4bit-mode，以下指令需發送兩次)
    //    step6-，Function-Set 指令、Entry-Mode 指令、其他客製化指令

    // step1, 等待 LCD 完成初始化的時間
    __delay_ms(55);

    // step2
    Lcd_Cmd_4bit(0x03);
    __delay_ms(4.1);

    // step3
    Lcd_Cmd_4bit(0x03);
    __delay_us(100);

    // step4、step5
    Lcd_Cmd_4bit(0x03);
    Lcd_Cmd_4bit(0x02); 

    // ==== 已進入 4bit-mode，以下 8bit 的資料需要分兩次送 ====

    // step6
    // Function-Set 指令 = 0x28 = 0b0010_1000 = 0b001(DL)_(N)(F)**
    //    DB5=1=代表Function-Set 指令
    //    DB4=0=4bit-mode
    //    DB3=N=1=雙行顯示
    //    DB2=F=0=5*8
    Lcd_Cmd_8bit(0x28);
    
    // EntryMode指令 = 0x06 = 0b0000_0110 = 0b0000_01(I/D)(S)
    //    DB2=1=代表EntryMode指令
    //    DB1=I/D=1=Increments
    //    DB0=S=0=ScreenOff=只移動游標，不移動螢幕
    Lcd_Cmd_8bit(0x06);
    
    // DisplayOnOff指令 = 0x0C = 0b0000_1100 = 0b0000_1(D)(C)(B)
    //    DB3=1=代表DisplayOnOff指令
    //    DB2=D=1=DisplayOn
    //    DB1=C=1=CursorOn
    //    DB0=B=0=BlikingOff
    Lcd_Cmd_8bit(0x0c);

    // clear screen
    Lcd_Cmd_8bit(0x01);
    
}

/* 
 * Usage
 
 void main(void) 
{
    
    Lcd_Init();
    
    Lcd_Set_Cursor(1, 0);
    Lcd_Print_String("0123456789abcdef");
    
    Lcd_Set_Cursor(2, 0);
    Lcd_Print_String("fedcba9876543210");
    
    while(1){}
}

 */