#include <xc.h>

#define _XTAL_FREQ 20000000 // 20MHZ

#pragma config FOSC = HS   // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF  // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON  // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF   // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF   // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF   // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF    // Flash Program Memory Code Protection bit (Code protection off)

void interrupt ISR()
{
  if (INTF)
  {
    // turn-off LED
    RC0 = ~RC0;
    
    __delay_ms(2000);
    
    // clear INTF to 0
    INTF = 0;
  }
}

void main(void) 
{
    // 本範例使用 RB0/INT 作為外部中斷的觸發，由 INTE/INTF 控制
    
    // ==== config IO pin ====
    // 將 RC0 設置為輸出
    TRISC0 = 0;
    
    // ==== config interrupt ====
    INTEDG = 0;     // 設置 RB0/INT 在上升緣觸發中斷
    INTE = 1;       // 允許 RB0/INT 發起中斷
    GIE = 1;        // 允許全局中斷
    
    while(1)
    {       
        // turn-on LED 
        RC0 = 1;
        
        __delay_ms(100);

    }
    
}
