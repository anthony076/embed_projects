## Timer模組的使用
- PIC16F877A 上的 Timer
  - 有三組 Timer，Timer0/Timer2 為8bit定時器，Timer1為16bit定時器
  - 3bit的分頻器，提供8種分頻選擇
  - 可選擇使用內部的震盪頻率 或 由RA4/T0CKI接腳輸入的外部時脈
  - 使用RA4/T0CKI接腳作為 Timer 的觸發源時，可以選擇觸發的方式

- Timer-Block-Diagram
  
  <img src="doc/timer/timer-block-diagram-with-prescaler.png" width=80% height=auto>

- Timer模組有兩種使用模式
  - 計時器模式 (timer-mode)，OPTION_REG暫存器的T0CS位設置為 0
  - 計數模式 (counter-mode)，OPTION_REG暫存器的T0CS位設置為 1
  - 兩者都會進行+1，都可以用來延遲時間，差異在`提供計數的時脈源`不同，

- timer-mode 和 counter-mode 的差異
  - timer-mode 的計數方式
    
    此模式下的 Timer 是一個 8-bit 的計時器，計數值會在`每一個新的指令週期`自動 +1，
    8-bit 代表最大的計數值為 0 ~ 2^8(255)，當計數值 > 255 時，Timer模組會自動發出 Timer溢位的中斷

  - counter-mode 的計數方式

    此模式下的 Timer 是一個 8-bit 的計數器，計數值會在 `RA4/T0CKI 輸入時脈的上緣或下緣時`自動+1，
    8-bit 代表最大的計數值為 0 ~ 2^8(255)，當計數值 > 255 時，Timer模組會自動發出 Timer溢位的中斷

## 利用 Timer 取代 delay 函數 (利用 Timer 執行定時任務)
- 阻塞和非阻塞式的 delay函數
  - delay函數是阻塞式的，會使主線程暫停執行
  - Timer 的運行和主線程是並行的，不會阻塞主線程

- Timer 是基於 interrupt 的，interrupt 是CPU級的主動輪詢，適合用於定時執行任務，
  
  例如，在不影響主線程的狀態下，透過 timer 定時閃爍 LED

- 利用 Timer 的中斷實現定時任務
  - 發生中斷的時機
    
    Timer 啟動後，會不斷自動的計數 (+1)，計數一次的時間是根據配置決定的，
    Timer 的中斷必須在`計數值超過容許最大值(8-bit Timer 為 255)，才會觸發中斷`，

  - 變更計數的初始值
    
    Timer `預設會從 0 開始計數`，但使用者可以透過`寫入TMR0暫存器，變更計數的初始值`
  
  - 透過計數中斷次數，延長定時時間
    
    發生中斷的時間，依照配置大約在 ms 或 us 的等級，若需要 > ms 的定時任務，就需要`在中斷處理函數中，搭配中斷次數的計數`來實現，
  
    例如，若觸發中斷的時間為 100ms，要實現 1s 的定時任務，
    則需要 1s / 100ms = 0.01 * 1000 = 10 次，代表每中斷10次，花費大約 1s 的時間

## 計數時間的計算與調整
- Timer 的邏輯電路中，擁有與 WDT 共用的分頻器(Prescaler)，透過分頻器可以調整 Timer 計數的速度，
  - 注意，一旦分頻器分配給 Timer 使用，WDT 就無法再使用，
  
  - 若分頻器分配給 `Timer` 使用，Timer 的計數值會自動寫入 TMR0暫存器，
    此時，若任何數據或指令寫入TMR0暫存器，都會導致分頻器的計數值被清0，

  - 若分頻器分配給 `WDT` 使用，若任何數據或指令寫入TMR0暫存器，不會導致分頻器的計數值被清0，

---

- `狀況1`，不使用分頻器下的時間計算
  - step1，計數1次需要的時間 (TimerCount)
    
    預設情況下，Timer模組`使用 1/4 的內部時脈` (CLKO = Fosc / 4)，
    若 PIC 使用 16MHZ 的震盪器，CLK0 = 16M/4 = 4MHZ，則Timer模組計數1次需要的時間為，

    `TimerCount = 1/f)為 1/4M = 0.25us = 250ns`

  - step2，Timer模組發出一次中斷的時間 (Tic)

    timer的計數值要超過 255 時才會發出中斷，因此從 0 計數到 256 的總時間為，
    
    `Tic = TimerCount * 256 = 250ns * 256 = 64000ns = 0.064ms`
    
    代表在不使用分頻器的狀況下，timer 每 0.064ms 發出一次中斷

  - step3，欲產生 1s 的延遲，需要產生中斷的次數 (Count)

    `Count = 1s/0.064ms = 15625 個中斷`

  - 公式，根據 step1-step3，計算1秒需要多少個中斷的公式為
    ```
    CLK0 = Fosc / 4
    TimerCount = 1 / CLK0

    Tic = TimerCount * 256
    Count = 1 / Tic
    
    簡化為
    Count = Fosc / 1024  = 16000000 / 1024 = 15625
    ```
---

- `狀況2`，使用分頻器下的時間計算
  - 利用 OPTION_REG暫存器的 PS2:PS0位，可以設置分頻器，共有 2^3(8) 種選擇，

    可將輸入的頻率除以2倍/4倍/8倍/16倍/32倍/64倍/128倍/256倍

  - step1，計數1次需要的時間 (TimerCount)
    
    預設情況下，Timer模組`使用 1/4 的內部時脈` (CLKO = Fosc / 4)，

    若 PIC 使用 16MHZ 的震盪器，輸入的頻率 CLK0 為
    
    `CLK0 = 16M/4 = 4MHZ`
    
    若使用 8 倍的分頻器，經過分頻器後的頻率為 
    
    `Fpre = 4M / 8 = 0.5MHZ`
    
    則Timer模組計數1次需要的時間為，

    `TimerCount = 1/Fpre = 1/0.5M = 2us`

  - step2，Timer模組發出一次中斷的時間 (Tic)

    timer的計數值要超過 255 時才會發出中斷，因此從 0 計數到 256 的總時間為，
    
    `Tic = TimerCount * 256 = 2us * 256 = 512us`
    
    代表使用8倍分頻器的狀況下，timer 每 512us 發出一次中斷

  - step3，欲產生 1s 的延遲，需要產生中斷的次數 (Count)

    `Count = 1s/512us = 1953.125 個中斷`

  - 公式，根據 step1-step3，計算1秒需要多少個中斷的公式為，`以8倍分頻為例`
    ```
    CLK0 = Fosc / 4
    Fpre = CLK0 / 8
    TimerCount = 1 / Fpre

    Tic = TimerCount * 256
    Count = 1 / Tic
    
    簡化為
    Count = Fosc / ( 4 * 256 * Prescal ) = 16M / ( 4 * 256 * 8 ) = 1953.125
    ```

- `狀況3`，使用分頻器下的通用公式
  ```
  // TMR0 為TMR0的值，預設為 0 ，開發者可自定義初始值

  Tic = ( 4 * (256 - TMR0) * Prescal ) / Fosc
  Count = 1 / Tic = Fosc / ( 4 * (256 - Prescal) * Prescal ) 
  ```

## Timer0 相關暫存器
- 配置流程
  - step1，選擇時脈來源 
  - step2，(若選擇外部的RA4/T0CKI)，選擇+1的時機(選擇時脈邊緣)
  - step3，選擇是否使用分頻器
  - step4，(若使用分頻器)，選擇分頻器的除頻倍率

- OPTION_REG 暫存器
  - RBPU位，PORTB Pull-up Enable bit
    - 非 timer 必要的設置選項
    - RBPU=1，關閉 PORTB pull-ups
    - RBPU=0，開啟 PORTB pull-ups

  - T0CS位: TMR0 Clock Source Select bit
    - 透過選擇不同的TMR0時脈來源，將TMR0設置為 timer-mode 或 counter-mode
    - T0CS=1，用於將Timer模組設置為 counter-mode，
      TMR0 時脈來源為 RA4/T0CKI 接腳輸入的時脈

    - T0CS=0，用於將Timer模組設置為 timer-mode，
      TMR0 時脈來源為 內部指令週期使用的時脈(CLKO)，
      CLKO指的是OSC2/CLKO接腳輸入的時脈，即外部晶體震盪器輸入的時脈

  - T0SE位: TMR0 Source Edge Select bit
    - 選擇 counter-mode +1 的時機
    - 當 T0CS=1，選擇 RA4/T0CKI 接腳作為TMR0的時脈時 (Timer 處於 counter-mode)，
      此設置才有效
    - T0SE=1，counter 在 RA4/T0CKI 時脈的下緣時+1
    - T0SE=0，counter 在 RA4/T0CKI 時脈的上緣時+1

  - PSA位，Prescaler Assignment bit
    - 選擇將分頻器分配給 Timer模組 或 WDT模組
    - PSA=1，分頻器分配給 WDT模組，Timer模組無法除頻，Timer模組的時脈 == 輸入時脈 (CLKO 或 RA4/T0CKI)
    - PSA=0，分頻器分配給 Timer模組，WDT模組無法使用

  - PS2:PS0位，Prescaler Rate Select bits
    - 設置分頻的頻率
    - 若不需要分頻，設置 PSA=1，將分頻器分配給WDT模組
    - 可設置的分頻頻率

      | PS2:PS0 | 分頻頻率 (Fpre) |
      |---------| -------------- |
      |   000   | Fosc/2         |
      |   001   | Fosc/4         |
      |   010   | Fosc/8         |
      |   011   | Fosc/16        |
      |   100   | Fosc/32        |
      |   101   | Fosc/64        |
      |   110   | Fosc/128       |
      |   111   | Fosc/256       |

- TMR0暫存器
  保存當前的計數值，可以用於設置Timer計數的初始值

- INTCON暫存器
  - Timer模組屬於外設模組，要使用Timer模組，需要開啟以下開關
  - TMR0IE=1，啟用Timer0，允許Timer0發出中斷
  - PEIE=1，允許外設發出中斷
  - GIE=1，允許發出中斷

## 範例
- [簡易timer範例](replace-delay-with-timer/)


## 參考
- [ 5.0 TIMER0 MODULE @ datasheet-p55]()

- [Understanding Timers in PIC Microcontroller with LED Blinking Sequence](https://circuitdigest.com/microcontroller-projects/pic-microcontroller-timer-tutorial)

- [Use Timer Instead Of Delays](https://deepbluembedded.com/why-use-timer-instead-of-delay/)

- [使用timer1和timer2定時做多件事](https://www.arduino.cn/thread-12452-1-2.html)