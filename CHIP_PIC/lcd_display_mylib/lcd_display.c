#include "lcd.h"

void main()
{
    // set all pins of PORTB are output
    TRISB = 0x00;

    // initial PORTB value
    PORTB = 0x00;

    lcd_init();
    home();

    print("hello123456789ABCDEFGHIJK");

    while (1)
    {
        delay_ms(300);
        shift(LEFT);
    }
}