
// #1   Add LCD Library from Library Manager Panel in mikroC IDE,
//      and check the documentation for library usage.
// #2   For Lcd_Init(), need to define global variables first. (check below)

// define LCD pin (LCD Library Need)
sbit LCD_RS at PORTB.B0;
sbit LCD_EN at PORTB.B1;
sbit LCD_D4 at PORTB.B2;
sbit LCD_D5 at PORTB.B3;
sbit LCD_D6 at PORTB.B4;
sbit LCD_D7 at PORTB.B5;

// define LCD pin direction (LCD Library Need)
sbit LCD_RS_Direction at TRISB.B0;
sbit LCD_EN_Direction at TRISB.B1;
sbit LCD_D4_Direction at TRISB.B2;
sbit LCD_D5_Direction at TRISB.B3;
sbit LCD_D6_Direction at TRISB.B4;
sbit LCD_D7_Direction at TRISB.B5;

void main()
{
  Lcd_init();
  
  Lcd_Cmd(_LCD_CLEAR);
  Lcd_Cmd(_LCD_CURSOR_OFF);  // Turn off cursor

  // Write to LCD, Lcd_Out( row, column, *text);
  Lcd_Out(1,1, "Hello");
  Lcd_Out(2,1, "World");
  
  delay_ms(5000);
  Lcd_Cmd(_LCD_CLEAR);
  
  //
  Lcd_Out(1,1, "LCD");
  Lcd_Out(2,1, "Test");
}

