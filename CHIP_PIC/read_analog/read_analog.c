#include <stdio.h>
#include <stdlib.h>

// define LCD pin (LCD Library Need)
sbit LCD_RS at PORTB.B0;
sbit LCD_EN at PORTB.B1;
sbit LCD_D4 at PORTB.B4;
sbit LCD_D5 at PORTB.B5;
sbit LCD_D6 at PORTB.B6;
sbit LCD_D7 at PORTB.B7;

// define LCD pin direction (LCD Library Need)
sbit LCD_RS_Direction at TRISB.B0;
sbit LCD_EN_Direction at TRISB.B1;
sbit LCD_D4_Direction at TRISB.B4;
sbit LCD_D5_Direction at TRISB.B5;
sbit LCD_D6_Direction at TRISB.B6;
sbit LCD_D7_Direction at TRISB.B7;

void main()
{

    int voltage;
    char voltageTxt[10];

    // set PORTB = output for LCD
    TRISB = 0x00;

    // set RA1=input
    TRISA = 0b00000010;

    UART1_Init(9600);
    ADC_Init();

    Lcd_init();
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Cmd(_LCD_CURSOR_OFF); // Turn off cursor
    Lcd_Cmd(_LCD_RETURN_HOME);

    while (1)
    {
        // read RA1
        voltage = Adc_read(1);
        IntToStr(voltage, voltageTxt);

        // output to lcd
        Lcd_Out(1, 1, VoltageTxt);

        // output to uart
        UART1_Write_Text(voltageTxt);
        UART1_Write_Text("\r");

        delay_ms(300);
    }
}