#include "lcd.h"
#include <stdlib.h>

// 寫入 eeprom
void write_eeprom(unsigned char addr, unsigned char data)
{
    // 若 WR 不為0，表示前次寫入未完成，進入等待
	while (EECON1bits.WR) { 				
		continue; 							
	} 										
    
    // 將寫入的位址寫入 EEADR
	EEADR = addr;							
    // 將寫入的數據寫入 EEDATA
	EEDATA = data;						
    
    // 設置存取 eeprom-data-memory
    EECON1bits.EEPGD = 0;
    
    // 開放 EEPROM 寫入
	EECON1bits.WREN = 1;
    
    // 關閉 GIE
	INTCONbits.GIE = 0;						
    
    // 進行寫入的序列
	EECON2 = 0x55;							
	EECON2 = 0xAA;							
	EECON1bits.WR = 1;

    // 重新開啟 GIE
    INTCONbits.GIE = 1;
    
    // 關閉 EEPROM 的寫入功能
	EECON1bits.WREN = 0;	
}

// 讀取 eeprom
unsigned char read_eeprom(unsigned char addr)
{
    unsigned char data;
    
    // 設置要存取的 eeprom 位址
    EEADR = addr;				
    
    // 設置存取 eeprom-data-memory
    EECON1bits.EEPGD = 0;
    
    // 開始讀取
    EECON1bits.RD = 1;
    
    data = EEDATA;
    
    return data;
}

void main(void) 
{
    
    Lcd_Init();
    
    Lcd_Set_Cursor(1, 0);
    Lcd_Print_String("hello");
    
    Lcd_Set_Cursor(2, 0);
    Lcd_Print_String("world");
    
    Lcd_Clear();
    
    // 寫入 eeprom
    write_eeprom(0, 0x1234);
    
    
    int value, c1, c2, c3, c4;
    
    while(1)
    {       
        // 讀取 eeprom
        value = (int)read_eeprom(0);
        
        Lcd_Set_Cursor(1, 0);
        Lcd_Print_String("value: ");
        
        Lcd_Set_Cursor(2, 0);
                
        char svalue[10]; 
        itoa(svalue, value, 10);
        
        Lcd_Print_String(svalue);
        
        __delay_ms(1000);
    }
    
}
