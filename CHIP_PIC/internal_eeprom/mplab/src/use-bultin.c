#include "lcd.h"
#include <stdlib.h>

void main(void) 
{
    
    Lcd_Init();
    
    Lcd_Set_Cursor(1, 0);
    Lcd_Print_String("hello");
    
    Lcd_Set_Cursor(2, 0);
    Lcd_Print_String("world");
    
    Lcd_Clear();
    
    eeprom_write(0,0xff);
    int value, c1, c2, c3, c4;
    
    while(1)
    {       
      value = (int)eeprom_read(0);
      //value = 1234;
      
      Lcd_Set_Cursor(1, 0);
      Lcd_Print_String("value: ");
      
      Lcd_Set_Cursor(2, 0);
      
      // 方法1，利用 Lcd_Print_Char，不需要轉換數值，但需要拆分成自元        

      c1 = (value/1000) % 10;
      c2 = (value/100) % 10;
      c3 = (value/10) % 10;
      c4 = (value/1) % 10;

      Lcd_Print_Char(c1 + '0');
      Lcd_Print_Char(c2 + '0');
      Lcd_Print_Char(c3 + '0');  
      Lcd_Print_Char(c4 + '0');  
      
      // 方法2，利用 Lcd_Print_String，需要將int轉換為 string
      // 需要調用 stdlib 的 itoa()

      //  char svalue[10]; 
      //  itoa(svalue, value, 10);
      
      //  Lcd_Print_String(svalue);
      
      __delay_ms(1000);
    }
    
}
