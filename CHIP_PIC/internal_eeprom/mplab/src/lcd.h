/*
* 注意，本範例使用 4bit-mode 傳輸，且 RW 接腳在電路上直接接地，因此只能寫入LCD，無法從LCD讀取     
*
* 注意，寫入DR時，開啟傳輸(EN=1)和開啟傳輸(EN=0)之間，
* 	   至少需要等待 40us 以上，避免第一個字元無法顯示，詳見 Table6 @ datasheet
*/

#include <xc.h>

#define _XTAL_FREQ 20000000 // 20MHZ

#define RS RD2
#define EN RD3
#define D4 RD4
#define D5 RD5
#define D6 RD6
#define D7 RD7

#pragma config FOSC = HS   // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF  // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON  // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF   // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF   // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF   // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF    // Flash Program Memory Code Protection bit (Code protection off)

#define Lcd_Clear(); Lcd_Cmd_8bit(0x01);

// 根據輸入參數，設置 D7(RD7) ~ D4(RD4) 的內容
void Lcd_SetBit(char data) ;

// 取 cmd 的低四位，寫入 IR 中
// 用於初始化流程中，進入4bit-mode前使用
void Lcd_Cmd_4bit(char a);

// 將 8bit 的 cmd，分2次寫入 IR 中
void Lcd_Cmd_8bit(char a);

void Lcd_Set_Cursor(char a, char b);

// 傳送字元給DR，
// 使用此函數前，需要使用 Lcd_Set_Cursor 設置 DDRAM 的位址
void Lcd_Print_Char(char data);

// 傳送字串給DR，連續調用 Lcd_Print_Char()
void Lcd_Print_String(char *a);

void Lcd_Init();


/* 
 * Usage
 
 void main(void) 
{
    
    Lcd_Init();
    
    Lcd_Set_Cursor(1, 0);
    Lcd_Print_String("0123456789abcdef");
    
    Lcd_Set_Cursor(2, 0);
    Lcd_Print_String("fedcba9876543210");
    
    while(1){}
}

 */