## PIC16F877A 常用配置
- Oscillator Configurations
  - 詳見，[震盪器(osc)配置](#震盪器osc配置)

  - LP: 適用於32KHZ以下的 crystal-osc
  - XT: 適用於4MHZ以下的 crystal-osc
  - HS: 適用於4MHZ以上的 crystal-osc
  - RC: 適用於RC電路產生的 clock 輸入
  ```c
  #pragma config FOSC = EXTRC     // Oscillator Selection bits (RC oscillator)
  #pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
  #pragma config FOSC = XT        // Oscillator Selection bits (XT oscillator)
  #pragma config FOSC = LP        // Oscillator Selection bits (LP oscillator)
  ```

- Watchdog Timer (WDT)
  - 用於睡眠模式下，定時喚醒device執行指定的操作
  - 用於軟體故障時，重設控制器
  
  ```c
  #pragma config WDTE = ON        // Watchdog Timer Enable bit (WDT enabled)
  #pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)  
  ```
  
- Power-up Timer (PWRT)
  
  上電後，是否延遲一段時間，以等待電壓穩定
  
  ```c
  #pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
  #pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
  ```

- Brown-out Reset (BOR)
  
  電源電壓不足或暫時降低，低於可靠運行所需的水平時，是否進行重置
  
  ```c
  #pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)
  #pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
  ```

- Low Voltage (SINGLE SUPPLY) ICSP
  - 是否啟用低壓(單電源)的ICSP
  - 低壓: 僅使用單電源，可以使用普通邏輯電平對器件進行燒錄
  - 高壓: MCLR 引腳應連接到高於 Vdd 的電壓電平
  ```c
  #pragma config LVP = ON         /*Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit 
                                   (RB3/PGM pin has PGM function; low-voltage programming enabled)*/
  #pragma config LVP = OFF        /*Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit
                                 (RB3 is digital I/O, HV on MCLR must be used for programming)*/
  ```

- Data-EEPROM-Memory Code Protection bit (CPD)
  
  是否啟用Data-EEPROM-Memory的代碼保護
  
  ```c
  #pragma config CPD = ON         // Data EEPROM Memory Code Protection bit (Data EEPROM code-protected)
  #pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)  
  ```

- Flash-Program-Memory Write Enable bit
  
  Flash-Program-Memory 寫入致能控制位，控制是否開啟寫入保護，或控制保護的區域
  
  ```c
  #pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
  #pragma config WRT = 256        // Flash Program Memory Write Enable bits (0000h to 00FFh write-protected; 0100h to 1FFFh may be written to by EECON control)
  #pragma config WRT = 1FOURTH    // Flash Program Memory Write Enable bits (0000h to 07FFh write-protected; 0800h to 1FFFh may be written to by EECON control)
  #pragma config WRT = HALF       // Flash Program Memory Write Enable bits (0000h to 0FFFh write-protected; 1000h to 1FFFh
  ```

- Flash-Program-Memory Code Protection bit
  Flash-Program-Memory 讀取控制位，開啟時，FW無法被外部讀取
  ```c
  #pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)
  #pragma config CP = ON          // Flash Program Memory Code Protection bit (All program memory code-protected)
  ```

## 震盪器(osc)配置
- [震盪器基礎](../Module/osc.md)
  
- 注意，震盪器頻率 != 指令週期的頻率

  - 以 PIC16f877 為例，`指令週期的頻率(CLKO) 為震盪器頻率(Fosc)的 1/4 倍`

    參考，[p10 @ PIC16f877-datasheet]()

    <img src="doc/config-bits/relationship-of-instruction-freq-and-fosc.png" width=80% height=auto>

- 震盪器模式的選擇
  - `注意，可用石英振盪的頻率依 datasheet 為準`

  - `模式1`，LP-mode，Low-Power Crystal
    - 低功率且低頻的石英振盪器，適用於 32K - 200KHZ 的石英振盪器

  - `模式2`，XT-mode，Crystal/Resonator
    - 一般石英振盪電路提供的 clock，適用於 200k - 4MHZ 的石英振盪器

  - `模式3`，XTPLL-mode，Crystal/Resonator，且 PLL 致能

  - `模式4`，INTXT-mode，適用晶片內部的 Crystal/Resonator 電路
    - 內部振盪器通常是低成本內置 RC 振盪器，運行在 4MHz 精確度將有百分之 1.0

  - `模式5`，HS-mode，High-Speed Crystal/Resonator
    - 可操作於高速的石英震盪電路，適用於 4M - 25MHZ 的石英振盪器

  - `模式6`，HSPLL-mode，High-Speed Crystal/Resonator，且 PLL 致能

  - `模式7`，RC-mode，Resistor/Capacitor
    - 由電阻(R)和電容(C)組成的震盪電路
    - 依照R和C的設計，通常可以震盪出 31k - 8MHZ 的波形

  - `模式8`，EC-mode，外部時鐘(External Clock)直接輸入，例如頻率產生器

- 電容的選擇

  PIC 支援並聯的共振模式，不同的模式會影響外部電容(C1、C2)和內部電阻(Rf)的配置，
  透過調節PIC內部振盪電路放大增益，以保証產生符合 PIC 要求的標準頻率波型，

  以下為 PIC16f877 可使用的石英震盪器為例
  
  <img src="doc/config-bits/osc-config-1.png" width=80% height=auto>

## Ref
- [Configuration bits in PIC16F877A](https://openlabpro.com/guide/pic16f877a-configuration-bits/)

- [14.2 Oscillator Configurations @ PIC16f877-p147]()