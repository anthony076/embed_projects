## USART
- [通訊協定基礎](../Module/Communication.md)
  
- USART，Universal-Synchronous-Asynchronous-Receiver-Transmitter 
  - USART 代表 Serial-Port 可以操作在同步/非同步兩種傳輸模式，
    兩種傳輸模式的差異，在於MCU和外部裝置是否使用相同的CLK
  
  - 非同步傳輸時，代表 MCU 和 外部裝置通訊時，使用不同的 CLK，
    - MCU接收/發送都使用MCU內部的 BaudRateGenerator
    - 全雙工 (full-duplex)，發送和接收同時發生，

  - 同步傳輸時，代表 MCU 和 外部裝置通訊時，`使用相同的 CLK`，分為兩種模式
    - master-mode，MCU和外部裝置，都`使用MCU產生的CLK`
    - slave-mode，MCU和外部裝置，都`使用外部裝置產生的CLK`
    - 半雙工 (half-duplex)，發送和接收同時只會發生一種

- 以下兩個條件必須先配置後，才能將 `RC6/TX/CK接腳` 和 `RC7/RX/DT接腳` 設置為 UART
  - 條件1，RCSTA暫存器的SPEN位設置為啟用
  - 條件2，設置將 `RC6/TX/CK接腳` 和 `RC7/RX/DT接腳` 的方向

- UART 架構
  
  <img src="doc/uart/uart-block-diagram.png" width=80% height=auto>

- UART 相關暫存器
  - TXSTA 暫存器，TRANSMIT STATUS AND CONTROL REGISTER，設置/檢視 TX 用暫存器
  - RCSTA 暫存器，RECEIVE STATUS AND CONTROL REGISTER，設置/檢視 RX 用暫存器
  - SPBRG 暫存器，Baud Rate Generator Register，設置 BaudRate

- BaudRate 計算公式
  - SYNC = 0 (非同步模式)，BRGH = 0 (LowSpeed)
    
    $$ BaudRate = \frac{ F_{osc} }{64 * (SPBRG + 1) } $$

  - SYNC = 1 (同步模式)，BRGH = 0 (LowSpeed)
    $$ BaudRate = \frac{ F_{osc} }{4 * (SPBRG + 1) } $$

  - SYNC = 0 (非同步模式)，BRGH = 1 (HighSpeed)
    $$ BaudRate = \frac{ F_{osc} }{16 * (SPBRG + 1) } $$

  - SPBRG 值

    <img src="doc/uart/set-SPBRG.png" width=500 height=auto>

## 非同步傳輸模式
- 非同步傳輸模式下，MCU是全雙工的，代表MCU可以同時處理發送和接收
  
- 在非同步傳輸模式下，使用 Non-Returnto-Zero (NRZ) 的數據格式
  - 1bit start-bit
  - 8bit/9bit data-bits，常見 8bit
  - 1bit stop-bit
  - LSB 優先

- 不支援硬體優先級，但可以透過軟體實現，並保存在 data-bit-8

- 非同步傳輸模式在 Sleep 模式下會被關閉

- 非同步/Transmit
  - 非同步發射方塊圖
  
    <img src="doc/uart/asynchronous-transmitter-block-diagram.png" width=80% height=auto>

  - 簡易關係，發送時，TXREG暫存器 是 TSR暫存器的緩衝
    ```mermaid
    flowchart LR
    b1[databus] --> b2[TXREG] --> b3[TSR] --> pin
    ```

  - 功能，TXREG暫存器
    - 資料送 DataBus `先送到TXREG暫存器進行緩衝`，真正要傳輸時，才會將數據送到TSR暫存器
  
    - TXREG暫存器中的數據`不會立刻被載入到TSR暫存器`，當前一次傳輸完成後(stop-bit-1)，才會將TXREG暫存器中的數據傳遞給TSR暫存器
 
    - TXIF位=TXREG暫存器的狀態 / TXIE位=TXREG暫存器為空時，是否發出中斷
      - TXREG暫存器為空時，會設置 TXIF位，表示 TXREG暫存器為空，
      - 利用 TXIE位@PIE1-bit4，可以設置TXREG暫存器為空時，是否發出中斷

  - 功能，TSR暫存器(Transmit-Serial-Register)
    - TSR暫存器 的 bit8，用於優先級
      - 由 TX9，控制是否啟用 TSR bit8 的優先級位
      - 由 TX9D，控制 bit8 的內容

    - TSR暫存器 是否輸出到接腳
      - TSR暫存器的數據會被送到 PinBuffer 做為輸出到接腳的監控
      - 由 TRMT，指示TSREG暫存器的狀態，只能讀取不能設置，當TSR暫存器為空時，會自動設置 TRMT
      - 由 SPEN，控制TSR暫存器的資料送到 RC6/TX/CK 接腳

    - TSR暫存器 觸發發送的時機
      - 由 SPBRG，決定 TSR 觸發的頻率
      - 由 TXEN，決定 控制 TSR 是否有效
  
  - 數據禎 (Data-Frame)
  
    <img src="doc/uart/uart-bit-frame.png" width=40% height=auto>

    - uart 的數據禎為 11bit = 1bit-start + 1bit-stop + 8bit-data + 1bit-parity

  - 非同步發射波形圖
  
    <img src="doc/uart/asynchronous-transmitter-waveform.png" width=80% height=auto>

  - 非同步/Transmit 的使用流程
    - step1，透過 `SPBRG`，設置 BaudRate，透過 `BRGH` 設置高速或低速
    - step2，透過 `SYNC` 設置同步或非同步，並設置 `SPEN`，將 TSR暫存器連接到 TX 接腳
    - step3，若需要 TX 為空時的中斷通知，設置 `TXIE`
    - step4，若需要 9bit 傳輸 (bit-8)，設置 `TX9`
    - step5，透過 `TXEN`，使TSR暫存器有效，啟用後，TXREG暫存器才會傳遞數據給TSR暫存器，TXIF 才有作用
    - step6，如果啟用 9bit 傳輸，將 bit-8的值保存在 `TX9D` 中
    - step7，將數據保存在 `TXREG暫存器` 中，開始進行TX傳輸
    - step8，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能
    
- 非同步/Receive
  - 非同步接收方塊圖

    <img src="doc/uart/asynchronous-receive-block-diagram.png" width=80% height=auto>

  - 簡易關係，接收時，RSR暫存器 是 RCREG暫存器的緩衝
    ```mermaid
    flowchart LR
    b1[pin] --> b2[RSR] --> b3[RCREG] --> b4[databus]
    ```

  - 功能，RSR暫存器 (Receive-Serial-Register)
    - RSR透過 DataRecovery 獲取接收數據
      - RX接腳透過 SPEN位 的啟用，將 RX接腳連接到 RX 的 PinBuffer，從RX接腳接收的數據，會暫時保存在 PinBuffer 中，
      - 透過 DataRecovery，將 PinBuffer 中的數據移入 RSR 中，DataRecovery 透過 SPBRG暫存器 控制移入RSR暫存器的速度
    
    - RSR透過OERR位發起溢位錯誤  
      - 若前一次的接收已經結束，可以發起另一次接收，但RCREG暫存器仍然是占用狀態，
        此時，OERR位會被設置為1，此時 RSR 的數據就無法再傳遞給 RCREG，
        且 RSR 的數據會被拋棄
      
      - OERR位必須透過 Software 手動清除，透過將 CREN 設置為0，再重新設置為1

    - RSR透過FERR位發起 Framing-Error
      - Framing-Error 是由數據不正確所觸發的，可能是不同的通訊設定或是雜訊造成的
  
    - RSR透過 CREN位，使 RSR暫存器進行連續接收

  - 功能，RCREG暫存器
      - RSR暫存器中的數據`不會立刻被載入到RCREG暫存器`，當前一次傳輸完成後(stop-bit-1)，才會將RSR暫存器中的數據傳遞給RCREG暫存器
      
      - RSR的數據傳遞給RCREG後，RCIF位就會被設置為1，若RCIE位設置為1，
        就會在RCREG暫存器有數據時，發出 interrupt，用於提示RCREG暫存器已接收到數據

      - 在RCREG暫存器已滿時，RCIF自動被設置為1，當RCREG暫存器中的數據被讀取後，RCIF自動被清除為0

      - 讀取 RCREG，會覆蓋前一次的 RX9D/FERR 的值，因此，
        若需要保留RX9D/FERR，需要在讀取 RCREG 前，透過 RCSTA 讀取 RX9D/FERR 的值

  - 非同步接收波形圖
    
    <img src="doc/uart/asynchronous-receive-waveform.png" width=80% height=auto>

  - 非同步/Receive 的使用流程
    - step1，透過 `SPBRG`，設置 BaudRate，透過 `BRGH` 設置高速或低速
    - step2，透過 `SYNC` 設置同步或非同步，並設置 `SPEN`，將 RSR暫存器連接到RX接腳
    - step3，若需要 RX 為空時的中斷通知，設置 `RCIE`
    - step4，若需要 9bit 傳輸 (bit-8)，設置 `RX9`
    - step5，透過 `CREN`，使RSR暫存器有效，啟用後，RSR暫存器才會傳遞數據給RCREG暫存器，RXIF 才有作用
    - step6，當接收完成後，RCIF被自動被設置為1，若 RCIE=1，就會發出中斷
    - step7，透過 RCSTA 讀取 bit-8 和錯誤位，判斷錯誤是否發生
    - step8，透過 RCREG 讀取接收的數據
    - step9，如果有錯誤發生，將 CREN 設置為0，將錯誤清除，再重新將 CREN 設置為1，重新接收數據
    - step10，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能

## 同步傳輸模式
- 同步傳輸模式下，`發送和接收不會同時發生`，同時間只會處理發送 或者 接收

- 同步傳輸，代表 MCU 和 外部裝置通訊時，`使用相同的 CLK`，分為兩種模式
  - master-mode，MCU和外部裝置，都`使用MCU產生的CLK`
  - slave-mode，MCU和外部裝置，都`使用外部裝置產生的CLK`

- 同步傳輸/Master/Transmit 的使用
  - step1，透過 `SPBRG`，設置 BaudRate，透過 `BRGH` 設置高速或低速
  - step2，透過 `SYNC` 開啟同步、`SPEN` 開啟序列傳輸、`CSRC` 選擇 MasterMode
  - step3，若需要 TX 為空時的中斷通知，設置 `TXIE`
  - step4，若需要 9bit 傳輸 (bit-8)，設置 `TX9`
  - step5，透過 `TXEN`，使TSR暫存器有效
  - step6，若啟用 9bit 傳輸，將第9bit的資料放在 `TX9D`位中
  - step7，將要發送的資料，保存在 `TXREG` 暫存器中
  - step8，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能

- 同步傳輸/Master/Receive 的使用
  - step1，透過 `SPBRG`，設置 BaudRate，透過 `BRGH` 設置高速或低速
  - step2，透過 `SYNC` 開啟同步、`SPEN` 開啟序列傳輸、`CSRC` 選擇 Master/Slave Mode
  - step3，確保 `CREN`和`SREN`設置為0
  - step4，若需要 RX 為空時的中斷通知，設置 `RCIE`
  - step5，若需要 9bit 傳輸 (bit-8)，設置 `RX9`
  - step6，依照需求，將 `CREN` 或 `SREN` 設置為 1，使 RSR暫存器有效
  - step7，當接收完成後，`RCIF`被自動被設置為1，若 RCIE=1，就會發出中斷
  - step8，透過 `RCSTA` 讀取 bit-8 和錯誤位，判斷錯誤是否發生
  - step9，透過 `RCREG` 讀取接收的數據
  - step10，如果有錯誤發生，將 `CREN` 設置為0，將錯誤清除，再重新將 CREN 設置為1，重新接收數據
  - step11，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能

- 同步傳輸/Slave/Transmit 的使用
  - step1，透過 SYNC 開啟同步、SPEN 開啟序列傳輸、CSRC 選擇 SlaveMode
  - step2，確保 CREN和SREN設置為0
  - step3，若需要 TX 為空時的中斷通知，設置 `TXIE`
  - step4，若需要 9bit 傳輸 (bit-8)，設置 `TX9`
  - step5，透過 TXEN，使TSR暫存器有效
  - step6，若啟用 9bit 傳輸，將第9bit的資料放在 TX9D位中
  - step7，將要發送的資料，保存在 TXREG 暫存器中
  - step8，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能

- 同步傳輸/Slave/Receive 的使用
  - step1，透過 SYNC 開啟同步、SPEN 開啟序列傳輸、CSRC 選擇 SlaveMode
  - step2，若需要 RX 為空時的中斷通知，設置 `RCIE`
  - step3，若需要 9bit 傳輸 (bit-8)，設置 `RX9`
  - step4，將 CREN 設置為 1，使 RSR暫存器有效
  - step5，當接收完成後，RCIF被自動被設置為1，若 RCIE=1，就會發出中斷
  - step6，透過 RCSTA 讀取 bit-8 和錯誤位，判斷錯誤是否發生
  - step7，透過 RCREG 讀取接收的數據
  - step8，如果有錯誤發生，將 CREN 設置為0，將錯誤清除，再重新將 CREN 設置為1，重新接收數據
  - step9，如果使用 interrupt，確保 `INTCON暫存器`的 `GIE` 和 `PEIE` 已設置中斷的功能

## 常用設置
  - [必要] 設置 TX/RX 接腳的數據傳輸方向
    - TRISC6 = 0，將C6接腳(TX)設置為輸出，
    - TRISC7 = 1，將C7接腳(RX)設置為輸入，

  - [必要] 開啟/關閉UART的功能
    - 啟用 TX/RX 的功能，SPEN 會將 TSR/RSR暫存器連接到 TX/RX 的接腳
    - SPEN = 0，關閉 UART 的功能，TSR/RSR暫存器不會連接到 TX/RX 的接腳
    - SPEN = 1，打開 UART 的功能，TSR/RSR暫存器連接到 TX/RX 的接腳

  - 開啟 high-baud-rate，
    - 只對非同步傳輸有效
    - BRGH = 0，關閉 high-speed-BaudRate
    - BRGH = 1，開啟 high-speed-BaudRate

  - [必要] 設置傳輸長度為 8bit 或 9bit
    - TX9 = 1，啟用 TX 的優先級位
    - RX9 = 1，啟用 RX 的優先級位;

  - [必要] 開啟非同步/同步傳輸模式，
    - SYNC = 0，啟動非同步傳輸
    - SYNC = 1，啟動同步傳輸

  - [必要] 開啟 Tx 的功能，
    - TXEN = 1，啟動TX傳輸
    - TXEN = 0，停止TX傳輸
    
  - [必要] 開啟 RX 的功能，
    - CREN = 1，連續接收，CREN = Continuous Receive Enable bit
    - SREN = 1，單一接收，SREN = Single Receive Enable bit

  - 設置同步傳輸模式的 Master/Slave mode，
    - CSRC = Clock Source Select bit
    - 只對同步傳輸有效
    - CSRC = 1，Master-Mode，觸發同步傳輸動作的CLK，來自MCU內部的 BaudRateGenerator
    - CSRC = 0，Slave-Mode，觸發同步傳輸動作的CLK，來自MCU外部

## 範例
- [Serial (USART) Communication with PIC16F877A](https://www.teachmemicro.com/serial-usart-pic16f877a/)

## 參考
- [10.0 USART @ datasheet-p113]()
- [Basics of UART Communication](https://www.youtube.com/watch?v=JuvWbRhhpdI)
- [Understanding UART Communication Programming](https://www.youtube.com/watch?v=QmjKRwgddxw)