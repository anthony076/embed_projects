
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifndef ESP8266_H
#define ESP8266_H

#define ESP8266_STATION 0x01
#define ESP8266_SOFTAP 0x02

#define ESP8266_TCP 1
#define ESP8266_UDP 0

#define ESP8266_OK 1
#define ESP8266_READY 2
#define ESP8266_FAIL 3
#define ESP8266_NOCHANGE 4
#define ESP8266_LINKED 5
#define ESP8266_UNLINK 6

// ====== low-level-functions =====
void _esp8266_putch(unsigned char);
unsigned char _esp8266_getch(void);

void _esp8266_print(unsigned const char *);
inline unsigned char _esp8266_waitResponse(void);
inline uint16_t _esp8266_waitFor(unsigned char *);

// ====== setup-esp8266 =====
void esp8266_mode(unsigned char);
unsigned char esp8266_connect(unsigned char *, unsigned char *);
void esp8266_disconnect(void);
void esp8266_ip(char *);

// ====== request/response =====
bit esp8266_start(unsigned char protocol, char *ip, unsigned char port);
bit esp8266_send(unsigned char *);
void esp8266_receive(unsigned char *, uint16_t, bool);

// ====== others =====
bit esp8266_isStarted(void);
bit esp8266_restart(void);
void esp8266_echoCmds(bool);
