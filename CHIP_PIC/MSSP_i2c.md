## MSSP Module
- [通訊協定基礎](../Module/Communication.md)

- [I2C 基礎](../Module/I2C.md)

- 在 MSSP模組 中的 i2c 實現了 Master/Slave 的所有功能，
  並在 `Star-bit/Stop-bit 都提供了中斷通知`，來決定釋放 bus 的時機

- i2c 和 SPI 的異同
  - 兩者都可以連接多個 slave-device
  - 兩者都需要 Clock 進行數據的同步

  - spi 比 i2c 的速度更快，可以輕鬆達到 20MHZ，i2c 最快的 ultra-high-speed 為 5MHZ
  
  - spi 透過 CPOL、CPHA 的設置，明確定義數據發送/讀取的時間(在 clock 的上下緣)，
    i2c 沒有明確定義發送讀取的時間，只有定義在某個電位可以讀取，因為沒有強制，因此需要 i2c 回應是否接收完畢
  
  - i2c 透過位址選擇 device，SPI 透過 SS 接腳選擇 device
  
  - i2c 的硬體是以 open-drain 或 open-collector 實作，spi 則是以 push-pull 實作
  
  - i2c 只有一條數據線，spi 有兩根數據線 (MISO 和 MOSI)
  
  - i2c 的接腳是輸出同時也是輸入，在輸出數據時會接收Ack，有收到才會繼續下一筆數據的傳輸，
  
    spi 的接腳的傳輸方向是單向的

## i2c 相關暫存器
- i2c 架構
  
  slave-mode

  <img src="doc\mssp\i2c-block-diagram.png" width=40% height=auto>

  master mode

  <img src="doc\mssp\i2c-block-diagram-master.png" width=70% height=auto>

- i2c 使用到的暫存器
  - SSPCON暫存器，配置 MSSP 模組用
  - SSPCON2暫存器，配置 i2c 用
  - SSPSTAT暫存器，檢視狀態用的暫存器
  - SSPBUF暫存器，輸入/輸出數據的緩衝暫存器，
  - SSPSR移位暫存器，負責傳送數據/接收數據的暫存器，
  - SSPADD位址暫存器，用於保存 slave-device 的位址，僅在 i2c 有效

- `SSPBUF暫存器`，輸入/輸出數據的緩衝暫存器
    - 使用者向 i2c-device 寫入數據或讀取數據的暫存器

    - `用於輸出緩衝` : 輸出資料送到SSPSR之前會在SSPBUF緩衝
      ```mermaid
      flowchart LR
      b1[輸出數據] --> b2[SSPBUF\n緩衝暫存器] 
      b2 --> b3[SSPSR\n移位暫存器] --> b4[slave-device]
      ```
    - `用於輸入緩衝` : 從SSPSR輸入的資料，在軟體讀取前，會先在SSPBUF緩衝
      ```mermaid
      flowchart LR
      b1[輸入數據] --> b2[SSPSR\n移位暫存器] 
      b2 --> b3[SSPBUF\n緩衝暫存器] --> b4[RAM]
      ```

- `SSPSR移位暫存器`，負責傳送數據/接收數據的暫存器，
  
  會將數據由並列轉換為串列(發送數據)或串列轉換為並列(接收數據)，是晶片內部的暫存器，使用者無法直接存取

- `SSPSTAT暫存器` (與 SPI 共用，但部分定義不同)
  - SSPSTAT暫存器 用於指示 byte 的狀態

  - `SMP位`，Slew Rate Control bit，訊號轉換率控制
    - Slew-Rate-Control，用於`中頻對雜訊的抑制`，對高頻抑制雜訊的效果有限

      i2c 操作於`低頻(100k)`時，是否開啟 Slew-Rate-Control 都沒有影響，可以不開啟，
      i2c 操作於`中頻(400k)`時，容易受到雜訊影響，建議開啟，可以改善雜訊的問題
      i2c 操作於`高頻(1M)`時，更容易受到雜訊影響，即使開啟，降低雜訊的效果也不大，需要從電路、佈線等方面改善

      注意，啟用 Slew-Rate-Control 會導致上升速度下降

      <img src="doc/mssp/i2c-slew-onoff-diff.png" width=80% height=auto>

    - SMP=1，關閉 Slew-Rate-Control，i2c 操作在 100khz  或 1Mhz 時使用
    - SMP=0，開啟 Slew-Rate-Control，i2c 操作在 400khz時使用
      
      注意，開啟 Slew-Rate-Control，會降低SlewRate同時會降低上升時間

  - `CKE位`，SMBus Select bit
    - CKE=1，開啟 SMBus 輸入
    - CKE=0，關閉 SMBus 輸入

  - `D/A位`，Data/Address bit，用於 `i2c協議`，只讀位，
    - Master-mode: 保留，未使用
    - Slave-mode:用於標記最後接收/發送數據的類型
      - D/A=1，接收/發送的最後一個 byte 是 data
      - D/A=0，接收/發送的最後一個 byte 是 address
  
  - `P位`，Stop bit，用於 `i2c協議`，是否檢測到 STOP 訊號，只讀位
    - 實務上，SCL=1，SDA為上緣(0->1) 為 STOP 訊號
    - P=1，檢測到  STOP 訊號
    - P=0，未檢測到  STOP 訊號
    - 注意，在 RESET 和 SSPEN設置為0時(未啟動i2c)，P位會被設置為0

  - `S位`，Start bit，用於 `i2c協議`，是否檢測到 START 訊號，只讀位
    - 實務上，SCL=1，SDA為下緣(1->0) 為 START 訊號
    - S=1，檢測到 START 訊號，當 START 訊號結束後，會自動將 S位清除為0
    - S=0，未在檢測到 START 訊號
    - 注意，在 RESET 和 SSPEN設置為0時(未啟動i2c)，S位會被設置為0

  - `RW位`，Read/Write，用於 `i2c協議`，只讀位
    - Slave-mode， 保存 master 設置的 RW-bit 的值
      - RW=1，Read
      - RW=0，Write
      
      - 注意，在slave位址匹配時，RW位會保存位於匹配位址後方，且由 master 提供的 RW-bit 訊息
      
      - 注意，RW位只會在以下三種控制訊號的區間內有效
        - slave 接收到 master 的數據包: START + `匹配位址` + RW + Ack + 數據 + Ack + STOP)

        - 區間1，`匹配位址 -> START`，遇到 START，代表發起新傳輸，需要設置新的RW值 
        - 區間2，`匹配位址 -> STOP`，遇到 STOP，代表舊傳輸結束，需要清除RW值
        - 區間3，`匹配位址 -> nACK`，遇到 nACK，代表當前傳輸失敗，需要清除RW值
  
    - Master-mode，用於判斷傳輸是否在進行中
      - RW=1，傳輸正在進行中
      - RW=0，傳輸並未進行

      - 注意，將 RW位分別和 SEN、RSEN、PEN、PEN、RCEN、ACKEN 進行 OR 運算，
        可以判斷 MSSP 是否處於 Idle 的狀態
        ```c
        while (SSP1STATbits.RW
          || SSP1CON2bits.SEN     
          || SSP1CON2bits.RSEN    
          || SSP1CON2bits.PEN     
          || SSP1CON2bits.RCEN    
          || SSP1CON2bits.ACKEN)  
        ```
  
  - `UA位`，Update Address (update SSPADD)，
    - 僅適用於 10-bit Slave mode only，

      在 10bit-address 模式下，slave-address 無法一次讀取完畢，
      需要借用 SSPADD位址暫存器 來保存 10bit的 slave-address

      10bit-address 會被拆分成兩個 address-byte，
      - 第1個address-byte為，`11110 + A9 + A8 + RW`，
      - 第2個address-byte為，`A7 - A0`，

      在 10bit-address 模式下，address-byte 從 SSPSR 接收後，需要寫入 SSPADD位址暫存器中，
      接收兩個 address-byte，就要寫入SSPADD位址暫存器兩次，寫入兩次才能組合成一個完整的 10bit-address，

      在收到第1個 address-byte 之後(保存在 SSPSR)，尚未寫入SSPADD之前，UA位會被設置為1，
      提醒使用者要將位址資訊從 address-byte 取出後，再寫到 SSPADD 中，寫入 SSPADD 後，UA位會自動被清除為0，
      在收到第2個 address-byte，使用同樣的處理方式
      
    - UA=1，指示使用者需要更新 SSPADD暫存器中的位址
    - UA=0，未收到 address-byte，不需要進行更新 SSPADD位址暫存器

  - `BF位`，Buffer Full Status bit，只讀位
    - 在 Transmit-mode 下，
      - BF=1，要發送的數據，SSPBUF 已接收完成，SSPBUF 已滿
      - BF=0，要發送的數據，SSPBUF 尚未完成接收，SSPBUF 為空

    - 在 Reception-mode 下，
      - BF=1，數據傳輸仍然在進行中: SSPSR已接收數據，但SSPBUF已滿，資料未被讀取
      - BF=0，數據傳輸已經結束: SSPSR已接收數據，且資料已被讀取，SSPBUF為空

- `SSPCON暫存器` (與 SPI 共用，但部分定義不同)
  - `WCOL位`，Write Collision Detect bit，僅用於 Transmit-mode，指示寫入SSPBUF暫存器是否發生衝突

    - 注意，在 master-mode 中，`不允許多個事件排隊`，必須前一個事件完成前，才能觸發下一個事件

      例如，在 START 訊號完成前，不允許發起新的 START 訊號並馬上透過寫入SSPBUF來啟動傳輸，
      在此情況下，SSPBUF 不會被寫入任何資料，並且 WCOL會被設置為1，代表寫入 SSPBUF 並沒有發生
    
    - 在 Master-Transmit-mode 
      - WCOL=1，i2c bus未處於閒置狀態時，不允許啟動傳輸，若此時 Master 嘗試寫入SSPBUF，WCOL就會被設置為1，
        若 WCOL 被設置為1，就必須透過 software 清除
      - WCOL=0，未發生衝突

    - 在 Slave-Transmit-mode
      - WCOL=1，SSPBUF 寄存器在它仍在發送前一個字時被寫入。(必須手動清0)
      - WCOL=0，未發生衝突
    
    - `在接收模式中`，Master或Slave都可以是任意值

  - `SSPOV位`，Receive Overflow Indicator bit，接收溢位指示，指示SSPBUF是否發生溢位
    - 在`接收模式下`，
      - SSPOV=1，當 SSPBUF 寄存器仍保存前一份數據時，SSPSR 已經完成數據接收，此時會產生溢位 （必用軟件清零）
      - SSPOV=0，沒有溢位

    - 在`傳輸模式下`，SSPOV 可為任意值

  - `SSPEN位`，Synchronous Serial Port Enable bit，啟動SSP模組
    - 啟動 Serial Port，啟動後，會將SDA和SCL設置為 serial port 的接腳
    - 注意，啟動前，確保在啟動前 SDA 和 SCL 的傳輸方向已經被正確的設置

  - `CKP位`，Clock Release Control Bit，控制 Slave 端釋放 SCK 的時機
    - 用於 clock-stretch，當 slave 需要更多處理時間時，會主動拉低 clock，以獲取更多的處理時間

    - Slave
      - CKP=1，Slave 端釋放 clock
      - CKP=0，保持 clock 為 Low (執行 clock-stretch)

    - Mode，未使用此控制位

  - `SSPM3:SSPM0位`，Synchronous Serial Port Mode Select bits，i2c傳輸模式選擇
    - 0110 = i2c-Slave-mode，使用 7-bit-addressing
    - 0111 = i2c-Slave-mode，使用 10-bit-addressing
    - 1110 = i2c-Slave-mode，使用 7-bit-addressing + Start/Stop-Interrupt-enable，
      遇到 START 或 STOP 訊號時，晶片會主動發出中斷通知
    - 1111 = i2c-Slave-mode，使用 10-bit-addressing + Start/Stop-Interrupt-enable，
      遇到 START 或 STOP 訊號時，晶片會主動發出中斷通知

    - 1000 = i2c-Master-mode，設置 clock = Fosc/(4 * (SSPADD + 1))
    - 1011 = i2c-Firmware-Controlled-Master-Mode (Slave Idle)

- `SSPCON2暫存器`，開關i2c的各種控制訊號

  - `SEN位`，(master) Start Condition Enabled / (slave) Stretch Enabled bit
    - Master，啟動 master 的 START 條件，
      - SEN=1，啟動 SDA / SCL 接腳上的`開始條件`，啟動後，自動由硬體清除為0
      - SEN=0，開始條件閒置中
    
    - Slave，啟動 slave 的 clock拉伸功能
      - SEN=1，啟動`slave傳輸`和`slave接收`時的 clock-stretch 功能
      - SEN=0，只啟動 `slave傳輸`時的 clock-stretch 功能 (PIC16F87X 相容)

  - `PEN位`，Stop Condition Enable bit，`限 Master 使用`，
    - 啟用 master 的 STOP Condition
    - PEN=1，啟動 SDA / SCL 接腳上的`停止條件`，啟動後，自動由硬體清除為0
    - PEN=0，停止條件閒置中
  
  - `RSEN位`，Repeated Start Condition Enabled bit，`限 Master 使用`，啟用 Master連續讀寫的開關
    - RSEN=1，啟動 SDA / SCL 接腳上的`重複啟動條件`，啟動後，自動由硬體清除為0
    - RSEN=0，重複啟動條件閒置中

  - `ACKSTAT位`，Ack Status bit，`限 Master傳輸用`，確認 master 是否收到 slave-ack 的標記位
    - 僅在 master-transmit-mode 有效，用於 master 發送位址或數據後，確認 slave 有收到
    - Master 在接收到 Slave 的 ACK 後，才會進行下一筆傳輸
    - ACKSTAT=1，Master 未收到來自 Slave 的 Ack 訊號
    - ACKSTAT=0，Master 未收到來自 Slave 的 Ack 訊號

  - `ACKEN位`，Acknowledge Sequence Enable bit，`限 Master接收用`，
    - 用於 master `接收 slave 回傳的數據後`，master 回應給 slave 的 Ack，
      若需要連續讀寫，透過 ACKDT暫存器將Ack設置為1，否則，將Ack設置為0

    - ACKEN=1，啟動 SDA 和 SCL 接腳上的 ACK 序列 (ACK-sequence)，並傳送 ACKDT位 指定的值，
      啟動後，自動由硬體清除為0
    - ACKEN=0，ACK 序列 (ACK-sequence) 閒置中

  - `ACKDT位`，Ack Data bit，`限 Master接收用`，Master發送ACK的開關
    - Master 接收完畢後，需要發送ACK給Slave，通知Slave已成功收到數據，
    - ACKDTT=1，Master 告知 Slave 接收成功
    - ACKDTT=0，Master 告知 Slave 未接收成功

  - `RCEN位`，Receive Enable bit，`限 Master 使用`，Master接收開關
    - RCEN=1，啟動 Master 接收功能
    - RCEN=0，Master 接收功能閒置
  
  - `GCEN位`，General Call Enable bit，Slave，`限 Slave 使用`，
    - 用於 slave 是否識別 master 發送的 general call address (addr = 0x0)，
      
      address = 0x0，為 I2C 的保留字節，
      當 master 呼叫的地址為 0x0，代表對所有的 i2c-device 進行呼叫，

    - GCEN=1，開啟對 general-call-address 的識別，並在收到後，發起 interrupt 進行通知
    - GCEN=0，關閉對 general-call-address 的識別

## i2c 模組的使用
- 雙緩衝的接收設計

  在`接收數據`的過程中，SSPBUF和SSPSR`形成一個double-buffered接收器`，
  當SSPSR的數據寫入SSPBUF後，SSPIF會被設置為1，
  在SSPBUF內的數據未被讀取前，SSPSR仍然可以持續接收新的數據，

  若SSPSR寫入SSPBUF前，SSPBUF仍然是滿的，則SSPSR內的數據就會被拋棄，
  造成軟體接收的數據不如預期

  `寫入操作`時，SSPBUF和SSPSR`不是double-buffered的寫入器`，
  寫入SSPBUF的操作時，一旦數據寫入SSPBUF，數據也會立刻傳送到SSPSR

- MSSP-i2c 提供了兩種定址模式
  - 7bit-addressing
  - 10bit-addressing，
      - 10bit的slave-address被拆分成兩個 address-byte，完整10bit的 slave-address 保存在 SSPADD位址暫存器中
        - 第1個address-byte為，`11110 + A9 + A8 + RW`，
        - 第2個address-byte為，`A7 - A0`，
      - slave `接收數據時(RW=0)`，master 需要提供完整 10bit的 slave-address 才能呼叫 slave
      - slave `回覆數據時(RW=1)`，master 只需要提供第一個 address-byte 就能呼叫 slave

## i2c-operate-clock 的設置 (透過 SSPADD 設置 BRG 重載值)
- ref: pg99/234 @ datasheet

- 注意，可以透過 `Mplab-Code-Configurator (MCC)` 取得 SSPADD 的值作為驗證

- BRG 重載值用來決定`clock半週期的持續時間`，因此，頻率越快，BRG值越小
  - 每個半周期都會從 SSPADD 載入BRG重載值
  - clock=1，BRG 從 SSPADD 載入重載值100後，BRG 從100開始倒數，BRG = 0，clock=1的週期結束
  - clock=0，BRG 從 SSPADD 載入重載值100後，BRG 從100開始倒數，BRG = 0，clock=0的週期結束

- 設置 SSPADD

  當 SSPM3位:SSPM0位 = 1000 時，clock 與 Fosc、SSPADD 的關係為
  > clock = Fosc/(4 * (SSPADD + 1)) 
  
  ```
  clock = Fosc/(4 * (SSPADD + 1)) 

  4 * (SSPADD + 1) =  Fosc / clock

  SSPADD + 1 = 1/4 * (Fosc / clock) = Fosc / (4*clock)

  SSPADD = ( Fosc / 4*clock ) - 1
  ```

## i2c 操作於 slave-mode
- 初始化 slave   
  - step1，slave 的 SCL 和 SDA 必須初始化設置為 input

    slave 使用 master 提供的 clock，且必須接收 master 的數據或指令後，才會輸出數據給 master，
    因此 slave-mode 下的 SCL 和 SDA 必須初始化設置為 input，當 slave 需要輸出數據時，MSSP模組會自動變更為 output

  - step2，透過 SSPCON暫存器-SSPEN位=1 啟用 MSSP模組

  - step3，透過 SSPCON暫存器-SSPM3:SSPM0位，設置的操作模式
    - master 或 slave mode
    - 7bit-address 或 10bit-address
    - slave mode 是否啟用 START / STOP 訊號的 interrupt 通知

  - 注意，step2 和 step3，會使 SCL 和 SDA 接腳`連接到 open-drain`，
    確保在執行 step1 和 step2 前，SCL 和 SDA 已經被`設置為 input`，
    並確保在晶片外部，有為 SCL 和 SDA 提供 `pull-up 電阻`，才能使 i2c 正常工作

- slave 的 `ACK` 和 `interrupt`
  - 用於
    - ACK: 用於 slave 回覆 master 已收到數據
    - interrupt: 會觸發 ISR 常式，用於讀取接收的數據，或準備要回覆給 master 的數據

  - 觸發時機，在`第9個脈波`觸發 ACK 和 interrupt，
  
    slave 會在接收了 8bit 的數據(位址或資料)後，
    - 在SCL的第9個脈波的`上緣`，slave 會根據暫存器的狀況(是否有溢味)，自動回覆 ACK 給 master，
    - 在SCL的第9個脈波的`下緣`，slave 會根據暫存器的狀況(是否有溢味)，自動觸發 interrupt，

  - 發送/不發送 `ACK 的條件`

    在`地址匹配`或`地址匹配後的數據傳輸完成接收`，硬體都會自動產生 ACK 的訊號給 master，
    並將 SSPSR移位暫存器 的內容，載入到 SSPBUF暫存器中

    只有在以下的狀況，MSSP 模組不會產生 ACK 的訊號
    - 狀況1，在接收傳輸的數據前，BF 被設置為 1，代表 SSPBUF 已滿
    - 狀況2，在接收傳輸的數據前，SSPOV 被設置為 1，代表 SSPSR 已滿
    
    在以上兩種狀況下，SSPSR 的內容都不會載入 SSPBUF 中，但是會將 PIR1暫存器-SSPIF位 設置為1。
    發生溢位後，SSPOV位需要透過軟體設置為0，並讀取 SSPBUF暫存器的內容，才能將BF位清除為0

  - 觸發/不觸發 `interrupt 的條件`

    在 slave-mode 下，以下兩種狀況，會使硬體都會產生 interrupt，並將 SSPIF 自動設置為1
    - master 呼叫的位址與 slave-address 匹配
    - 每當8bit的數據完成傳輸 (接收數據或傳遞數據都會)
    
    interrupt 的訊號，會在 SCK 的第9個脈波的下緣發出，
    也可以透過 SSPM3:SSPM0，選擇是否在 START 和 STOP 時產生 interrupt，

    只有在 slave 接收數據(RW=0)，且遇到 ACK = 1 時，不會觸發 interrupt (不會將 SSPIF設置為1)

- 10bit-address `位址匹配`的流程
  
  當MSSP模組啟用後，會等待START訊號的發生，接收到START訊號後，
  START後方8bit的數據會被移入SSPSR暫存器中，並和 SSPADD位址暫存器的值進行比較，
  若匹配，且 BF 和 SSPOV 的值都是0時，會進行以下四個操作

  - 操作1，SSPSR 的內容已經載入 SSPBUF 中
  - 操作2，將 BF 設置為1，代表 SSPBUF 已滿
  - 操作3，發送 ACK 訊號
  - 操作4，若啟用 interrupt 通知，在 SCL 的第9個下緣，將 SSPIF 設置為1

  若是 10bit-address 的定址方式，slave 需要接收兩個 address-bytes
  - 第一個byte，`11110 + A9 + A8 + RW=0`，注意，master 需要 RW 設置為 0，代表寫入
  - 第二個byte，`A7 - A0`

  10bit-address 的完整流程如下，
  - 操作1，接收第1個 address-byte，並將 SSPIF、BF、UA、設置為1)
  
  - 操作2，將 address-byte 中的`A9、A8` 的值，更新 SSPADD 的內容，並將 UA位設置為0，並釋放對 SCL 的控制

  - 操作3，讀取 SSPBUF 的內容(將BF清除為0)，將 SSPIF 清除為 0

  - 操作4，接收第2個 address-byte，並將 SSPIF、BF、UA、設置為1

  - 操作5，將 address-byte 中的 `A7-A0` 的值，更新 SSPADD 的內容，
    若位址匹配，釋放對 SCL 的控制 (UA設置為0)

  - 操作6，讀取 SSPBUF 的內容(將BF清除為0)，將 SSPIF 清除為 0
  
    (以下用於 slave 傳輸數據給 master)
  
  - 操作7，接收重複的 START訊號

  - 操作8，接收master 要存取的位址(注意，此處不是 i2c-slave-addr)，SSPIF、BF 被設置為1

  - 操作9，讀取 SSPBUF 的內容(將BF清除為0)，將 SSPIF 清除為 0

---

- slave `接收數據`的流程，`7bit-addr`，SEN=0，未啟用 clock-stretch
  - master 需要將 RW 設置為 0，表示 master 寫入指令，不需要 slave 回覆

  - 當 slave 接收 8bit 數據後，BF位被設置為1

  - 在SCL的第9個脈波的`上緣`接收 ACK 訊號，並在`下緣`設置以下訊號

  - `若 ACK訊號為0(SDA=0)`，代表 slave 接收完成，此時
    - SSPIF 被自動設置為1，進入 ISR 中，
    
    - 在 SSIF 的 ISR 中
      - 從 SSPBUF 讀取數據，BF 會自動清除為0 
      - 手動將 SSIF 清除為 0

  - `若 ACK訊號為1(SDA=1)`，代表 slave 因溢位(SSPOV=1)無法接收，等待 master 傳遞 STOP 訊號

  - 時序圖

    <img src="doc/mssp/i2c-slave-waveform-7bitAddress-Reception-SEN0.png" width=80% height=auto>

- slave `回覆數據`的流程，`7bit-addr`，自動啟用 clock-stretch
  - 注意，slave `回覆數據時(RW=1)`，master 只需要提供第一個 address-byte 就能呼叫 slave

  - 在SCL的第9個脈波的`上緣`接收 ACK 訊號，並在`下緣`設置以下訊號

  - `若 ACK訊號為0(SDA=0)，代表接收完成，slave 需要準備下一筆數據`，此時
    - CKP 被自動設置為0，立刻進入 clock-stretch，此時 SCL 保持 LOW
      
      clock-stretch 的目的，讓 slave-CPU 進入ISR 的處理常式來準備數據
      
    - SSPIF 被自動設置為1，進入 ISR 中，
    
    - 在 SSIF 的 ISR 中
      - 將要傳送的數據寫入 SSPBUF 後，BF被設置為1
      - 將 CKP 手動設置為1，關閉 clock-stretch，此時，master 會重新輸出 clock，master 也開始接收數據
      - 手動將 SSIF 清除為 0

    - 數據傳輸完畢後，BF 被自動設置為0，並重複上述步驟
	
  - `若 ACK訊號為1(SDA=1)，代表傳輸結束`，此時
      - CKP 不會被設置為 0，不會進入 clock-stretch
      - 待 master 發送 STOP 的訊號

  - 時序圖

    <img src="doc/mssp/i2c-slave-waveform-7bitAddress-Transmission.png" width=100% height=auto>

---

- slave `接收數據`的流程，`10bit-addr`，SEN=0，自動啟用 clock-stretch
  - master 需要將 RW 設置為 0，表示 master 寫入指令，不需要 slave 回覆

  - 當 slave 接收第一個address-byte後，BF位被設置為1，UA位被設置為1

  - 在SCL的第9個脈波的`上緣`接收 ACK 訊號，並在`下緣`設置以下訊號

  - `若 ACK訊號為0(SDA=0)`，代表 slave 接收完成，此時
    - CKP 不會被設置為 0，但 UA 已經被設置為1，後續由 UA 關閉 clock-stretch

    - SSPIF 被自動設置為1，進入 ISR 中，且進入 clock-stretch
    
    - 在 SSIF 的 ISR 中
      - 從 SSPBUF 讀取數據，BF 會自動清除為0 
      - 將位址資訊寫入SSPADD中，UA位自動被清除為0，clock-stretch 自動被關閉，繼續接收第2個 address-byte
      - 手動將 SSIF 清除為 0

  - `若 ACK訊號為1(SDA=1)`，代表 slave 因溢位(SSPOV=1)無法接收，等待 master 傳遞 STOP 訊號

  - 時序圖

    <img src="doc/mssp/i2c-slave-waveform-10bitAddress-Reception-SEN0.png" width=80% height=auto>

- slave `回覆數據`的流程，`10bit-addr`，自動啟用 clock-stretch
  - 注意，在 10bit-address 下，`回覆數據前，只需要第一個 address-byte 來呼叫 slave`
  
  - 在SCL的第9個脈波的`上緣`接收 ACK 訊號，並在`下緣`設置以下訊號

  - `若 ACK訊號為0(SDA=0)，代表接收完成，slave 需要準備下一筆數據`，此時
    - CKP 被自動設置為0，立刻進入 clock-stretch，此時 SCL 保持 LOW
      
      clock-stretch 的目的，讓 slave-CPU 進入ISR 的處理常式來準備數據
      
    - SSPIF 被自動設置為1，進入 ISR 中，
    
    - 在 SSIF 的 ISR 中
      - 將要傳送的數據寫入 SSPBUF 後，BF被設置為1
      - 將 CKP 手動設置為1，關閉 clock-stretch，此時，master 會重新輸出 clock，master 也開始接收數據
      - 手動將 SSIF 清除為 0

    - 數據傳輸完畢後，BF 被自動設置為0，並重複上述步驟
	
  - `若 ACK訊號為1(SDA=1)，代表傳輸結束`，此時
      - CKP 不會被設置為 0，不會進入 clock-stretch
      - SSPIF被設置為1
      - 待 master 發送 STOP 的訊號

  - 時序圖

    <img src="doc/mssp/i2c-slave-waveform-10bitAddress-Reception+Transmission.png" width=100% height=auto>

---

- 比較，接收數據 和 回覆數據的差異
  - RW 值的差異
    - 接收數據時，slave 收到 master 指定的 `RW-bit 為 0`，代表 master 要執行`寫入`操作，slave `只負責接收`
    - 回覆數據時，slave 收到 master 指定的 `RW-bit 為 1`，代表 master 要執行`讀取`操作，slave `接收之後還要回覆給 master`

  - 是否開啟 clock-stretch
    - clock-stretch 分為兩種，
      - 由`使用者手動開啟`，需要使用者手動將 `SEN 設置為 1`，
      
      - 由`晶片自動開啟`，即使在 SEN = 0，晶片仍然會啟用 clock-stretch 的功能
        - 原則1，在接收數據後，`若 CPU 需要對暫存器操作，就會花費額外的時間`，晶片會自動開啟 clock-stretch，但開啟/關閉的方式不同
        - 原則2，自動開啟的 clock-stretch 會在 interrupt 觸發時開啟，透過 CKP 或 UA 進行關閉

        - 例如，(透過 `UA` 自動開啟) 在`接收數據`時，且為` 10bit-address` 時，在將讀入的位址寫入 SSPADD 暫存器前，UA被自動設置為1，並進入 clock-stretch
          > (自動關閉) 將數據寫入 SSPADD 暫存器後，會自動關閉 clock-stretch

        - 例如，(透過 `CKP` 自動開啟) 在`回覆數據`時，需要將回復的數據寫入暫存器，此時，晶片會自動將 CKP 設置為 0，並進入 clock-stretch
          > (手動關閉) 需要手動設置 CKP = 1，才能關閉 clock-stretch
  
- 手動開啟 clock-stretch
  - 7bit-address 和 10bit-address 都實現了在數據傳輸時自動clock-stretch 的功能

  - 透過 `SSPCON2暫存器-SEN位` 可以在接收數據時，手動開啟 clock-stretch 的功能，
    一旦SEN設置為1，SCL pin 就會`在接收8bit的數據`後，強制將 SCL-bus 拉低為 LOW

  - `狀況1`，`7bit`-address / `Receive`-Mode(RW=0)，slave只接收數據 / `SEN=1`
    - 在SCL的第9個脈波的下緣，若 BF=1，則 CKP 會自動被設置為0，使得 SCL-bus=0，
    
    - 在繼續接收其他數據前，CKP 必須在 SSPIF 的 ISR 中，手動將 CKP 的值設置為1，釋放對 SCL 的控制，讓 SCL-bus 回到 1
    
    - 藉著將 SCL-bus 拉低為 LOW，使用者可以在 master 發起其他傳輸前，在 SSPIF 的 ISR 中，從 SSPBUF讀取已經接收的數據，避免發生 Overflow 
    
    - 若使用者在SCL的第9個脈波的下緣前，就讀取了 SSPBUF 的內容，使得 BF=0，CKP 就不會被設置為0，clock-stretch 也不會發生
    
    - 無論 BF 的狀態，任何時候都可以將 CKP 設置為1

    - 時序圖

      <img src="doc/mssp/i2c-slave-waveform-7bitAddress-Reception-SEN1.png" width=100% height=auto>

  - `狀況2`，`10bit`-address / `Receive`-Mode(RW=0)，slave只接收數據 / `SEN=1`
    - 在此條件下，接收完 `address` 的數據後，clock-stretch 會自動發生，但不會將 CKP 設置為0
    
    - 在SCL的第9個脈波的下緣時，若 `UA=1`，clock-stretch 會被啟動
    
    - UA會分別在接收`第1個address-byte`和接收`第2個address-byte`之後，被自動設置為1，且 RW=0，
      並在更新SSPADD後，自動將 UA設置為0，釋放對 SCL 的控制
    
    - 在 address 之後的數據接收，都會採用 `狀況1` 的 clock-stretch (使用 CKP 觸發 clock-stretch)，
      透過 `UA` 自動開啟 clock-stretch `只會發生在前兩次的 address-sequence`
  
    - 若使用者在SCL的第9個脈波的下緣前就查看了UA位，並透過更新SSPADD將UA清除為0，
      但未提前透過讀取SSPBUF將BF位清除為0，CKP 就不會被設置為 0，
      基於 CKP 的 clock-stretch 是根據 BF 的狀態，且只會發生在數據傳輸階段，不會發生在位址傳輸階段

    - 時序圖

      <img src="doc/mssp/i2c-slave-waveform-10bitAddress-Reception-SEN1.png" width=100% height=auto>

    - 參考，[slave 接收數據的流程，10bit-addr，SEN=0，自動啟用 clock-stretch]() 的時序圖
  
  - `狀況3`，`7bit`-address / `Transmit`-Mode(RW=1)，slave回覆數據，`不使用 SEN`
    - 在SCL的第9個脈波的下緣，且BF=0，會自動將 CKP 清除為0，以進入clock-stretch，不需要將 SEN設置為1
    - 當 interrupt 被觸發(SSPIR=1) 進入中斷常式後，必須在允許繼續傳輸前，將 CKP 設置為1，否則 master 會一直等待
    - 藉著 clock-stretch 將 SCL-bus 拉低為LOW，使用者就可以在 master 啟動新的傳輸前，在中斷常式中讀取SSPBUF讀取已接收的數據，並將BF清除為0
    - 若使用者在SCL的第9個脈波的下緣前，就讀取了 SSPBUF 的內容，使得 BF=0，
      CKP 就不會被設置為0，clock-stretch 也不會發生
    - 無論 BF 處於何種狀態，都可以將CKP設置為1
    - 參考，[slave 回覆數據的流程，7bit-addr`，自動啟用 clock-stretch]()

  - `狀況4`，`10bit`-address / `Transmit`-Mode(RW=1)，slave回覆數據，`不使用 SEN`
    - 和狀況2一樣，clock-sketch 是由前兩個接收address-sequence的`UA位`所控制
    - 在接收完兩個address-sequence後，第三個address-sequence包含 第一個 address-byte，和被設置為1的RW，
    - 第三個address-sequence後，UA不會被設置為1，而是和`狀況3`一樣，clock-stretch 由 BF 所控制
    - 參考，[slave 回覆數據的流程，10bit-addr，自動啟用 clock-stretch]()
  
- 和 master 進行同步時脈的 clock-stretch
  - 當CKP被設置為0，SCL-bus 不會立刻被拉低為0，只有在 slave 檢測到 SCL-bus 為0時，才會將主動將 SCL設置為0，
    因此，需要等到 master 已經將 SCL-bus 拉低為0，slave 才會觸發對 SCL 的控制，使 clock-stretch 生效

  - 在 CKP 未被設置為1時，master 對 SCL-bus 沒有控制權，即使 master 放棄對SCL的控制，SCL-bus 也不會回到1，
    此時，因為 slave 依然控制著 SCL，並使得 SCL-bus 為 0

  - 在 CKP 被設置為1時，不需要等待 master，slave 會放棄對 SCL 的控制，使 SCL-bus 立刻回到 1

  - 圖解

    <img src="doc\mssp\i2c-clock-synchronization-for-CKP.png" width=70% height=auto>
    
  
- General-call-address 呼叫所有 i2c-slave-devices

  - General-call-address 是 i2c 保留的特殊位址，理論上，所有 i2c-slave-device 收到 0x0 的位址呼叫時，
    都需要回復 ACK=0

  - 要使 PIC 能夠識別 0x0 的位址，需要設置 GCEN=1

  - 時序圖

    <img src="doc/mssp/i2c-slave-waveform-00h.png" width=70% height=auto>

## i2c 操作於 master-mode
- 概述

  - 藉由設置 `SSPCON暫存器-SSPM0位:SSPM3位`，並將 `SSPCON暫存器-SSPEN位` 設置為1，在 master-mode 中，MSSP 模組會對 SCL-bus 和 SDA-bus 進行操作

  - master 模式支援在偵測到 START(S) 和 偵測到 STOP(P) 時產生 interrupt

  - 在 reset 和 MSSP模組關閉時，START(S)和STOP(P)是關閉的，

  - 當STOP(P)被設置為1，或是bus是idle為閒置(START和STOP都是0)時，master 對 i2c-bus 的控制有可能會被拿走

  - 在 FW-Controlled-master-mode 中，使用者的代碼需要根據 START 和 STOP 的條件，執行所有 i2c-bus 的操作

  - master-mode 下，觸發 interrupt 的條件
    - 條件1，發起 Start-Condition
    - 條件2，發起 Stop-Condition
    - 條件3，數據傳輸已完成發送，或已完成接收
    - 條件4，發送 Ack
    - 條件5，發起 Repeated-Start-Condition

  - master-mode 不支援操作排隊(not allow queueing of events)，
    
    必須等到前一個操作完畢後，才能進行下一個操作，否則就會觸發衝突，
    例如，不能啟用First-START-condition(執行SEN=1)後，立刻執行寫入SSPBUF的操作(執行SSPBUF=data)，

- 在 master-transmit-mode 的時序流程
  - 在 SCL 輸出 clock 時，數據會透過 SDA 輸出，
  - START-condition 的輸出，用於指示 slave 開始傳輸
  - 第一個傳送的byte，包含 7bit 的 slave-address + `RW=0`，
  - 數據每次傳送 8bits，
  - 每當 8bits 的數據完成`傳送`後，會從 slave `接收`到一個 ACK，
  - STOP-condition 的輸出，用於指示 slave 結束傳輸

- 在 master-receive-mode 的時序流程
  - 在 SCL 輸出 clock 時，數據會透過 SDA 輸出，
  - START-condition 的輸出，用於指示 slave 開始傳輸
  - 第一個傳送的byte，包含 7bit 的 slave-address + `RW=1`，
  - 數據每次傳送 8bits，
  - 每當 8bits 的數據完成`接收`後，會`發送`一個 ACK 給 slave，
  - STOP-condition 的輸出，用於指示 slave 結束傳輸
  
- master-transmit 操作流程
  - step1，使用者將 `SSPCON2暫存器-SEN`設置為1，用於產生 `START`-condition
  - step2，`SSPIF暫存器`被設置為1，在其他操作發生前，MSSP模組會等待一段 START-time
  - step3，將 slave-address 寫入 `SSPBUF暫存器`，用於發送
  - step4，slave-address 被移出到 SDA pin，直到所有8bit的發送完成
  - step5，MSSP模組將 slave 發送的 ACK-bit 移入，並將值寫入 `SSPCON2暫存器-ACKSTAT位` 中
  - step6，MSSP模組在第9個clock-cycle的下緣，將SSPIF設置為1，發起中斷，代表 slave 已接收
  - step7，使用者將要發送的8bit指令寫入`SSPBUF暫存器`
  - step8，數據依序被轉移到 SDA pin，直到所有 8bit 都被發送完成
  - step9，MSSP模組將 slave 發送的 ACK-bit 移入，並將值寫入 `SSPCON2暫存器-ACKSTAT位` 中
  - step10，MSSP模組在第9個clock-cycle的下緣，將SSPIF設置為1，發起中斷，代表 slave 已接收
  - step11，使用者將 `SSPCON2暫存器-PEN`設置為1，用於產生 `STOP`-condition
  - step12，一旦 STOP-condition 完成，會自動發起中斷

- 設置 clock 和 Baud-Rate-Generator(BRG)，
  
  <img src="doc/mssp/i2c-master-brg-block-diagram.png" width=60% height=auto>
  
  - 用於 SPI mode 的 BRG(baud-rate-generator) 同樣用來設置 SCL 的頻率是 100khz、400khz、1Mhz 其中一種

    <img src="doc/mssp/i2c-clock-with-brg.png" width=80% height=auto>

  - 在 master-mode 下，BRG 的重載值(reload-value)，會放在`SSPADD暫存器的低七位`(bit6-bit0)，
    當數據寫入SSPBUF，BRG 會自動開始向下計數，計數到0之後停止，除非將重載值重新載入到 BRG 中

  - BRG 在每個指令週期(Tcy)會遞減兩次，分別在Q2和Q4時遞減一次

  - SCL 接腳輸出的 clock 會經過兩輪的 BRG 計數(two-rollovers)
    
    <img src="doc/mssp/i2c-brg-two-roller-count.png" width=80% height=auto>

    - 在 clock 為 HIGH 的時間內，BRG 會進行第1輪的計數，並從最大值向下計數到0，
    - 在 clock 由 HIGH 轉為 LOW 時，BRG 從 SSPADD暫存器中重新載入最大值，
    - 在 clock 為 LOW 的時間內，BRG 會進行第2輪的計數，並從最大值向下計數到0，

  - 只要給定的工作完成，例如，傳輸的最後一位數據位有ACK跟隨，內部的 clock 就會自動計數，並且SCL會保持在最後的狀態

- 一旦 master mode 啟動，使用者可以有以下六個操作
  - 選項1，(啟動) 對 SDA-bus 和 SCL-bus 發起 START 訊號
  - 選項2，(啟動) 對 SDA-bus 和 SCL-bus 發起 Repeated-START 訊號

  - 選項3，(傳輸-寫入) 啟動 slave-address 或 數據的傳輸，並將資料寫入 SSPBUF暫存器中
  - 選項4，(傳輸-接收) 配置 i2c port，以接收數據
  - 選項5，(傳輸-接收) 在接收數據的尾端後(接收8bit的數據後)，產生 Ack 訊號

  - 選項6，(結束) 對 SDA-bus 和 SCL-bus 發起 START 訊號

- `時序圖`，SCL仲裁(clock-arbitration) 的時序圖，

  <img src="doc/mssp/i2c-brg-reload-timing.png" width=80% height=auto>

  - 注意，時脈線仲裁(clock-arbitration) 和 匯流排仲裁(bus-arbitration) 是不同的
    - clock-arbitration，用於 master 輸出 clock 時，遇到 slave 執行 clock-stretch 造成 clock 狀態的衝突時，BRG 的行為
    - bus-arbitration，用於多個 master 對 bus 的競爭

  - 使用場景

    clock-arbitration 發生於 master 處於接收、發送，或是連續的 START/STOP condition 時，
    master `放棄對 SCL 的控制，允許 SCL 回到 HIGH` 時，SCL 因 slave 使用` clock-stretch 主動將 SCL 拉低為 LOW` 造成的衝突
    
  - 處理方式

    一般狀況下，master 在 BRG 計數到0 > 放棄對 SCL 的控制，允許 SCL 回到 HIGH > `master 檢查 SCL-bus 的狀態`

    - `狀況1`，若 SCL-bus `已經回到 HIGH`，代表`沒有 clock-stretch` 的發生，
      - 1-1，重新載入 BRG 的值，
      - 1-2，重新開始向下計數

    - `狀況2`，若 SCL-bus，`依然維持在 LOW`，代表 slave 執行 clock-stretch，
      - 2-1，繼續檢測 SCL-bus 是否為 LOW，若是，BRG 保持停止
      - 2-2，SCL-bus 已經回到 HIGH，代表 clock-strech 已經結束
        - 2-2-1，重新載入 BRG 的值，
        - 2-2-1，重新開始向下計數

- `時序圖`，啟動 First-START(S) 的時序 (單次讀寫)
  - First-START-condition 的流程
    - 設置 SEN=1
    - 保持期，SCL=SDA=1，應保持 TBRG 的時間
    - 觸發 First-START-condition，SCL=1，SDA=1->0，
	
  - `step1`，設置`SSPCON2暫存器-SEN=1`，開始啟動 First-START-condition
    
    若 SDA-bus 和 SCL-bus 取樣為 1，代表 bus 為閒置狀態，此時，將 SSPADD 的值重新載入 BRG，並等待 T<sub>BRG</sub> 的時間，讓 BRG 設置完成
  
  - `step2`，在 SCL-bus 為 1 時，`將 SDA 拉低為 LOW`，並設置S位

    SCL=1，SDA 由 HIGH -> LOW，代表 First-START 的控制訊號，

    SDA 拉低為 LOW 的期間，`SSPSTAT暫存器-S位`會被設置為1，
    代表 First-START-condition 正在處理中，兩個步驟會在 T<sub>BRG</sub> 的時間內完成

    First-START 結束後，`SDA 和 SCL 都拉低為 LOW`，待 BRG 設置完成後，SCL 開始輸出 clock
    - SDA 拉低為 LOW，代表 master 控制 SDA-bus，由 master 控制數據輸出
    - SCL 拉低為 LOW，代表 master 控制 SCL-bus，由 master 控制clock輸出

  - `step3`，清除S位 + 發出中斷

    在 T<sub>BRG</sub> 的時間後，First-START-condition 發送完成後，
    - 硬體自動設置`SSPCON2暫存器-SEN=1->0`，
    - BRG 被暫停，
    - SDA 維持在 LOW 的狀態，
    - SSPIF 會被設置為 1，用於發起中斷，
      
      使用者在ISR中，將要發送的數據寫入`SSPBUF暫存器`中，
      代數據完成寫入後，開始將 SSPBUF 中的資料從 SDA 接腳依序移出(shift-out)

  - 注意，First-START-condition 期間的`匯流排衝突(BCLIF)` 
    - 狀況1，若在 First-START-condition `開始前`，SDA-bus=0 和 SCL-bus=0
    - 狀況2，在 First-START-condition 的階段中，在 SDA 被 master 拉低為 LOW 前，SCL-bus 轉變為 LOW，
    - 以上兩種狀況代表發生 bus 發生衝突(bus-collision)，`PIR2暫存器-BCLIF位`會被設置為1，並且中斷 First-START-condition

  - 注意，First-START-condition 期間的`寫入衝突(WCOL)`
    
    若在 First-START-condition 未完成前，使用者執行寫入 SSPBUF 的操作，
    就會發生寫入衝突，WCOL 會被設置為1，且寫入的行為並不會發生。

  - 注意，First-START-condition 結束前，禁止寫入以下控制位

    由於 master-mode 不支援操作排隊，在 First-START-condition 結束前，
    SSPCON2暫存器的低5位控制位，ACKEN、RCEN、PEN、RSEN、SEN 是不能進行設置的，s直到 First-START-condition 完成。
  
  - 完整 First-START-condition 的時序圖

    <img src="doc/mssp/i2c-master-first-start-condition-timing.png" width=80% height=auto>

- `時序圖`，啟動 Repeat-START(RS) 的時序 (連續讀寫)
  - Repeat-START-condition 的流程
    - 設置 RSEN=1，啟動 Repeat-START-condition
    - 控制 SCL，使 SCL=0，用於重置BRG
    - SDA=0->1，BRG開始計數，SDA=1需維持TBRG的時間
    - SCL=0->1，BRG開始計數，SCL=1需維持TBRG的時間
    - SCL=1，SDA=1->0，需維持TBRG的時間，時間結束後, SDA保持為LOW

  - `step1`，設置 `SSPCON2暫存器-RSEN位=1`，並驅使 SDA 和 SCL 回到 idle 狀態 (SDA=SCL=1)

    - 1-1，將 SCL 拉低為 LOW，當 SCL 檢測為 LOW 時，將 SSPADD暫存器的值重新載入 BRG 中，並開始計數

    - 1-2，master 釋放 SDA 的控制，`使 SDA 回到 HIGH`，並占用 1個 T<sub>BRG</sub> 的時間，
      等待 BRG 的計數完畢，檢測到 SDA 回到 HIGH，若是，開始驅動 SCL

    - 1-3，使 SCL 回到 HIGH 的狀態，占用 1個 T<sub>BRG</sub> 的時間，
      當檢測到 SCL 為 HIGH，BRG會從 SSPADD暫存器中重新載入重載值，並重新開始計數
    
  - `step2`，在 SCL-bus 為 1 時，`將 SDA 拉低為 LOW`，並設置S位

    SCL=1，SDA 由 HIGH -> LOW，代表 Repeat-START 的控制訊號，

    SDA 拉低為 LOW 的期間，`SSPSTAT暫存器-S位`會被設置為1，
    代表 Repeat-START-condition 正在處理中，兩個步驟會在 T<sub>BRG</sub> 的時間內完成

  - `step3`，清除S位+發出中斷

    在 T<sub>BRG</sub> 的時間後，Repeat-START-condition 發送完成，
    硬體自動設置`SSPCON2暫存器-RSEN=1->0`，代表 Repeat-START-condition 啟動完成，
    
    將 SSPIF 設置為 1，發起中斷，使用者在ISR中，將要發送的數據寫入`SSPBUF暫存器`中，
    代數據完成寫入後，開始將 SSPBUF 中的資料從 SDA 接腳依序移出(shift-out)

  - 注意，若其他操作正在進行時，將 RSEN 設置為1，則 RSEN 並不會起作用

  - 注意，Repeat-START-condition 期間的`匯流排衝突(BCLIF)` 
    - 狀況1，當 SCL=0->1，檢測出 SDA 為 LOW
    - 狀況2，在 master 主動將 SDA 拉低為 LOW 前，SCL 檢測為 LOW，
      代表有其他 master 正嘗試傳輸數據
    
  - 注意，Repeat-START-condition 結束前，禁止寫入以下控制位

    由於 master-mode 不支援操作排隊，在 Repeat-START-condition 結束前，
    SSPCON2暫存器的低5位控制位，ACKEN、RCEN、PEN、RSEN、SEN 是不能進行設置的，直到 Repeat-START-condition 完成。

  - 完整 Repeat-START 的時序圖

    <img src="doc/mssp/i2c-master-repeat-start-condition-timing.png" width=80% height=auto>

- `時序圖`，ACK 的時序圖
  - 透過設置 `SSPCON2暫存器-ACKEN=1`，啟動 ACK序列 (Acknowledge-sequence)
  
  - `step1`，設置 ACKDT暫存器，
    - 若要發送 Acknowledge，設置 ACKDT = 0，
    - 若要發送 non-Acknowledge，設置 ACKDT = 1，
  
  - `step2`，設置 ACKEN=1，
    
    此時，master 會控制 SCL，`使 SCL 被拉低為0`，並將 ACKDT暫存器的值發送到 SDA 接腳上，
    此時，BRG 會開始計數，並等到 T<sub>BRG</sub> 的時間後，master 會釋放對 SCL 的控制，使 SCL 回到 1，

    此段時間為 ACK 的設置時間

  - `step3`，slave讀取ACK + 關閉 ACKEN
    
    當檢測到 SCL 為1後，BRG 會再度開始計數，並等到 T<sub>BRG</sub> 的時間後，再度將 SCL 拉低為0，
    隨後，ACKEN 會自動被設置為 0，BRG 關閉，MSSP模組恢復到 IDLE 的狀態 (沒有其他操作進行中)

    此段時間為 ACK 的穩定時間，用於提供 slave 讀取 ACK 的狀態

  - 完整 ACK 的時序圖

    <img src="doc/mssp/i2c-master-ack-condition-timing.png" width=80% height=auto>

  - `相關旗標`，WCOL 旗標

    當ACK序列正在進行，使用者又將資料寫入 SSPBUF 時，就會發生寫入衝突，
    此時，WCOL 會自動被設置為1，且寫入 SSPBUF 的操作不會執行，

    注意，當 WCOL被設置為1後，就只能透過 software 清除為0

- `時序圖`，STOP(P) 的時序圖
  - 注意，STOP-condition 的條件為 SCL=1，SDA=0->1(上緣)

    因此，在 SCL 設置為 1 之前，需要提前將 SDA 設置為 LOW

  - STOP-condition 的流程
    - 設置 PENE=1，
    - 設置 SDA，SDA=1->0，須維持 TBRG 的時間
    - 設置 SCL，SCL=0->1，須維持 TBRG 的時間
    - 觸發 STOP-condition，SCL=1，且 SDA=0->1，須維持 TBRG 的時間

  - `step1`，在SCL的第9個脈波的下緣，master 偵測到 ACK=1

    ACK=1，slave 發生異常，master 需要傳送 STOP-condition 通知 slave 停止 
  
  - `step2`，在 ISR 中，設置 `SSPCON2暫存器-PEN=1`，啟動 stop-condition

    設置 PEN=1 後，使得 master 自動將 SDA 拉低為 LOW
  
  - `step3`，設置 SDA 為 LOW
    
    SDA 拉低為 0 後，需要確認 SDA 是否已經為 LOW，
    若 SDA 已經為 LOW，BRG 會開始計數，並維持一段 T<sub>BRG</sub> 的時間

  - `step4`，設置 SCL 為 HIGH

    當前一段 SDA 的時間到達後，會自動將 SCL 拉高為 HIGH，BRG 會重新開始計數，
    並維持一段 T<sub>BRG</sub> 的時間

  - `step5`，設置 STOP-condition

    當前一段 SCL 的時間到達後，會將 SDA 釋放，讓 SDA 重新為到 HIGH，
    當 SDA 偵測到為HIGH後，`SSPSTAT暫存器-P位`會自動被設置為1，並維持一段 T<sub>BRG</sub> 的時間，
    當 T<sub>BRG</sub> 的時間到達後，`SSPCON2暫存器-PEN位`會自動被清除為0，代表 STOP-condition 已經結束

  - 完整 STOP 的時序圖

    <img src="doc/mssp/i2c-master-stop-condition-timing.png" width=80% height=auto>

  - `相關旗標`，WCOL 旗標

    當STOP序列正在進行，使用者又將資料寫入 SSPBUF 時，就會發生寫入衝突，
    此時，WCOL 會自動被設置為1，且寫入 SSPBUF 的操作不會執行，

    注意，當 WCOL被設置為1後，就只能透過 software 清除為0

- master-mode，發送數據 (Transmission)
  - `step1`，master `寫入 address-byte`
    - 要傳送 7bit/10bit-address 的 address-byte，只需要將數據寫入 SSPBUF暫存器中，
    - 寫入後，自動將 BF 設置為1，
    - 寫入後，BRG 開始計數，並開始下一輪的傳輸
    - master 會在 SCL 拉低為 LOW (SCL=1->0，下緣) 時，將 address/date byte中的每一個bit，從 SDA-pin 送出，
      - 送出前，SDA 需要`維持前一個狀態`一段時間 (data-input-holding-time)，最大值為 0.9us，
      - 藉著`變更 SDA 的狀態`實現送出數據，此段時間為數據設置時間(data-input-setup-time)，最小值為 100ns
      - 在SDA維持+變更的期間，SCL 會維持一個 T<sub>BRG</sub> 的時間，等待 SDA 上的數據變更完成
      - data-input-holding-time / data-input-setup-time 示意圖

        <img src="doc/mssp/i2c-master-tranmit-sda-timing.png" width=80% height=auto>

  - `step2`，slave `讀取數據 + 回復 ACK`
    - master 放棄對 SCL 的控制使`SCL=1`，並維持一個 T<sub>BRG</sub> 的時間，`等待 slave 讀取 SDA 上的數據`，期間，SDA 的狀態必須保持穩定，並在SCL下緣(SCL=1->0)後，有保持一段時間
    
    - master 在 8bit 的數據都移出後，`BF 會自動設置為0`，在第8個脈波的下緣，master 會放棄對 SDA 的控制，`使 SDA=1`，master 釋放對 SDA 的控制，slave 才能利用 SDA 回復 ACK-bit 的訊號

    - master 在第9個脈波的下緣，對 SDA 進行採樣以獲取 slave 回復的 ACK-bit，並將ACK-bit的值寫入`ACKDT位`，若ACK=0，`ACKSTAT位`被設置為0，否則 ACKSTAT位=1
  
  - `step3`，master `寫入 data-byte`，
    > 參考，step1 和 step2

  - master 發送數據時序圖
    
    <img src="doc/mssp/i2c-master-tranmit-mode.png" width=80% height=auto>

  - `相關旗標`，BF 旗標

    在 master-transmission，當資料寫入 SSPBUF (SSPBUF=資料)後，BF自動設置為1，
    當 8bit 的資料都從 SSPSR 移出後 (SSPSR為空)，BF自動設置為0

  - `相關旗標`，WCOL 旗標

    當傳輸正在進行時，使用者又將資料寫入 SSPBUF 時，就會發生寫入衝突，
    此時，WCOL 會自動被設置為1，且寫入 SSPBUF 的操作不會執行，

    注意，當 WCOL被設置為1後，就只能透過 software 清除為0

  - `相關旗標`，ACKSTAT 旗標

    當 slave 發送 Acknowledge(ACK=0)，ACKSTAT 自動設置為0，
    當 slave 發送 non-Acknowledge(ACK=1)，ACKSTAT 自動設置為1，
    
    slave 只有在以下兩種狀況會回復 ACK
    - slave 只有在識別 bus 上的 address 是自己的
    - slave 完整的接收了來自 master 的數據後

- master-mode，接收數據 (Reception)
  - `step1`，設置RCEN位，以啟用接收
    要啟用接收，需要設置 `SSPCON2暫存器-RCEN位=1`，

    注意，在設置 SSPCON2暫存器-RCEN=1 前，必須確定 MSSP模組是處於 IDLE 狀態，
    否則 RCEN的設置會被忽略

  - `step2`，BRG 開始計數，
    
    BRG 會在以下三種狀況下開始計數
    - 在每一個 SCL-rollover(SCL=0 或 SCL=1)
    - SCL 的狀態改變 (SCL=1->0，SCL=0->1)
    - 每一個 bit 被移入 SSPSR

  - `step3`，接收完畢，MSSP模組進入 IDLE 狀態

    在第8個脈波下緣，`SSPCON2暫存器-RCEN位`會自動被清除為0，
    - SSPSR暫存器的內容，會被寫入SSPBUF
    - BF位被自動設置為1
    - SSPIF位被自動設置為1，觸發中斷，並進入 ISR 中斷處理常式中
    - BRG 暫停計數，
    - SCL 保持為 LOW
    - 執行上述操作後，SSP模組處於 IDLE 狀態，並等待下一個命令

  - `step4`，手動讀取接收的內容
    
    使用者讀取 SSPBUF 的內容後，BF 自動被清除為0  

  - `step5`，master 發送 ACK 給 slave

    在接收數據結束時，藉著設置`SSPCON2暫存器-ACKEN位`，發送 ACK 給 slave

  - master 接收數據時序圖
    
    <img src="doc/mssp/i2c-master-reception-mode.png" width=80% height=auto>

  - `相關旗標`，BF 旗標
    
    在 master-reception，當讀取的資料從 SSPSR 載入 SSPBUF 後，BF自動設置為1，
    在使用者讀取 SSPBUF 的內容後，BF自動設置為0

  - `相關旗標`，SSPOV 旗標

    當 SSPSR 已經寫入 8bit 後，BF 仍然為1，SSPOV 就會自動被設置為1

  - `相關旗標`，WCOL 旗標

    當接收正在進行中( SSPSR 仍然在接收數據 )，使用者又進行 寫入 SSPBUF 的操作時，
    WCOL 就會自動被設置為1，且寫入 SSPBUF 的操作不會發生

    注意，當 WCOL被設置為1後，就只能透過 software 清除為0
  
- `時序圖`，SDA仲裁(bus-arbitration) 的時序圖

## multi-master 的 SDA 仲裁 (bus-arbitration)
- 概述
  - 在 multi-master 模式下，可以在 START-condition 和 STOP-condition 產生的 interrupt 中，決定是否釋放對 bus 的控制

  - 在`Reset`或`MSSP模組關閉`時，`SSPSTAT暫存器-S位`和`SSPSTAT暫存器-P位`都是清除為0的

  - 當 `SSPSTAT暫存器-P位=1` 或是 `bus處於閒置狀態且S位和P位都是0時`，I2C 的控制權是有可能被拿走的，

  - bus 忙碌時，致能的SSP中斷會在 STOP-condition 發生時產生中斷
  
  - 在 multi-master 模式下，為了仲裁就必須對 SDA 行監測，
    監測的方式是在硬體方面執行檢查，並將仲裁的結果放在`PIR2暫存器-BCLIF位` 

  - 如何判斷仲裁是否發生
    
    當 master 在 SDA pin 輸出 address/data 時，仲裁會發生在 `masterA` 釋放對 SDA 的控制`使 SDA-bus 輸出狀態為 HIGH`時，
    而另一個 `masterB` 控制 SDA `使 SDA-bus 輸出為 LOW`

    當 masterA 放棄對 SDA 的控制時，會`預期 SDA-bus 的狀態為 HIGH`，然而，實際對 SDA-bus 檢測時，
    卻發生 `SDA-bus 的實際狀態為 LOW` (因 masterB 的控制造成)，
    
    兩者的差異，使得 masterA 知道 bus-collision，此時，master 會設置`PIR2暫存器-BCLIF位=1`，
    並將 i2c 的接腳重置，恢復到 idle 的狀態

  - SDA的仲裁有可能會發生在以下幾個狀況
    - 狀況1，在 slave-address-byte/data-byte 傳送時發生仲裁
    - 狀況2，在 First-START-contion 發生仲裁
    - 狀況3，在 Repeat-START-condition 發生仲裁
    - 狀況4，在 Stop-condition 發生仲裁
    
- `狀況1`，在 `slave-address-byte/data-byte` 數據傳送時發生仲裁
  - master 在`SCK=0`時，開始對SDA進行控制，並設置SDA-bus的數據，
  - master 在`SCK=1`時，放棄對SDA的控制，`讓 slave 可以讀取數據`，同時，`master 可以檢測 SDA 是否發生衝突`，
    注意，若不是放對SDA的控制，就無法實現 multi-master 的功能
  
  - 時序圖

    <img src="doc/mssp/i2c-bus-collision-on-transmit.png" width=80% height=auto>

- `狀況2`，在 `First-START-contion`發生仲裁
  - First-START-condition 的條件
    - 設置 SEN=1
    - 保持期，SCL=SDA=1，應保持 TBRG 的時間
    - 觸發 First-START-condition，SCL=1，SDA=1->0，

  - 在 First-START-condition 期間發生的衝突有三種
    - 衝突1，SDA 發生衝突，SDA在保持期`前`，被拉低為LOW
    - 衝突2，SCL 發生衝突，SCL在保持期中，被拉低為LOW
    - 衝突3，SDA 發生衝突，SDA在保持期`中`，被拉低為LOW (提前觸發)
  
  - `衝突1`，SDA 發生衝突，SDA在保持期`前`，被拉低為LOW

    在 First-START-condition 的啟動初期(SEN=1設置後，SDA拉低前)，
    SDA 檢測到被拉低為 LOW，代表發生 bus-collision，
    有其他 master 正在使用 SDA-bus

    <img src="doc/mssp/i2c-bus-collision-on-start-sda.png" width=80% height=auto>

  - `衝突2`，SCL 發生衝突，SCL在保持期中，被拉低為LOW

    SEN=1設置後，進入 First-START-condition 的啟動序列，
    master 會放棄對SCL和SDA的控制，使SCL=SDA=1，
    若檢測到 SDA 已經是1，BRG 就會開始向下計數到0(花費TBRG的時間)，
    
    若在BRG計數期間，SDA=1，但SCL檢測到被拉低為 LOW，代表發生 bus-collision

    <img src="doc/mssp/i2c-bus-collision-on-start-scl.png" width=80% height=auto>

  - `衝突3`，SDA 發生衝突，SDA在保持期`中`，被拉低為LOW (提前觸發)

    正常情況下，SEN設置為1後，SDA和SCL應維持HIGH至少TBRG的時間，

    在此段時間內，SDA被其他master拉低，START 被其他 master 提前觸發，
    使得 SDA=SCL=1 的持續時間少於 TBRG，
    因 SDA和 SCL 的狀態沒有發生衝突，BCLIF不會被觸發為1，

    但是 `BRG 會在 START 提前觸發後被重置`，確保 START 能維持 T<sub>BRG</sub> 的時間

    <img src="doc/mssp/i2c-bus-collision-on-start-brg.png" width=80% height=auto>

- `狀況3`，在 `Repeat-START-condition` 發生仲裁
  - Repeat-START-condition 的條件
    - 設置 RSEN=1，啟動 Repeat-START-condition
    - 控制 SCL，使 SCL=0，用於重置BRG
    - SDA=0->1，BRG開始計數，SDA=1需維持TBRG的時間
    - SCL=0->1，BRG開始計數，SCL=1需維持TBRG的時間
    - SCL=1，SDA=1->0，需維持TBRG的時間，時間結束後, SDA保持為LOW

  - 在 Repeat-START-condition 期間發生的衝突有2種
    - 衝突1，SDA 發生衝突，當 SCL=0->1 時，SDA 被檢測出為 LOW
    - 衝突2，SCL 發生衝突，在 SDA=1->0 之前，SCL 被檢測出為 LOW

  - `衝突1`，SDA 發生衝突，當 SCL=0->1 時，SDA 被檢測出為 LOW

    SDA=0->1，BRG開始計數，在 SDA=1 維持了 TBRG 的時間後，
    釋放對 SCL 的控制，使得 SCL=0->1，當檢測到 SCL 為 HIGH 時，會對 SDA 的狀態進行檢測，
    
    - 若 SDA 為 LOW，代表 bus-collision 發生
    - 若 SDA 為 HIGH，代表 bus-collision 未發生，SDA=SCL=1，BRG重載後開始倒數，並維持 TBRG 的時間
      
      注意，若在倒數期間，SDA 被其他 master 拉低為 LOW，並不會產生 bus-collision，
      不會有兩個 master 同時將 SDA 拉低的情況發生


    <img src="doc/mssp/i2c-bus-collision-on-repeat-start-sda.png" width=80% height=auto>

  - `衝突2`，SCL 發生衝突，在 SDA=1->0 之前，SCL 被檢測出為 LOW
    
    在 SDA=1->0 之前，當 SDA=1，且位於 SCL=1 的計數期間，
    若 SCL 被檢測被拉低為LOW，就會發生 bus-collision

    <img src="doc/mssp/i2c-bus-collision-on-repeat-start-scl.png" width=80% height=auto>
    

- `狀況4`，在 `STOP-condition` 發生仲裁
  - STOP-condition 的條件
    - 設置 PENE=1，
    - 設置 SDA，SDA=1->0，須維持 TBRG 的時間
    - 設置 SCL，SCL=0->1，須維持 TBRG 的時間
    - 觸發 STOP-condition，SCL=1，且 SDA=0->1，須維持 TBRG 的時間

  - 在 STOP-condition 期間發生的衝突有2種
    - `衝突1`，SDA 發生衝突，在 STOP-condition 階段，SDA 未回到HIGH
      
      在觸發 STOP-condition 階段，正常狀況下，SCL=1，
      SDA 會被 master 釋放控制並回到 HIGH，
      
      在觸發 STOP-condition 階段結束後執行檢查，
      發現 SDA 被其他 master 占用，使得 SDA 保持在 LOW，造成 bus-collision
      
      <img src="doc/mssp/i2c-bus-collision-on-stop-sda.png" width=80% height=auto>

    - `衝突2`，SCL 發生衝突，在 STOP-condition 階段之前，SCL 已經被拉到 LOW

      在設置 SCL 期間，正常狀況下，SDA 會被 master 釋放控制並回到 HIGH，(SCL=0->1)，但在此期間，SCL 被其他 master 控制回到 LOW 的狀態
      
      在觸發 STOP-condition 階段結束後執行檢查，
      發現 SCL 被其他 master 占用，使得 SCL 保持在 LOW，造成 bus-collision

      <img src="doc/mssp/i2c-bus-collision-on-stop-scl.png" width=80% height=auto>

## 範例代碼
- i2c 初始化
  ```c
  #define SSPEN_ON 1<<5               
  #define SSPM_MASTER 1<<3            
  #define SMP_SLEW_CONTROL_ON 0<<7
  #define SMP_SLEW_CONTROL_OFF 1<<7   
  #define SMB_ON 1<<6 0x40
  #define SMB_OFF 0<<6

  void i2c_init(void)
  {
      // ==== 啟動 MSSP模組, 設置 MSSP模組為 i2c-master-mode ====
      // WCOL=0 SSPOV=0 SSPEN=1 CKP=0 SSPM3=1 SSPM2=0 SSPM1=0 SSPM0=0 @ p84/p234 
      SSPCON = SSPEN_ON | SSPM_MASTER; //0b0010_1000;
      
      // ==== 將所有開關清除為0 ====
      //GCEN=0 ACKSTAT=0 ACKDT=0 ACKEN=0 RCEN=0 PEN=0 RSEN=0 SEN=0 @ p85/p234
      SSPCON2 = 0x00;

      // ==== 透過 SSPADD 設置 BRG 的重載值 ====
      //clock = FOSC/(4 * (SSPADD + 1))  
      SSPADD = 0x04;
      
      // ==== 設置迴轉率控制、關閉 SMBus ====
      // SMP=0 CKE=0 D/A P S R/W UA BF @ p83/234
      // the bit without assignment is readonly
      SSPSTAT = SMP_SLEW_CONTROL_ON | SMB_OFF;
  }
  ```

- 觸發各種 condition
  ```c
  void i2c_busIdle()
  {
      // 檢查i2c是否在工作，若是，進入迴圈等待

      // 方法1，透過 SSPCON2 和 SSPSTAT 的狀態位
      // - 檢查 SSPCON2 的 ACKEN, RCEN, PEN, RSEN, SEN 是否為1，若是，代表 master 正在控制 bus
      // - 檢查 SSPSTAT 的 RW 是否為1，若是，代表 master 正在接收數據
      while((SSPCON2 & 0x1F) || (SSPSTAT & 0x04));    // Check if bus is idle

      // 方法2，透過中斷
      //while(!SSPIF);
  }

  // 發送 start-condition
  void i2c_start(void)         // Function to initiate start condition
  {
      SEN = 1;
      i2c_busIdle();
  }

  // 發送 stop-condition
  void i2c_stop(void)          // Function to initiate stop condition
  {
      PEN=1;
      i2c_busIdle();
  }

  // 發送 repeat-start-condition
  void i2c_repeatedStart(void)    // Initiating repeated start condition
  {
      RSEN=1;
      i2c_busIdle();
  }

  // 發送 ack-condition
  void i2c_ack(void)                 // Function to initiate acknowledge sequence 
  {
      ACKDT = 0;
      ACKEN = 1;
      i2c_busIdle();
  }

  // 發送 non-ack-condition
  void i2c_Noack(void)              // Function to initiate No acknowledge sequence
  {
      ACKDT = 1;
      ACKEN = 1;
      i2c_busIdle();
  }

  // 啟用接收
  void receive_enable(void)      // Function to enable reception
  {
      RCEN =1;
      i2c_busIdle();
  }
  ```

## 範例
- [檢視 i2c-master 發送的指令](i2c-master-only/mplab/)
- [使用i2c與RTC通訊，顯在LCD顯示結果](i2c-rtc-pcf8563/mplab/)
- [PIC18F4520+RTC(DS1307)](https://www.youtube.com/watch?v=tu4vGNo6mvc)

## Ref
- [i2c Communication with PIC Microcontroller PIC16F877](https://circuitdigest.com/microcontroller-projects/i2c-communication-with-pic-microcontroller-pic16f877a)

- [I2C communication in Pic microcontroller](https://techetrx.com/pic-microcontroller/i2c-communication-pic-microcontroller/)

- [How to ack / nack i2c operation in PIC18F46k22](https://www.microchip.com/forums/m1207050.aspx)

- [i2c example code from Microchip](https://www.microchip.com/forums/m994750-p6.aspx)

- 範例
  - [Interfacing PCF8563 RTC with PIC microcontroller](https://techetrx.com/pic-microcontroller/interfacing-rtc-with-pic-microcontroller/)

  - [I2C Communication with PIC Microcontroller PIC16F877](https://circuitdigest.com/microcontroller-projects/i2c-communication-with-pic-microcontroller-pic16f877a)
