#include <stdio.h>
#include <stdlib.h>

// define LCD pin (LCD Library Need)
sbit LCD_RS at PORTB.B0;
sbit LCD_EN at PORTB.B1;
sbit LCD_D4 at PORTB.B4;
sbit LCD_D5 at PORTB.B5;
sbit LCD_D6 at PORTB.B6;
sbit LCD_D7 at PORTB.B7;

// define LCD pin direction (LCD Library Need)
sbit LCD_RS_Direction at TRISB.B0;
sbit LCD_EN_Direction at TRISB.B1;
sbit LCD_D4_Direction at TRISB.B4;
sbit LCD_D5_Direction at TRISB.B5;
sbit LCD_D6_Direction at TRISB.B6;
sbit LCD_D7_Direction at TRISB.B7;

void main()
{
    // digital value from ADC
    int digital;

    // real analog value from converted from digital value
    float temperature;

    // voltage in str type
    //char tempText[10];
    char txt[6];

    float resolution = 5.0 / 1024.0;

    // set PORTB = output for LCD
    TRISB = 0x00;
    // set RA1=input
    TRISA = 0b00000010;

    ADC_Init();

    Lcd_init();
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Cmd(_LCD_CURSOR_OFF); // Turn off cursor
    Lcd_Cmd(_LCD_RETURN_HOME);

    while (1)
    {
        // read RA1
        digital = Adc_read(1);

        // For LM35, 10mv/1-degree => 1V = 1/10mV = 100 (degree/1V)
        //     formular 1, voltage = digital * 5 / (1024-1)
        //     formular 2, degree =  voltage * 100 degree/V
        // From formular 1and 2, we could get temperature = digital * (5/1024-1) * 100 = digital * 0.488
        temperature = digital * 0.488;

        // ==== show result ====
        // method 1, NOT RECOMMAND, only show int result
        // IntToStr(temperature, tempText);

        // method 2,
        // txt store ascii-code, in order to show char, need to +48
        // for example, 1 == ascii 0d49

        // show ten-digit
        txt[0] = (int)temperature / 10 + 48;

        // show unit-digit
        txt[1] = (int)temperature % 10 + 48;

        // show .
        txt[2] = 46;

        // show 0 char
        txt[3] = 48;

        // show degree char
        txt[4] = 223;

        // output to lcd
        Lcd_Out(1, 1, txt);

        delay_ms(300);
    }
}