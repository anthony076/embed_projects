void main()
{
     // set PORTB initial value
     PORTB = 0x00;

     // set RB7=input, RB0=output
     TRISB = 0b10000000;

     while (1)
     {
         if (PORTB.B7 == 1)
         {
          PORTB.B0 = 1;
         } else {
           PORTB.B0 = 0 ;
         }
         
         //delay_ms(500);
     }
}