#define _XTAL_FREQ 20000000

// 初始化 i2c
void I2C_Initialize(const unsigned long feq_K);

// 判斷 i2c 是否在忙碌中，若是，進入迴圈等待
void I2C_Hold();

// master 發送 START-condition
void I2C_Begin();

// master 發送 STOP-condition
void I2C_End();

// master 發送數據給 slave
void I2C_Write(unsigned data);

// master 接收 slave 的傳遞的數據
unsigned short I2C_Read(unsigned short ack);
