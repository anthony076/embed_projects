//PIN 18 -> RC3 -> SCL
//PIN 23 -> RC4 ->SDA

#define SSPEN_ON 0x20
#define SSPM_MASTER 0x08
#define SMP_SLEW_CONTROL_ON 0x0
#define SMP_SLEW_CONTROL_OFF 0x80
#define SMB_ON 0x40
#define SMB_OFF 0x0

#include <xc.h>
#include "i2c.h"

void I2C_Initialize(const unsigned long feq_K) //Begin IIC as master
{
  // 將 SDA SCL 設置為 input
  TRISC3 = 1;  TRISC4 = 1;
  
  // ==== 啟動 MSSP模組, 設置 MSSP模組為 i2c-master-mode ====
  // WCOL=0 SSPOV=0 SSPEN=1 CKP=0 SSPM3=1 SSPM2=0 SSPM1=0 SSPM0=0 @ p84/p234 
  SSPCON = SSPEN_ON | SSPM_MASTER;
  
  // ==== 將所有開關清除為0 ====
  //GCEN=0 ACKSTAT=0 ACKDT=0 ACKEN=0 RCEN=0 PEN=0 RSEN=0 SEN=0 @ p85/p234
  SSPCON2 = 0;
  
  // ==== 透過 SSPADD 設置 BRG 的重載值 ====
  //clock = FOSC/(4 * (SSPADD + 1))  
  SSPADD = (_XTAL_FREQ/(4*feq_K*100))-1; //Setting Clock Speed pg99/234
  
// ==== 設置迴轉率控制、關閉 SMBus ====
  // SMP=0 CKE=0 D/A P S R/W UA BF @ p83/234
  // the bit without assignment is readonly
  SSPSTAT = SMP_SLEW_CONTROL_ON | SMB_OFF;
}

void I2C_Hold()
{
    // 檢查i2c是否在工作，若是，進入迴圈等待
    // - 檢查 SSPCON2 的 ACKEN, RCEN, PEN, RSEN, SEN 是否為1，若是，代表 master 正在控制 bus
    // - 檢查 SSPSTAT 的 RW 是否為1，若是，代表 master 正在接收數據

    while (   (SSPCON2 & 0b00011111)    ||    (SSPSTAT & 0b00000100)   ) ; 
}

void I2C_Begin()
{
  I2C_Hold();   // 判斷i2c是否在使用中，若是，進入等待
  SEN = 1;      // master 發送 start-condition
}

void I2C_End()
{
  I2C_Hold();   // 判斷i2c是否在使用中，若是，進入等待
  PEN = 1;      // master 發送 stop-condition
}

void I2C_Write(unsigned data)
{
  I2C_Hold();       // 判斷i2c是否在使用中，若是，進入等待
  SSPBUF = data;    // master 將 傳遞給 slave 的數據寫入 SSPBUF 中
}

unsigned short I2C_Read(unsigned short ack)
{
  unsigned short incoming;
  I2C_Hold();       // 判斷i2c是否在使用中，若是，進入等待
  RCEN = 1;         // 開始接收數據
  
  I2C_Hold();           // 判斷i2c是否在使用中，若是，進入等待
  incoming = SSPBUF;    // 從 SSPBUF 讀取已接收的數據
  
  I2C_Hold();           // 判斷i2c是否在使用中，若是，進入等待
  ACKDT = (ack)?0:1;    // 根據 ack 設置 ACKDT
  ACKEN = 1;            // master 將 ACKDT的值 發送到 bus
  
  return incoming;      // 返回讀取的數據
}