/* 
 * rtc.h/rtc.c are used for rtc-module(PCF8563) only.
 * rtc.h/rtc.c used i2c-module to communicate with PCF8563. 
 
 * Slave-address，PCF8563 有兩個 slave-address，用於區分寫入或讀取的操作
 *      - 寫入時，slave-address = 0xA2
 *      - 讀取時，slave-address = 0xA3
 *      - 參考，p1/30
 
 * RTC reg map
 *      0: control/status1，(TEST1, 0, STOP, 0,     TESTC, 0,   0,  0  )
 *      1: control/status2，(0,     0, 0,    TI/TP, AF,    TF,  AIE TIE)
 *      2: SECONDS
 *      3: MINUTES
 *      4: HOURS
 *      5: DAYS
 *      6: WEEKDAYS
 *      7: MONTHS/CENTURY
 *      8: YEARS
 *      9: MINUTE ALARM
 *      A: HOUR ALARM
 *      B: DAY ALARM
 *      C: WEEKDAY ALARM
 *      D: CLKOUT CONTROL
 *      E: TIMER CONTROL
 *      F: TIMER
 
 * 寫入/讀取流程
 *      - 寫入流程: slave-address=0xA2 > 指定要讀取 rtc-register 的位址 > 依序將數據寫入SSPBUF
 *      - 讀取流程: slave-address=0xA3 > 指定要讀取 rtc-register 的位址 > 依序從SSPBUF讀取已接收的數據
 *      - 參考，p15/30
 
 * 注意，寫入後要讀取，透過 repeat-start-condition 銜接，不要以 stop-condtion 結束
 * 注意，要結束讀取前，先發送 non-ack-conditon，再傳送 stop-condition
 
 */

#include <xc.h>
#include "rtc.h"
#include "i2c.h"

// 初始化rtc(配置rtc)
void rtc_init()
{    
    i2c_start();        // 發送 start-condition
    i2c_busIdle();      // 等待 start-condition 完成
    
    // 寫入 pcf8563 的位址(呼叫pcf8563)，slave-addr=0xA2 代表寫入rtc
    SSPBUF = 0xA2;      // 發送數據
    i2c_busIdle();      // 等待接收 slave 的 ack 訊號
    
    // 指定要存取rtc暫存器的起始位置
    // Register address from where you want to start writing
    SSPBUF = 0x00;
    i2c_busIdle();
    
    // 依序存取 rtc暫存器
    
    // 配置 control-status-1
    SSPBUF = TEST1_NORMAL_MODE | STOP_RTC_RUN | TESTC_POROverride_DISABLE;
    i2c_busIdle();
    
    // 配置 control-status-2，清除所有開關和FLAG
    SSPBUF = 0x00;
    i2c_busIdle();
    
    // 配置 seconds
    SSPBUF = 0x00;      
    i2c_busIdle();
    
    // 配置 minutes
    SSPBUF = 0x15;
    i2c_busIdle();
    
    // 配置 hours
    SSPBUF = 0x10;
    i2c_busIdle();
    
    // 配置 days (日期)
    SSPBUF = 0x04;
    i2c_busIdle();
    
    // 配置 weekdays (星期))
    SSPBUF = 0x00;
    i2c_busIdle();
    
    // 配置 months (月份)
    SSPBUF = 0x02;
    i2c_busIdle();
    
    // 配置 years
    SSPBUF = 0x19;
    i2c_busIdle();
    
    // 結束配置
    i2c_stop();
}

// 讀取 rtc
DateTime * rtc_receive()        
{
    // master 發送 start-condition
    i2c_start();            

    // 寫入 pcf8563 的位址(呼叫pcf8563)，slave-addr=0xA2 代表寫入rtc
    SSPBUF = 0xA2;  // 發送數據
    i2c_busIdle();  // 等待收到 slave 的 ack 訊號

    // 指定要存取rtc暫存器的起始位置
    SSPBUF = 0x02;
    i2c_busIdle();

    i2c_repeatedStart();        // Repeated start condition
    
    // ===============
    
    // 可以不要，不影響功能
    //i2c_start();
    
    // 寫入 pcf8563 的位址(呼叫pcf8563)，slave-addr=0xA3 代表從rtc讀取
    SSPBUF = 0xA3;              
    i2c_busIdle();

    // 實例化 struct 
    DateTime datetime = {
        .sec = 0, // sec
        .min = 0, // min
        .hour = 0, // hour
        .day = 1, // day
        .weekday = 1, //weekday
        .month = 1, // month
        .year = 1900,    //year
    };

    DateTime * dt = &datetime;
    
    // 接收 seconds-byte
    receive_enable();           // 開啟接收 
    dt->sec = SSPBUF;     // 從 SSPBUF 讀取已接收的內容
    i2c_busIdle();              // 確認 bus 閒置中
    i2c_ack();                  // 發送已接收給 slave

    // 接收 minutes-byte
    receive_enable();
    dt->min = SSPBUF;
    i2c_busIdle();
    i2c_ack();

    // 接收 hours-byte
    receive_enable();
    dt->hour = SSPBUF;
    i2c_busIdle();
    i2c_ack();
    
    // 接收 days-byte
    receive_enable();
    dt->day = SSPBUF;
    i2c_busIdle();
    i2c_ack();

    // 接收 weekdays-byte
    receive_enable();
    dt->weekday = SSPBUF;
    i2c_busIdle();
    i2c_ack();

    // 接收 months-byte
    receive_enable();
    dt->month = SSPBUF;
    i2c_busIdle();
    i2c_ack();

    // 接收 years-byte
    receive_enable();
    dt->year = SSPBUF;
    i2c_busIdle();
    i2c_Noack();    // 結束接收前，發送 non-ack-condition 給 slave

    // 停止接收，發送 stop-condition 給 slave
    i2c_stop();
    
    return dt;
}
