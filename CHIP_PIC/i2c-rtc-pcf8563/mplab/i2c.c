
#include <xc.h>
#include "i2c.h"

#define SSPEN_ON 1<<5               
#define SSPM_MASTER 1<<3            
#define SMP_SLEW_CONTROL_ON 0<<7
#define SMP_SLEW_CONTROL_OFF 1<<7   
#define SMB_ON 1<<6 0x40
#define SMB_OFF 0<<6

void i2c_init(void)
{
    // ==== 啟動 MSSP模組, 設置 MSSP模組為 i2c-master-mode ====
    // WCOL=0 SSPOV=0 SSPEN=1 CKP=0 SSPM3=1 SSPM2=0 SSPM1=0 SSPM0=0 @ p84/p234 
    SSPCON = SSPEN_ON | SSPM_MASTER; //0b0010_1000;
    
    // ==== 將所有開關清除為0 ====
    //GCEN=0 ACKSTAT=0 ACKDT=0 ACKEN=0 RCEN=0 PEN=0 RSEN=0 SEN=0 @ p85/p234
    SSPCON2 = 0x00;

    // ==== 透過 SSPADD 設置 BRG 的重載值 ====
    //clock = FOSC/(4 * (SSPADD + 1))  
    SSPADD = 0x04;
    
    // ==== 設置迴轉率控制、關閉 SMBus ====
    // SMP=0 CKE=0 D/A P S R/W UA BF @ p83/234
    // the bit without assignment is readonly
    SSPSTAT = SMP_SLEW_CONTROL_ON | SMB_OFF;
}

void i2c_busIdle()
{
    // 檢查i2c是否在工作，若是，進入迴圈等待
    // - 檢查 SSPCON2 的 ACKEN, RCEN, PEN, RSEN, SEN 是否為1，若是，代表 master 正在控制 bus
    // - 檢查 SSPSTAT 的 RW 是否為1，若是，代表 master 正在接收數據
    while((SSPCON2 & 0x1F) || (SSPSTAT & 0x04));    // Check if bus is idle
}

void i2c_start(void)         // Function to initiate start condition
{
    SEN = 1;
    i2c_busIdle();
}

void i2c_stop(void)          // Function to initiate stop condition
{
    PEN=1;
    i2c_busIdle();
}

void i2c_repeatedStart(void)    // Initiating repeated start condition
{
    RSEN=1;
    i2c_busIdle();
}

void i2c_ack(void)                 // Function to initiate acknowledge sequence 
{
    ACKDT = 0;
    ACKEN = 1;
    i2c_busIdle();
}

void i2c_Noack(void)              // Function to initiate No acknowledge sequence
{
    ACKDT = 1;
    ACKEN = 1;
    i2c_busIdle();
}

void receive_enable(void)      // Function to enable reception
{
    RCEN =1;
    i2c_busIdle();
}