
void lcd_init(void);

void lcd_cmd(unsigned char c);

void lcd_data(unsigned char d);

void enable(void);