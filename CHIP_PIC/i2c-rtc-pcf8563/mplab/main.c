
#include <stdlib.h>
#include <xc.h>
#include "config.h"

#include "i2c.h"
#include "rtc.h"

#include "lcd.h"
#include "display.h"
    
int main()
{
    // 設置 GPIO 方向
    TRISC = 0x18;   // 0b0001_1000，RC3=RC4，設置SCL/SDA為 input
    TRISD = 0x00;   // 設置 PORTD 為 output
    TRISB = 0x00;   // 設置 PORTB 為 output
    
    PORTB = 0x00;   // 將 PORTB 接腳狀態清空為 0
    PORTD = 0x00;   // 將 PORTD 接腳狀態清空為 0
    
    // 初始化模組
    lcd_init();     // 初始化 lcd-module
    i2c_init();     // 初始化 i2c-module
    rtc_init();     // 透過 i2c 設置 rtc, 設置 秒、分、小時、... 等
    
    // 建立 DateTime 指針
    DateTime * dt;
    
    while(1)
    {
        // 設置DDRAM位址為 0x00
        lcd_cmd(0x80);
        dt = rtc_receive();   // 透過 i2c 讀取 rtc 的內容
        
        display_clock(dt);    // 顯示日期和時間

        // 顯示 AM/PM
        lcd_cmd(0x89);
        display_ampm(dt->hour);

        // 顯示
        lcd_cmd(0xc9);
        display_weekday(dt->weekday);    // Display Weekdays

    }    
    
    return 0;
}

