/*
 * diplay.h/display.c is the high level module of lcd-module, 
 * used to display date and time.
 */

#include "rtc.h"

// 將 hex-code 轉換為 ASCII
void display(unsigned int);

// 顯示字串
void disp_wd(char *wd);

// 顯示時間
void display_clock(DateTime * dt);

// 顯示AMPM
void display_ampm(unsigned int hour);

// 顯示星期
void display_weekday(unsigned int);

