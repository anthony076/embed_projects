
void i2c_init(void);            // 初始化 MSSP-i2c 
void i2c_busIdle(void);         // 檢查 bus 是否忙碌中，若是，進入等待
void i2c_start(void);           // master 發送 start-condition
void i2c_stop(void);            // master 發送 stop-condition
void i2c_repeatedStart(void);   // master 發送 repeat-start-condition
void i2c_ack(void);             // master 發送 ack-condition
void i2c_Noack(void);           // master 發送 non-ack-condition
void receive_enable(void);      // master 啟用接收開關
