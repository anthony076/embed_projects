/* 
 * rtc.h/rtc.c are used for rtc-module(PCF8563) only.
 * rtc.h/rtc.c used i2c-module to communicate with PCF8563. 
 */

#ifndef RTC_H
#define RTC_H

#define TEST1_NORMAL_MODE 0x0
#define TEST1_TEST_MODE 0x80

#define STOP_RTC_RUN 0x0
#define STOP_RTC_DISABLE 0x20

#define TESTC_POROverride_DISABLE 0x0
#define TESTC_POROverride_ENABLE 0x08

// 定義 struct 
typedef struct {
    unsigned int sec;
    unsigned int min;
    unsigned int hour;
    unsigned int day;
    unsigned int weekday;
    unsigned int month;
    unsigned int year;
} DateTime;
      
void rtc_init();     
DateTime * rtc_receive();

#endif