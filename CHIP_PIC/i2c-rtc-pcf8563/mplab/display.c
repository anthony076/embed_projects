/*
 * diplay.h/display.c is the high level module of lcd-module, 
 * used to display date and time.
 */

#include <xc.h>
#include "config.h"

#include "display.h"
#include "lcd.h"
#include "rtc.h"

// 顯示數值
void display(unsigned int x)    // 將 hex-code 轉換為 ASCII
{                              
    lcd_data((x/16)+48);
    __delay_ms(5);
    
    lcd_data((x%16)+48);
    __delay_ms(5);
}

// 顯示字串
// 注意，顯示單一字元，直接調用 lcd_data()
void disp_wd(char *wd)
{
    while(*wd != '\0')
    {
        lcd_data(*wd);
        wd++;
    }
}

// 顯示時間和日期
void display_clock(DateTime * dt)
{
    display(dt->hour & 0x3f);
    lcd_data(':');

    display(dt->min & 0x7f);
    lcd_data(':');

    display(dt->sec & 0x7f);

    lcd_cmd(0xc0);
    __delay_ms(5);

    display(dt->day & 0x3f);
    lcd_data('/');

    display(dt->month & 0x1f);
    lcd_data('/');

    display(dt->year & 0xff);
}

// 顯示星期
void display_weekday(unsigned int weekday)
{
    switch(weekday)
    {
        case 0: 
            disp_wd("MON");
            break;
        case 1: 
            disp_wd("TUE");
            break;
        case 2:
            disp_wd("WED");
            break;
        case 3:
            disp_wd("THU");
            break;
        case 4:
            disp_wd("FRI");
            break;
        case 5: 
            disp_wd("SAT");
            break;
        case 6: 
            disp_wd("SUN");
            break;
    }
}

// 顯示 AM/PM
void display_ampm(unsigned int hour)
{
  if(hour>0x11)
           disp_wd("PM");
       else
           disp_wd("AM");
}