## 中斷基礎

- 持續獲得狀態並根據狀態進行響應的方式有兩種
  - 方法1，Polling 輪詢
  - 方法2，Interrupt 中斷基礎

- 方法1，Polling，
  - 是基於軟體的，由軟體觸發和控制 
  - 在主進程中，透過迴圈的方式主動檢查狀態，並根據結果執行對應的響應
  - 會阻塞主進程
  - 不是即時性的，受代碼和代碼的數量控制，
    - 例如，在代碼中加入5秒的時間延遲，表示至少5秒才會輪詢一次
    - 例如，在響應的代碼中，只有10條指令要處理，和有1000條指令要處理的時間不同

- 方法2，Interrupt，
  - 內中斷是基於CPU執行指令時產生的錯誤，例如溢位，是CPU內建的中斷
  - 外中斷是基於硬體的，由硬體觸發中斷，由軟體進行處理，是開發者建立的中斷
  - 系統級別的 Polling，不受代碼控制
  - Interrupt 在指令週期中，就設置了Interrupt的檢查，
    
    若Interrupt產生就立刻執行對應的ISR程式

    <img src="doc/interrupt/instruction-cycle-with-interrupts.png" width=80% height=auto>

- MCU 處理中斷的流程
  ```mermaid
  flowchart LR 

  b1[觸發中斷] --> b2[CPU檢測到中斷後\n停止主程式] 
  b2 --> b3[判斷中斷類型] 
  b3 --> b4[執行對應的中斷處理函數] --> b5[回到主程式停止點繼續執行]
  ```
  
  中斷發生時，CPU 會暫停執行，並將下一條指令的位址推入 stack 中，
  接著跳轉到中斷向量的位址(0004h)取得中斷處理程式的位址，
  跳轉中斷處理程式的位址並執行，

  中斷處理程式結束時，會執行 RETFIE 的指令，取回下一條指令的位址，
  並回到主程序繼續執行
    
- MCU 屬於非向量中斷 (MCU 和 8086 CPU 中斷的差別)
  - 相同，MCU將CPU和外設(或稱為 module)放在同一個晶片中，
    
    因此，除了CPU以外的module都是外設，由module觸發的中斷，都屬於硬體中斷
    
  - 相同，和 8086 CPU 一樣，MCU 內部的CPU`只有一個接腳接受中斷觸發`，
    - 相異，8086 在CPU外部(在主板上)，`需要 8259A 的中斷控制晶片`，來控制那一個中斷源會傳遞給CPU，
      並由CPU去執行對應的中斷處理函數(ISR)

    - 相異，MCU 將 8259 的功能，`實作為一個數位邏輯電路已經放入MCU的晶片`中

  - 8086 需要`中斷類型碼`，擁有`多個中斷處理函數`
    - 8086 CPU `只有通用暫存器，沒有特殊暫存器可以保存中斷狀態的暫存器`，
      只能透過中斷類型碼識別，判斷中斷是由哪一種中斷觸發，
    
    - 8086 利用中斷向量表，來保存各種中斷類型碼對應的中斷處理函數的位址，
      每一種中斷對應一個中斷類型碼，中斷類型馬可換算出中斷向量表的特定位置，該位址保存中斷處理函數的位址，
      ```mermaid
      flowchart LR

      中斷類型碼(中斷向量表編號) --> 中斷向量表位址 --> 中斷處理函數的位址
      ``` 

    - `8086 CPU 需要中斷類型碼識別`才能觸發中斷處理函數，每一個中斷類型碼識別對應一個中斷處理函數，
      因此，8086 會有多個中斷處理函數

  - MCU `不需要中斷類型碼`，且`只有一個中斷處理函數`
    - MCU 是小型的PC，除了CPU外，已經包含許多微型的外設(或稱為 module)，而 8086 只是一個CPU，需外部電路才能提供更多的功能

    - MCU `有各種特殊暫存器`，每一種中斷的狀態，`在暫存器中都有特定的標記位置`，
    透過檢查中斷暫存器的內容，就可以得知是哪一種中斷觸發的類型

    - MCU `不需要中斷類型碼識別`，只要有中斷被觸發，且CPU接收到中斷觸發訊號，
    CPU就會自動執行中斷處理函數，
    
      在中斷處理函數透過`檢查中斷暫存器的內容`，就可以知道觸發中斷的來源，
      因此，MCU 只需要一個中斷處理函數，就可以對不同類型的中斷進行處理

    - 因為MCU只需要一個中斷處理函數，也只需要一個記憶體位址保存中斷處理函數的位址，
      在 PIC16f877 中，可供開發者自定義的中斷向量，稱為 Interrupt-Vector，位於 0004h 的位址中，
      內建的只有一個 Reset-Vector，位於 0000h 的位址中

- MCU 中斷的硬體架構
  
  <img src="doc/interrupt/interrupt-hardware.png" width=80% height=auto>

  - 觸發中斷的來源
    - 軟體中斷，若軟體主動觸發的中斷，例如，SWI
    - 硬體中斷，由CPU的外設(或稱為 module)觸發的中斷，例如，CCP-module、SPI、UART、I2C、ADC、EEPROM、GPIO、... 

  - 當外設(或稱為 module)的中斷產生，第一時間會將該中斷在暫存器中的中斷標記位設置為1，
    中斷標記位變更後，必須要`CPU接收到中斷通知到，才會去執行中斷處理函數`，
    
    CPU是否收到中斷配置，是由配置決定，中斷處理函數是否執行，要看開發者是否實現
  
  - 要使CPU接收到模組產生的中斷通知，至少要打開 PEIE 和 GIE，
    - PEIE，外設中斷致能
    - GIE，全局中斷致能

  - 由上述的數位邏輯電路，可以決定哪一個中斷觸發訊號，最後可以傳遞給 CPU，
    CPU 收到中斷觸發信號後，會自動跳轉到註冊的中斷常式中 

## 與中斷相關暫存器

- 注意，RB0 和 RB7-RB4 使用不同的中斷暫存器
  - RB0/INT 接腳的中斷，由 INTE位/INTF位 控制，
  - RB7-RB4 接腳的中斷，由 RBIE位/RBIF位 控制，

- INTCON暫存器
  - INTCON暫存器控制第一級的中斷開關，外設自身的中斷開關屬於第二級的中斷開關

  - GIE位，Global Interrupt Enable
    - CPU 接收中斷訊號的總開關
    - GIE=1，啟用全局中斷，允許中斷訊號傳遞給CPU
    - GIE=0，關閉全局中斷，不允許中斷訊號傳遞給CPU

  - PEIE位，Peripheral Interrupt Enable
    - 外設(或稱為 module) 發出中斷訊號的總開關
    - PEIE=1，啟用外設中斷，允許外設發起中斷訊號
    - PEIE=0，關閉外設中斷，不允許外設發起中斷訊號

  - TMR0IE位，Timer 0 Overflow Interrupt Enable
    - TMR0IE=1，允許TIMER0發出溢位中斷
    - TMR0IE=0，不允許TIMER0發出溢位中斷

  - TMR0IF位，Timer-0是否發出溢位中斷的標記位

  - INTE位，RB0/INT External Interrupt Enable
    - INTE=1，允許RB0/INT發出中斷
    - INTE=0，不允許RB0/INT發出中斷

  - INTF位，RB0/INT 是否發出中斷的標記位

  - RBIE位，RB Port Change Interrupt Enable
    - RBIE=1，允許RB7-RB4接腳的狀態變更時，發出中斷通知
    - RBIE=0，不允許RB7-RB4接腳的狀態變更時，發出中斷通知

  - RBIF位，RB7-RB4接腳，是否發出狀態變更中斷的標記位

- OPTION_REG暫存器
  - INTEDG位，Interrupt Edge Select bit
    - 設置 RB0/INT 接腳觸發 interrupt 的時機
    - INTEDG=1，RB0/INT 在 clk 上升邊緣時觸發中斷，在按鈕按下後觸發
    - INTEDG=0，RB0/INT 在 clk 下降邊緣時觸發中斷，在按鈕放開後觸發

## 中斷的使用
- step1，設置 GPIO 的方向，

- step2，設置 GPIO 的初始值，

- step3，設置中斷
  - (選用) 3-1，若使用 RB0/INT接腳，設置 RB0/INT接腳發出中斷的時機
  - 3-2，根據需求設置 INTCON暫存器的開關，例如，
    - 若使用 RB0/INT接腳，設置 INTE = 1
    - 若使用 外設中斷，設置 PEIE = 1

- step4，開啟全局中斷，INTCON.GIE = 1，使CPU 可以接收中斷訊號，

- step5，定義中斷處理函數，`void interrupt ISR()`
  - 檢查中斷發生的標記位
  - 定義處理流程
  - 將中斷標記位清除為 0，才能接受下一次的中斷觸發

## 範例
- [interrupt的基本使用](interrupt_basic/)

## Ref
- [Interrupt in datasheet-14.11 p55]()

- [中斷基礎 + ISR 範例 @ x86asm](https://gitlab.com/anthony076/x86asm/-/blob/main/doc/10_farcall-interrupt-and-error.md)

- [硬體中斷 @ x86asm](https://gitlab.com/anthony076/x86asm/-/blob/main/doc/14_hardware_interrupt_and_clock.md)

- [Interrupts In PIC Microcontrollers](https://deepbluembedded.com/interrupts-in-pic-microcontrollers/)

- [IRQ – Interrupt Request Pins](https://deepbluembedded.com/irq-interrupt-requests/)

- [How To Write ISR Handlers](https://deepbluembedded.com/how-to-write-isr-handlers/)