## MSSP Module
- [通訊協定基礎](../Module/Communication.md)
- [SPI 基礎](../Module/SPI.md)

- Master Synchronous Serial Port (MSSP) 是`主從式同步串列通訊`，
  可操作兩種協議: spi協議 和 i2c協議

## SPI 架構和接腳
- spi 架構

  <img src="doc\mssp\spi-block-diagram.png" width=60% height=auto>

- SPI Master/Slave mode 使用的接腳
      
  | 硬體接腳             | SPI                  | 用途                |
  | -------------------- | -------------------- | ------------------- |
  | RC5/SDO接腳          | Serial-Data-Out(SDO) | 資料輸出            |
  | RC4/SDI/SDA接腳      | Serial-Data-In(SDI)  | 資料輸入            |
  | RC3/SCK/SCL接腳      | Serial-Clock(SCK)    | 觸發傳送/接收的時脈 |
  | RA5/AN4/SS/C2OUT接腳 | Slave-Select(SS)     | slave-spi模組致能   |

## SPI 相關暫存器
- SPI 使用的暫存器
  - SSPSTAT暫存器，檢視MSSP模組暫存器
  - SSPCON暫存器，配置MSSP模組暫存器1
  - SSPCON2暫存器，配置MSSP模組暫存器2
  - SSPSR暫存器，移位暫存器，負責將SSPBUF暫存器的數據移入或移出，使用者無法存取

- SSPSTAT暫存器
  - `SMP位`，Sample bit，選擇對輸入數據進行取樣的時機
    - Master-mode
      - SMP=1，在資料輸出週期的尾部，對輸入數據(SDI)進行取樣
      - SMP=0，在資料輸出週期的中部，對輸入數據(SDI)進行取樣
    - Slave-mode，需要將 SMP位設置為0
  
  - `CKE位`，SPI Clock Select bit，Transmit-Edge，數據(SDI/SDO)開始傳輸的時機
    - CKE=1，在 Clock 狀態，由 Active->Idle 時進行傳輸，由 Idle->Actvie 時進行SDI取樣
    - CKE=0，在 Clock 狀態，由 Idle->Active 時進行傳輸，由 Active->Idle 時進行SDI取樣
    - 注意，Idle-state 有可能是0或1，由 SSPCON1-bit4 的CKP位進行控制，
      CKP 定義Idle的電位，CKE定義SPI模組動作的時間，CKP/CKE 兩者一起搭配使用

  - `D/A位`，Data/Address bit，用於 `i2c協議`，只讀位

  - `P位`，Stop bit，用於 `i2c協議`，只讀位
    - 當 SSPEN=0，關閉 MSSP模組時，P位會自動設置為0

  - `S位`，Start bit，用於 `i2c協議`，只讀位

  - `RW位`，Read/Write，用於 `i2c協議`，只讀位

  - `UA位`，Update Address，用於 `i2c協議`，只讀位

  - `BF位`，Buffer Full Status bit，僅用於 Receive-mode，指示 SSPBUF 是否已滿
    - BF=1，接收已完成，SSPBUF已滿，需要手動讀取結果
    - BF=0，接收未完成，SSPBUF為空，

- SSPCON1暫存器，用於 SPI 協議
  - `WCOL位`，Write Collision Detect bit，僅用於 Transmit-mode，指示寫入SSPBUF暫存器是否發生衝突
    - 若發生衝突，需要手動清除為0
    - WCOL=1，發生衝突，SSPBUF暫存器內已經寫入資料，且資料仍然在傳送中
    - WCOL=0，未發生衝突

  - `SSPOV位`，Receive Overflow Indicator bit，接收溢位指示，指示SSPBUF是否發生溢位
    - Master-mode

      在主動模式下，SSPOV位不會被設置為1，
      因為每個新的接收或發送都是通過寫入 SSPBUF寄存器來啟動的

    - Slave-mode
      - SSPOV=1，在 SSPBUF暫存器仍然保留前一筆資料下，接收到新的數據
      - SSPOV=0，SSPBUF暫存器未發生溢位

  - `SSPEN位`，Synchronous Serial Port Enable bit，啟動SSP模組
    - SSPEN=1，啟用SSP模組，並設置 SCK、SDO、SDI、SS 作為序列傳輸的接腳
    - SSPEN=0，關閉SSP模組，並設置 SCK、SDO、SDI、SS 為一般的IO接腳
    - 注意，在啟用前，必須正確地將這些接腳設置為正確的傳輸方向

  - `CKP位`，Clock Polarity Select bit，設置 Clock 的 Idle 極性
    - CKP=1，將 clock-high-level 設置為 idle-state
    - CKP=.，將 clock-low-level 設置為 idle-state
    - 注意，CKP位 和 CKE位，兩者可以控制數據發送的時機
  
  - `SSPM3:SSPM0位`，Synchronous Serial Port Mode Select bits，傳輸模式選擇
    - Master-mode
      - 0000，spi-master-mode，設置 CLK=Fosc/4
      - 0001，spi-master-mode，設置 CLK=Fosc/16
      - 0010，spi-master-mode，設置 CLK=Fosc/64
      - 0011，spi-master-mode，設置 CLK=TMR2/2
  
    - Slave-mode
      - 0100，spi-slave-mode，設置 clock源為SCK接腳，設置`SS接腳控制`啟用
      - 0101，spi-slave-mode，設置 clock源為SCK接腳，設置`SS接腳控制`關閉，SS接腳可被設置為 IO 接腳
  
## SPI 的使用
- 概述

  - MSSP模組是由 `transmit/receive-shift-register(SSPSR移位暫存器)` 和 `buffer-register(SSPBUF暫存器)` 組成，
    SPI 的寫入和讀取都使用同一組的 SSPSR移位暫存器 和 SSPBUF暫存器，寫入和讀取不會同時發生

  - SSPSR移位暫存器負責依序將數據移出(SDO)或移入(SDI)，數據傳輸時，`MSB`會優先處理，
    
  - SSPSR移位暫存器接收SDI接腳輸入的數據時，每當接收的數據準備好(接收到8bit)後，就會從SSPSR轉由SSPBUF保存，
    此時，`SSPSTAT-BF位` 和 `SSPIF位` 都會被設置為1，

  - SSPBUF暫存器是不為空的狀況下，SSPSR暫存器仍然可以接收數據
    
    接收數據保存在SSPBUF後，需要等待軟體讀取SSPBUF的內容後，才會將SSPBUF清空，
    
    SSPBUF 和 SSPSR 形成一個 double-buffering 的接收器，在SSPBUF內的數據未被讀取前，SSPSR 仍然可以接收新的數據，

  - 在數據傳送和接收的期間，對 `SSPBUF暫存器`的寫入會被忽略，
    
    若發生SSPBUF暫存器的寫入操作，會自動將 `SSPCON1暫存器-WCOL位` 設置為1，
    此時，必須透過軟體將`WCOL位`清除為0，才能正確判斷下一次的寫入是否成功
  
  - 要接收到有效的數據，必須要在下一筆8bit輸入數據從SSPSR轉移到SSPBUF前，將SSPBUF暫存器清空，
    - 若下一筆8bit的輸入資料寫入SSPBUF之前，沒有將SSPBUF清空，就會將下一筆8bit的輸入資料拋棄，造成資料不連續，使預期的輸入資料失效
    
    - 方法1，透過 `MSSP模組的中斷(SSPIF)`，用於通知傳輸已經完成，SSPBUF已滿
    
    - 方法2，若不使用SSPIF通知，則進行寫入和讀取之前，需要手動確認 SSPBUF 的狀態
      - 透過 BF位，BF=1，表示`輸入數據已接收完畢`(SSPBUF已滿)，BF=0，SSPBUF暫存器的內容已經被讀取
      - 透過 WCOL位，WCOL=1，發生寫入衝突(SSPBUF已滿)，WCOL=0，未發生寫入衝突

- 基礎數據傳輸範例
  - step1，檢查SSPSTAT暫存器-BF位，判斷前一次的接收是否完成 (判斷 SSPBUF暫存器是否為空)
  - step2，BF=1，前一次接收已完成，讀取 SSPBUF 的內容
  - step3，將 SSPBUF 的內容保存在記憶體中
  - step4，將要傳送的資料寫入SSPBUF
  - step5，等待資料傳送完畢

- 軟體設置
  - step1，設置GPIO接腳 + 啟動SPI模組
    - 1-1，透過 `TRIS暫存器`，將 SDI接腳、SDO接腳、SCK接腳、SS接腳，設置為正確的方向
      - SDI接腳，由SPI模組自動控制，
      - SDO接腳，設置 SDO=0=輸出
      - SCK接腳，若為 Master-mode，SCK=0=輸出，若為 Master-mode，SCK=1=輸入，
      - SS接腳，設置 SS=1=輸入

    - 1-2，將 `SSPCON1暫存器-SSPEN位` 設置為1，啟動 SPI 模組

    - 注意，若要重設 SPI 模組，將 SSPEN位清除為0，重新設置SPI相關參數後，再將SSPEN位重新設置為1，
      將SSPEN設置為1，才會將 SDI接腳、SDO接腳、SCK接腳、SS接腳、提供給MSSP模組使用

  - step2，配置 SPI
    - 2-1，透過 `SSPCON1暫存器-SSPM3:SSPM0位`，設置 Master-mode 或 Slave-mode

    - 2-2，透過 `SSPCON1暫存器-CKP位`，相當於 CPOL，決定 clock-idle 的極性，

    - 2-3，透過 `SSPSTAT暫存器-CKE位`，相當於 !CPHA，決定 transmit/sample-edge 的方向

      CKE 用於決定觸發SDO/SDI開始傳輸的CLK邊緣方向(transmit-edge)，對SDI採樣的CLK邊緣方向(sample-edge)和CKE相位相反

    - 2-4，透過 `SSPSTAT暫存器-SMP位`，

      決定`輸入數據取樣的時機`和`輸出數據移出`的時機，

- 硬體連接方式，以兩個 PIC 的通訊為例

  <img src="doc/mssp/two-spi-module-connection.png" width=80% height=auto>

  多設備的連接方式

  <img src="doc/mssp/multiple-slave-device.png" width=50% height=auto>

- 時序圖

  Master-mode

  <img src="doc/mssp/waveform-master-mode.png" width=80% height=auto>

  Slave-mode

  <img src="doc/mssp/waveform-slave-mode.png" width=80% height=auto>

  Master + Slave Transaction Example

  <img src="doc/mssp/spi-write-read-transaction.png" width=80% height=auto>

## 範例
- [簡易SPI庫](simple-spi-logger/)
- [以DAC為例，使用SPI庫](spi_communicate_DAC_useLibrary/)
- [以DAC為例，使用reg](spi_communicate_DAC_useReg/)

## 問題
- SPI 如何得知 Slave 回傳的數據長度
  
  根據 [通訊協定基礎](../Module/Communication.md)，
  因為使用相同的CLK作為同步源，SPI需要預先知道接收數據的長度，才能安排足夠數量的 CLK 供 Slave 傳輸數據
  
## Ref
- [9.0 MASTER SYNCHRONOUS SERIAL PORT (MSSP) Module @ datasheet-p73]()

- [SPI Communication with PIC Microcontroller PIC16F877A](https://circuitdigest.com/microcontroller-projects/pic16f877a-spi-communication-tutorial)

- [SPI Modes and Timing](http://www.rosseeld.be/DRO/PIC/SPI_Timing.htm)

- [Introduction to SPI Interface]()https://www.analog.com/en/analog-dialogue/articles/introduction-to-spi-interface.html

- [SPI基礎 @ 成大資工](http://wiki.csie.ncku.edu.tw/embedded/SPI#spi-functional-description)

- [SPI communication Data Transmission visualization](https://www.youtube.com/watch?v=sHmixFJhr3M)

