// set display code
int zero = 0x3f;
int one = 0x06;
int two = 0x5b;
int thre = 0x4f;
int four = 0x66;
int five = 0x6d;
int six = 0x7d;
int seve = 0x07;
int eigh = 0x7f;
int nine = 0x6f;

// define content for display
// array cant store reference directly, use pointer instead.
int * display[] = {&zero, &one, &two, &thre, &four, &five, &six, &seve, &eigh, &nine};

int i;

void main()
{
     // set PORTB initial value
     PORTB = 0x00;

     // set PORTB direction, 0=output, 1=input
     TRISB = 0x00;

     while (1)
     {
       for (i=0; i<10; i++)
       {
           PORTB = *display[i];
           delay_ms(500);
       }
     }
}