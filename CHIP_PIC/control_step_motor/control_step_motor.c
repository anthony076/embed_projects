
void main()
{
  // set PORTB initial value
  PORTB = 0x00;

  // set all pins in PORTB to OUTPUT mode
  TRISB = 0x00;

  while (1)
  {

    // PORTB = xxxx(2b)(2a)(1b)(1a)

    // cw 0-degree, drive 1a
    PORTB = 0b00000001;
    delay_ms(3000);

    // cw 45-degree, drive 1a, 2a
    PORTB = 0b00000101;
    delay_ms(3000);

    // cw 90-degree, drive 2a
    PORTB = 0b00000100;
    delay_ms(3000);

    // cw 135-degree, drive 2a, 1b
    PORTB = 0b00000110;
    delay_ms(3000);

    // cw 180-degree, drive 1b
    PORTB = 0b00000010;
    delay_ms(3000);
  }
}