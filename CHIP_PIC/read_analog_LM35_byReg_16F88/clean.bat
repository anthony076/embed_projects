@echo off

rmdir /S /Q "proteus/Project Backups"
del /F /Q *.asm *.bmk *.brk *.ini *.cp *.dbg *.dct *.dlt *.log *.lst *.mcl *.dic *.cfg *.txt

