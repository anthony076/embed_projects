// ==== Pin assignment ====
//     RB0 = LCD-RS, RB1 = LCD-E, RB4=DB4, RB5=DB5, RB6=DB6, RB7=DB7,
//     RA1/AN1 = lm35 sensor
//
//     external osc 12MHZ，Tosc = 1 / 12 = 0.083，倍率 = 1.6 / 0.083 = 19.27
//     選用 Tad = 32 Tosc 的選項 

// ==== init ADC ====
//      ADFM= adc result stored at ADRESL[7:0] + ADRESH[1:0]
//      ADCSx= 0b010 = 32 Tosc，ADCS2:ADCS0 = 010
//      CHSx= 0b001 = Select AN1
//      PCFGx= 0x00 = no external Vref, use internal VDD / VSS

// ADCON0:  ADCS1   ADCS0   CHS2    CHS1    CHS0    GO/DONE     -   ADON
//          1       0       0       0       1       1           0   1       = 0x8D
//          ADON=1, power-on ADC module
//          GO=1, ADC start working

// ADCON1:  ADFM    ADCS2  VCFG1   VCFG0   PCFG3    PCFG2   PCFG1   PCFG0
//          1       0       0       0      0        0       0       0       = 0x80

#include <stdio.h>
#include <stdlib.h>

// define LCD pin (LCD Library Need)
sbit LCD_RS at PORTB.B0;
sbit LCD_EN at PORTB.B1;
sbit LCD_D4 at PORTB.B4;
sbit LCD_D5 at PORTB.B5;
sbit LCD_D6 at PORTB.B6;
sbit LCD_D7 at PORTB.B7;

// define LCD pin direction (LCD Library Need)
sbit LCD_RS_Direction at TRISB.B0;
sbit LCD_EN_Direction at TRISB.B1;
sbit LCD_D4_Direction at TRISB.B4;
sbit LCD_D5_Direction at TRISB.B5;
sbit LCD_D6_Direction at TRISB.B6;
sbit LCD_D7_Direction at TRISB.B7;

void main()
{
    int digital;
    float temperature;

    char txt[6]; // voltage in str type

    ANSEL = 0b00000010; // select RA1/AN1 pin input
    //TRISA = 0b00000010; // set RA1/AN1 pin input
    
    TRISB = 0x00;       // set PORTB = output
    ADCON1 = 0x80;

    Lcd_init();
    Lcd_Cmd(_LCD_CLEAR);
    Lcd_Cmd(_LCD_CURSOR_OFF); // Turn off cursor
    Lcd_Cmd(_LCD_RETURN_HOME);
    
    while (1)
    {
        // need to set GO/DONE and ADON in ADCON0 to HIGH every time,
        // in order to trigger ADC Work again
        ADCON0 = 0x8D;
        
        // wait for ADC conversion working in progress.
        while (GO_DONE_bit == 1);

        // Read ADC result from reg
        digital = (ADRESH << 8) | ADRESL;

        // convert digital value into temperature
        temperature = digital * 0.488;

        txt[0] = (int)temperature / 10 + 48;
        txt[1] = (int)temperature % 10 + 48;
        txt[2] = 223; // show degree char
        txt[3] = 67;  // show C char

        Lcd_Out(1, 1, txt);

        delay_ms(300);
    }
}