## ADC 的基礎
參考，[adc-basic](../Module/ADC.md) 和 [SAR-ADC](../Module/ADC.md)

## ADC 架構
- AD Block Diagram
  
  <img src="doc/adc/ad-block-diagram.png" width=80% height=auto>

- 對於 28pin 的PIC晶片，ADC-module 有5個 analog 輸入，
  
  對於 40/44pin 的PIC晶片，ADC-module 有8個 analog 輸入

- 外部參考電壓
  - 參考電壓就是類比電壓的最大值，ADC 會將輸入類比電壓與參考電壓進行比較
  - 利用參考電壓可以算出解析度
  - AD 模組有兩個`外部參考電壓輸入`，讓使用者可以自定義參考電壓
  - 外部的參考電壓輸入，是透過 RA3 或 RA2 的接腳輸入，
    透過 ADCON1 暫存器的 PCFG3:PCFG0 控制是否啟用外部參考電壓輸入

- PIC16f877 採用 `SAR-ADC` 的架構

## ADC 相關暫存器
- ADC 使用到的暫存器

  <img src="doc/adc/registers-for-adc.png" width=80% height=auto>

  - TRISA/PORTA暫存器，設置接腳，將接腳設置為 ADC
  - ADCON0/ADCON0暫存器，用於配置 ADC 的功能
  - ADRESH/ADRESH暫存器，用於存放ADC轉換後的結果
  - PIR/PIE暫存器，
    - ADIF位，A/D Converter Interrupt，ADC轉換完成的標記位
    - ADIE位，啟用AD中斷通知
  - INTCON暫存器
    - PEIE位，啟用外設中斷通知
    - GIE位，啟用中斷通知

- ADCON0/ADCON1暫存器
  - ADCS2/ADCS1/ADCS0 = A/D Conversion Clock Select bits，選擇取樣率

    參考，[AD 轉換時脈的設置]() 一節

  - CHS2:CHS0 = Analog Channel Select bits，選擇 Analog 轉換通道

    | CHS2 | CHS1 | CHS0 | Channel         |
    | ---- | ---- | ---- | --------------- |
    | 0    | 0    | 0    | Channel 0 (AN0) |
    | 0    | 0    | 1    | Channel 1 (AN1) |
    | 0    | 1    | 0    | Channel 2 (AN2) |
    | 0    | 1    | 1    | Channel 3 (AN3) |
    | 1    | 0    | 0    | Channel 4 (AN4) |
    | 1    | 0    | 1    | Channel 5 (AN5) |
    | 1    | 1    | 0    | Channel 6 (AN6) |
    | 1    | 1    | 1    | Channel 7 (AN7) |

  - GO/DONE = A/D Conversion Status bit，指示轉換狀態
    - GO/DONE = 1，執行 AD 轉換中
    - GO/DONE = 0，AD 轉換已完成
    - 注意，GO/DONE位只有在 ADON = 1 的條件下才有效

  - ADON: A/D On bit，AD模組啟動控制和指示
    - ADON = 1，啟動 AD模組
    - ADON = 0，關閉 AD模組

  - ADFM: A/D Result Format Select bit，轉換結果格式選擇
    - 概念，

      AD 轉換的結果(10-bit 的數位值)，會放在各 8bit 長的 ADRESH和ADRESL暫存器中，利用 ADFM 決定AD轉換的結果是靠右放置或靠左放置
      
      <img src="doc/adc/ad-result-format-justification.png" width=80% height=auto>

    - ADFM = 0，靠左放置，結果 = ADRESH7:ADRESH0 + ADRESL7:ADRESL6
    - ADFM = 1，靠右放置，結果 = ADRESH1:ADRESH0 + ADRESL7:ADRESL0
    - 注意，未使用的部分補0

  - PCFG3:PCFG0 = A/D Port Configuration Control bits，接腳配置控制位
    - A = Analog-Pin / D = Digital-Pin
    - Vref+ = Vref+ 連接的接腳，有可能是外部接腳或內部電源的接腳
    - Vref- = Vref- 連接的接腳，有可能是外部接腳或內部電源的接腳
    - C/R
      - C=Channel，使用到的 Analog Channel 的數量 = Analog-Pin 的數量
      - R=Reference，使用到的外部參考電壓數量，若使用內部參考電壓，則不計入此項

    <img src="doc/adc/adc-pin-config.png" width=80% height=auto>

## ADC 的使用
- ADC 使用流程
  - step0，利用 TRISA/PORTA暫存器，配置類比接腳的方向
  
  - step1，配置 ADC
    - 1-1，利用 ADCON1暫存器，配置 ADC 使用的接腳配置
    - 1-2，利用 ADCON0暫存器，選擇 ADC 的輸入通道
    - 1-3，利用 ADCON0暫存器，選擇 ADC 的轉換時脈
    - 1-4，開啟 ADC 模組

  - step2，(選用)配置AD中斷
    - 清除 ADIF 位 @ PIR暫存器
    - 設置 ADIE 位 @ PIE暫存器，啟用AD中斷通知
    - 設置 PEIE 位 @ INCON暫存器，啟用外設中斷通知
    - 設置 GIE 位 @ INCON暫存器，啟用中斷通知
  
  - step3，等待需要的 acquisition time

  - step4，開始進行轉換，將 ADCON0暫存器的 GO/DONE 設置為 1

  - step5，等待AD轉換完成，判斷方式有兩種
    - 輪詢 GO/DONE 位，若轉換完成， GO/DONE 會自動被設置為 0
    - 透過 ADIF 中斷
  
  - step6，從 ADRESH/ADRESL暫存器讀取結果，清除 AFID位

- ADC 的使用需求
  - ADC <font color="red">不會直接取用輸入的類比電壓</font>，避免外部電壓影響內部電路

  - ADC 透過`採樣電路`獲取輸入的類比電壓，
    
    實務上，透過`輸入類比電壓對電容器充電`的方式，
    獲取輸入的類比電壓值，該電容器稱為採樣電容，電容充電的電壓稱為採樣電壓，理想上，採樣電容電壓 == 輸入類比電壓，避免失真

  - 為了達到預期的精度，就要使採樣電容充電到與輸入相同的電壓，則最小的充電時間(Tc)

    充電時間會受採樣電容的大小(Chold)、輸入電路阻抗(Rs)、晶片內部接線阻抗(Ric)、採樣電路阻抗 (Rss) 的影響

    ```
    Tc = Chold (Ric + Rss + Rs) In(1/2047)
    ```

    根據 Analog-Input-Model，
    - Ric，晶片內部接線的阻抗為 1k 歐姆 
    - Rss，採樣電路的阻抗根據 VDD 的值而有差異，VDD=5V 時，Rss = 7k 歐姆
    - Rs，輸入電路的阻抗為 10k 歐姆
    - Chold，採樣電容 = 120pF

    <img src="doc/adc/Analog-Input-Model.png" width=100% height=auto>

    ```
    Tc = Chold (Ric + Rss + Rs) In(1/2047)
       = Chold (Ric + Rss + Rs) In(1/2047)
       = - 120 pF (1 kΩ + 7 kΩ + 10 kΩ) In(0.0004885) = 16.47us
    ```
    
    要使採樣電容完全充電，需要至少 16.47us

  - 最小的獲取時間 (Acquisition-Time，Tcaq)
    
    除了充電時間外，電路的設置時間 和 溫度和老化造成阻抗的變化，也會影響AD轉換的結果，
    為了保持AD轉換的精度，需要滿足最小獲取採樣電壓的時間(Tacq)

    ```
    Tacq = 設置時間 + 採樣電容充電時間 + 受溫度係數影響的時間
    = (Amplifier Settling Time) + (Holding Capacitor Charging Time) + (Temperature Coefficient)
    = Tamp + Tc + Tcoff
    = Tamp + Tc + [(工作溫度-25) * 0.05u/C]
    
    Tamp，設置電路的準備時間 = 2us
    Tcoff，是工作溫度造成的時間變化，工作溫度以 50度 計算

    Tacq = 2us + 16.47us + [(50-25) * 0.05] = 19.72us
    ```

    在 AD轉換之前，至少需要 19.72us 的獲取時間

- AD 轉換時脈(Tad)的設置 (CHS2:CHS0 的設置)

  - 此選項依據工作時脈(Tosc)設置 Tad 的倍率，使得 Tad 達到 1.6us 的要求
    Tad = N * Tosc，
    
    | ADCS2 | ADCS1 | ADCS0 | Frquency | Operation     |
    | ----- | ----- | ----- | -------- | ------------- |
    | 0     | 0     | 0     | 1.25MHZ  | Tad = 2 Tosc  |
    | 0     | 0     | 1     | 5MHZ     | Tad = 8 Tosc  |
    | 0     | 1     | 0     | 20MHZ    | Tad = 32 Tosc |
    | 0     | 1     | 1     |          | Internal RC   |
    | 1     | 0     | 0     | 2.5MHZ   | Tad = 4 Tosc  |
    | 1     | 0     | 1     | 10MHZ    | Tad = 16 Tosc |
    | 1     | 1     | 0     | 40MHZ    | Tad = 64 Tosc |
    | 1     | 1     | 1     |          | Internal RC   |

  - 10bit的AD轉換，每一個bit的轉換時間為 Tad，
    
    每次轉換還需要包含設置時間和數據傳輸時間，
    因此，要完成 10bit 的轉換，至少需要 12 Tad，

    <img src="doc/adc/ad-conversion-cycle.png" width=80% height=auto>
    
    PIC16f877 的 Tad = 1.6us，因此要完成 10bit 的AD轉換，
    需要 12Tad = 12 * 1.6 = 19.2us

  - 時脈放大倍率的計算

    | 振盪器工作週期             | 倍率                         |
    | -------------------------- | ---------------------------- |
    | Tosc = 1 / 1.25MHZ = 0.8us | Tad / Tosc = (1.6/0.8) = 2   |
    | Tosc = 1 / 5MHZ = 0.2us    | Tad / Tosc = (1.6/0.2) = 8   |
    | Tosc = 1 / 20MHZ = 0.05us  | Tad / Tosc = (1.6/0.05) = 32 |
    | Tosc = 1 / 2.5MHZ = 0.4us  | Tad / Tosc = (1.6/0.4) = 4   |
    | Tosc = 1 / 10MHZ = 0.1us   | Tad / Tosc = (1.6/0.1) = 16  |
    | Tosc = 1 / 40MHZ = 0.025us | Tad / Tosc = (1.6/0.2) = 64  |
    
  - 設置範例
    
    選用 8MHZ 的外部震盪器，Tosc = 1/8 = 0.125us，為滿足 Tad = 1.6us 的需求，需要增加 Tosc 的時間，
    
    增加倍率為 Tad / Tosc = 1.6 / 0.125 = 12.8

    不能選用 Tad = 8 Tosc 的設置，Tad = 8 * 0.125 = 1us < Tad = 1.6us 的需求

    因此，選擇 Tad = 16 Tosc 的設置，將 ADCS2:ADCS0 = 101

- 在 Sleep-mode 下使用 ADC
  - 在 Sleep-mode 下只能使用內部的RC電路作為 Tad 的來源，
    若使用 Sleep-mode 指令前，沒有選擇內部的RC電路作為 Tad 的來源，
    則之前的 AD 轉換會被終止，且 ADC模組也會被關閉，但 ADON 仍然保持為1

  - 當 ADCS2:ADCS0 = *11，表示使用內部的RC電路作為Tad的來源，
    此時，Tad 與外部震盪器的頻率無關，

  - 內部的RC電路作為 Tad 的來源，固定為 4us，且會有 2us - 6us 的變動，
    只推薦工作時脈 > 1MHZ，且 Sleep-Mode 下使用
  
  - Sleep-mode 下若使用 AD interrupt，將會使晶片從睡眠中被喚醒，
    若沒有啟用 AD-interrupt，完成轉換後，ADC 會自動被關閉，即使 ADON 仍然保持為1，

## 範例
- [read_analog_LM35_byReg](read_analog_LM35_byReg/)
- [read_analog_LM35_byReg_16F88](read_analog_LM35_byReg_16F88/)
- [adc-tutorial](https://circuitdigest.com/microcontroller-projects/pic-microcontroller-pic16f877a-adc-tutorial)

## Ref
- [Analog-to-Digital Converter module @ datasheet](p129@datasheet)
