#include <xc.h>
#include "esp8266.h"

// crystal = 20MHZ
#define _XTAL_FREQ 20000000

#define RS RD2
#define EN RD3
#define D4 RD4
#define D5 RD5
#define D6 RD6
#define D7 RD7

#pragma config FOSC = HS   // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF  // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON  // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF   // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF   // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF   // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF    // Flash Program Memory Code Protection bit (Code protection off)

void Initialize_UART(void)
{
    // 初始化 UART，參考，p113 of datasheet

    // 設置 C6/C7 接腳的數據傳輸方向
    TRISCbits.TRISC6 = 0;   // output
    TRISCbits.TRISC7 = 1;   // input

    // 開啟 UART 的功能
    // SPEN = Serial Port Enable bit
    // SPEN 將UART的暫存器連接到 TX/RX 接腳 (使 TX/RX 的 Pin-Buffer 連接到 PIN)

    RCSTAbits.SPEN = 1;

    // 開啟非同步傳輸模式，
    TXSTAbits.SYNC = 0;

    // 設定 high-speen-baud-rate，只對非同步傳輸有效
    TXSTAbits.BRGH = 1;

    // 設置 BaudRate
    //      SYNC=0=異步傳輸 / BGH=1=高速傳輸時，
    //      Baud Rate = FOSC/(16 (X + 1)) = 20M/176 = 113,636.3636363636 ~ 115200
    SPBRG = 10;

    // 設置RX/TX的傳輸長度，是否啟用 第9位(bit-8)
    TXSTAbits.TX9 = 0;
    RCSTAbits.RX9 = 0;

    // 開啟Tx傳輸的功能，使發送數據的TSR暫存器生效
    TXSTAbits.TXEN = 1;

    // 開啟RX接收的功能，使接收數據的RSR暫存器生效
    // 注意，RX 接收方式有兩種，CREN=連續接收，SREN=單次接收
    RCSTAbits.CREN = 1;
}

// 根據 傳入data_bit的低四位，設置DB7-DB4的內容，
//      用於4bit-mode下，透過 DB7-D4 設置IR命令，
//      RS RW | DB7 DB6 DB5 DB4 
//      例如，data_bit = 0x08 = 0b0000_1000，則 D7 = 1，D6=D5=D4=0
//      住，此函數只有設置 DB7-DB4 的功能，沒有傳送的功能
void Lcd_SetBit(char data_bit) 
{
    if (data_bit & 1)
    {
        D4 = 1;
    }
    else
    {
        D4 = 0;
    }

    //
    if (data_bit & 2)
    {
        D5 = 1;
    }
    else
    {
        D5 = 0;
    }

    //
    if (data_bit & 4)
    {
        D6 = 1;
    }
    else
    {
        D6 = 0;
    }

    if (data_bit & 8)
    {
        D7 = 1;
    }
    else
    {
        D7 = 0;
    }
}

// 將指令寫入 IR 中
void Lcd_Cmd(char a)
{
    // 選擇 IR
    RS = 0;
    // 根據 a 的值，設置 D4-D7 的內容
    Lcd_SetBit(a);

    // EN 接腳致能，使 D4-D7 的內容寫入 LCD 中
    EN = 1;
    __delay_ms(4);

    EN = 0;
}

// 透過 4-bit-mode 傳送 1Byte-data 給 DR
void Lcd_Print_Char(char data)
{
    // 將數據拆分為高4位和低四位
    char Lower_Nibble, Upper_Nibble;

    Lower_Nibble = data & 0x0F;
    Upper_Nibble = data & 0xF0;

    // 選擇 DR
    RS = 1;

    // ==== 先傳送高四位 ====
    Lcd_SetBit(Upper_Nibble >> 4);

    // 開始傳送
    EN = 1;
    __delay_us(40);

    // 關閉傳送
    EN = 0;

    // ==== 再傳送高四位 ====
    Lcd_SetBit(Lower_Nibble); // Send Lower half

    EN = 1;
    __delay_us(40);

    EN = 0;
}

// 傳送字串給DR，連續調用 Lcd_Print_Char()
void Lcd_Print_String(char *a)
{
    int i;
    for (i = 0; a[i] != '\0'; i++)
        Lcd_Print_Char(a[i]); // Split the string using pointers and call the Char function
}

void Lcd_Start()
{
    // 將 RDx 設置為 output
    TRISD = 0x00;
    
    // 將 PORTD 的內容清除為 0
    PORTD = 0x00;

    __delay_ms(55);

    Lcd_Cmd(0x03);
    __delay_ms(5);

    Lcd_Cmd(0x03);
    __delay_ms(11);

    Lcd_Cmd(0x03);

    Lcd_Cmd(0x02); // 02H is used for Return home -> Clears the RAM and initializes the LCD
    Lcd_Cmd(0x02); // 02H is used for Return home -> Clears the RAM and initializes the LCD
    
    // 0b0000_1000 = DisplayOnOff = 0b0000_1DCB
    // 
    Lcd_Cmd(0x08); // Select Row 1
    Lcd_Cmd(0x00); // Clear Row 1 Display

    Lcd_Cmd(0x0C); // Select Row 2
    Lcd_Cmd(0x00); // Clear Row 2 Display

    Lcd_Cmd(0x06);
}

void Lcd_Clear()
{
    Lcd_Cmd(0); // Clear the LCD
    Lcd_Cmd(1); // Move the curser to first position
}

void Lcd_Set_Cursor(char a, char b)
{
    char temp, z, y;

    if (a == 1)
    {
        temp = 0x80 + b - 1; // 80H is used to move the curser
        z = temp >> 4;       // Lower 8-bits
        y = temp & 0x0F;     // Upper 8-bits

        Lcd_Cmd(z); // Set Row
        Lcd_Cmd(y); // Set Column
    }
    else if (a == 2)
    {
        temp = 0xC0 + b - 1;
        z = temp >> 4;   // Lower 8-bits
        y = temp & 0x0F; // Upper 8-bits
        Lcd_Cmd(z);      // Set Row
        Lcd_Cmd(y);      // Set Column
    }
}


void main()
{
    TRISD = 0x00;

    Initialize_UART();

    Lcd_Start();
    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("Circuit Digest");
    Lcd_Set_Cursor(2, 1);
    Lcd_Print_String("ESP5266 with PIC");

    __delay_ms(1500);
    Lcd_Clear();

    // 
    do
    {
        Lcd_Set_Cursor(1, 1);
        Lcd_Print_String("ESP not found");

    } while (!esp8266_isStarted()); // 阻塞，直到收到 ESP8266 回復的 OK

    // 收到 OK 才會繼續往下執行
    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("ESP is connected");
    __delay_ms(1500);
    Lcd_Clear();

    // 將 esp8266 設置為 softAP 模式
    //      發送 AT+CWMODE=2 的命令給ESP8266
    //      阻塞，直到收到 ESP8266 回復的 OK
    esp8266_mode(2);

    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("ESP set as AP");
    __delay_ms(1500);
    Lcd_Clear();

    // 設置 esp8266 要連線的 wifi
    //      發送 AT+CWJAP="hua","fake" 的命令給ESP8266
    //      阻塞，直到收到 ESP8266 回復的 OK
    esp8266_connect("hua", "fake");

    Lcd_Set_Cursor(1, 1);
    Lcd_Print_String("AP configured");
    __delay_ms(1500);

    while (1)
    {
        
    }
}