#include "esp8266.h"
#include <stdio.h>
#include <string.h>

// 使用 MPLAB 的 XC 編譯器
#include <xc.h>

// 透過TX發送 1個字元(1byte) 的數據
void _esp8266_putch(char bt)
{
    // TXIF 用於判斷 TXREG 是否為空，
    //      TXIF = 0，代表前一次發送未完成，需等待
    //      TXIF = 1，代表前一次發送已完成，可寫入新發送數據

    while (!TXIF)
    {
        NOP();
    }

    // 將 byte 數據寫入 TXREG 中
    TXREG = bt;
}

// 發送字串
void _esp8266_print(unsigned const char *ptr)
{
    while (*ptr != 0)
    {
        // 發送後，將指針指向下一個字元
        _esp8266_putch(*ptr++);
    }
}

// 透過RX接收 1個字元(1byte) 的數據
char _esp8266_getch()
{
    // 讀取 RCREG 暫存器的內容，會覆蓋前一次的錯誤
    // 因此，接收數據前，先判斷前一次接收是否有錯誤，
    //      若有，將 CREN 或 SREN 設置為 0，再將 CREN 或 SREN 重新設置為1

    // 有溢位錯誤
    if (OERR)
    {
        // 重設 CREN 或 SREN
        CREN = 0;
        CREN = 1;
    }

    // RCIF 用於判斷 RCREG 是否為空，
    //      RCIF = 0，代表前一次接收未完成，需等待
    //      RCIF = 1，代表前一次接收已完成，可寫入新發送數據
    while (!RCIF)
    {
        NOP();
    }

    // 讀取 RCREG 的內容，並返回
    return RCREG;
}

// 連續讀取的RX的回應，直到接收完成，返回讀取的內容
inline unsigned char _esp8266_waitResponse(void)
{
    unsigned char so_far[6] = {0, 0, 0, 0, 0, 0};
    unsigned const char lengths[6] = {2, 5, 4, 9, 6, 6};
    unsigned const char *strings[6] = {"OK", "ready", "FAIL", "no change", "Linked", "Unlink"};
    unsigned const char responses[6] = {ESP8266_OK, ESP8266_READY, ESP8266_FAIL, ESP8266_NOCHANGE, ESP8266_LINKED, ESP8266_UNLINK};

    unsigned char received;
    unsigned char response;

    bool continue_loop = true;

    while (continue_loop)
    {
        received = _esp8266_getch();

        for (unsigned char i = 0; i < 6; i++)
        {
            if (strings[i][so_far[i]] == received)
            {
                so_far[i]++;

                if (so_far[i] == lengths[i])
                {
                    response = responses[i];
                    continue_loop = false;
                }
            }
            else
            {
                so_far[i] = 0;
            }
        }
    }

    return response;
}

// 指定字串出現前，進入等待迴圈，返回已經讀取的字元數
inline uint16_t _esp8266_waitFor(unsigned char *string)
{
    unsigned char so_far = 0;
    unsigned char received;
    uint16_t counter = 0;

    do
    {
        received = _esp8266_getch();
        counter++;

        if (received == string[so_far])
        {
            so_far++;
        }
        else
        {
            so_far = 0;
        }
    } while (string[so_far] != 0);

    return counter;
}

// 測試 esp8266 模組是否已經啟動
bit esp8266_isStarted(void)
{
    _esp8266_print("AT\r\n");
    return (_esp8266_waitResponse() == ESP8266_OK);
}

// 重新啟動 esp8266 模組
bit esp8266_restart(void)
{
    _esp8266_print("AT+RST\r\n");
    if (_esp8266_waitResponse() != ESP8266_OK)
    {
        return false;
    }
    return (_esp8266_waitResponse() == ESP8266_READY);
}

// 用於開啟/關閉 esp8266 的 echo-command 功能
void esp8266_echoCmds(bool echo)
{
    // ATE 1 \r\n，開啟 echo-command
    // ATE 0 \r\n，關閉 echo-command

    _esp8266_print("ATE");

    if (echo)
    {
        _esp8266_putch('1');
    }
    else
    {
        _esp8266_putch('0');
    }

    _esp8266_print("\r\n");

    // 等待，執行收到 esp8266 發送的 OK
    _esp8266_waitFor("OK");
}

// 用於設置 esp8266 的 操作模式
void esp8266_mode(unsigned char mode)
{
    _esp8266_print("AT+CWMODE=");
    _esp8266_putch(mode + '0');
    _esp8266_print("\r\n");
    _esp8266_waitResponse();
}

// 設置 esp8266 要連接的 wifi
unsigned char esp8266_connect(unsigned char *ssid, unsigned char *pass)
{
    _esp8266_print("AT+CWJAP=\"");
    _esp8266_print(ssid);
    _esp8266_print("\",\"");
    _esp8266_print(pass);
    _esp8266_print("\"\r\n");
    return _esp8266_waitResponse();
}

// 使 esp8266 關閉當前的 wifi 連線
void esp8266_disconnect(void)
{
    _esp8266_print("AT+CWQAP\r\n");
    _esp8266_waitFor("OK");
}

// 顯示 esp8266 的 ip (in byte)
void esp8266_ip(unsigned char *store_in)
{
    _esp8266_print("AT+CIFSR\r\n");
    unsigned char received;
    do
    {
        received = _esp8266_getch();
    } while (received < '0' || received > '9');

    for (unsigned char i = 0; i < 4; i++)
    {
        store_in[i] = 0;
        do
        {
            store_in[i] = 10 * store_in[i] + received - '0';
            received = _esp8266_getch();
        } while (received >= '0' && received <= '9');

        received = _esp8266_getch();
    }

    _esp8266_waitFor("OK");
}

// 使 esp8266 與遠端主機建立一個 TCP 或 UDP 的連線
bit esp8266_start(unsigned char protocol, char *ip, unsigned char port)
{
    _esp8266_print("AT+CIPSTART=\"");

    if (protocol == ESP8266_TCP)
    {
        _esp8266_print("TCP");
    }
    else
    {
        _esp8266_print("UDP");
    }

    _esp8266_print("\",\"");
    _esp8266_print(ip);
    _esp8266_print("\",");

    unsigned char port_str[5] = "\0\0\0\0";

    sprintf(port_str, "%u", port);
    _esp8266_print(port_str);
    _esp8266_print("\r\n");

    if (_esp8266_waitResponse() != ESP8266_OK)
    {
        return 0;
    }

    if (_esp8266_waitResponse() != ESP8266_LINKED)
    {
        return 0;
    }
    return 1;
}

// 發送請求給遠方主機
bit esp8266_send(unsigned char *data)
{
    unsigned char length_str[6] = "\0\0\0\0\0";
    sprintf(length_str, "%u", strlen(data));
    _esp8266_print("AT+CIPSEND=");
    _esp8266_print(length_str);
    _esp8266_print("\r\n");
    while (_esp8266_getch() != '>')
        ;
    _esp8266_print(data);
    if (_esp8266_waitResponse() == ESP8266_OK)
    {
        return 1;
    }
    return 0;
}

// 接收給遠方主機的回應
void esp8266_receive(unsigned char *store_in, uint16_t max_length, bool discard_headers)
{
    _esp8266_waitFor("+IPD,");

    uint16_t length = 0;
    unsigned char received = _esp8266_getch();

    do
    {
        length = length * 10 + received - '0';
        received = _esp8266_getch();
    } while (received >= '0' && received <= '9');

    if (discard_headers)
    {
        length -= _esp8266_waitFor("\r\n\r\n");
    }

    if (length < max_length)
    {
        max_length = length;
    }

    uint16_t i;

    for (i = 0; i < max_length; i++)
    {
        store_in[i] = _esp8266_getch();
    }

    store_in[i] = 0;

    for (; i < length; i++)
    {
        _esp8266_getch();
    }

    _esp8266_waitFor("OK");
}
