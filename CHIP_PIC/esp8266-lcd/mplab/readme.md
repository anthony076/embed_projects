## 使用 vscode 開啟
- 本範例預設使用 MPLAB-IDE 的 XC 編譯器，要使用vscode編譯，
  利用 .vscode 目錄的 c_cpp_properties.json，指定編譯器和 header 的位置

- 在 vscode 中安裝並開啟 c/c++ 的插件

## 取得 hex
- 執行 build.bat
- 若有編譯錯誤，透過 MPLAB-IDE開啟專案，確認可正常編譯後，才改用 build.bat

## 執行模擬
- 執行 proteus\simulation-circuit.pdsprj
- 指定 hex 檔
- 執行模擬
- Debug > Virtual Terminal - T2 開啟終端
- 點擊終端 > 手動輸入 OK，模擬 ESP8266 的 response 後，可以看到LCD 有對應的變化



