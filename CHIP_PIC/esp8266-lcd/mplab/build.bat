@echo off

@REM set Make="C:\Program Files\MPLABX\gnuBins\GnuWin32\bin\make.exe"
set Make="C:\Program Files\Microchip\MPLABX\v6.00\gnuBins\GnuWin32\bin\make.exe"

del /F /S /Q pic-esp8266-lcd.hex
%Make% build

copy /Y dist\default\production\mplab.production.hex .\pic-esp8266-lcd.hex

rmdir /S /Q build dist