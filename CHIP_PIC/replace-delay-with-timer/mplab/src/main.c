#include <xc.h>

#define _XTAL_FREQ 20000000 // 20MHZ

#pragma config FOSC = HS   // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF  // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON  // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF   // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF   // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF   // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF    // Flash Program Memory Code Protection bit (Code protection off)

// 本範例使用Timer模組實現LED閃爍的功能，每3秒將 RC0(LED)反向一次
//      Fosc = 振盪器頻率 = 20000000
//      TMR0 = 100，使計數器從 100 數到 256 
//      Precal = 64，使用 64 倍分頻器
//      中斷發生時間 Tic = ( 4 * (256 - TMR0) * Prescal ) / Fosc
//                     = ( 4 * (256 - 100) * 64 ) / 20000000 = 0.0019968 ~ 2ms
//      中斷計數 Count = 3s / 2ms = 1.5K = 1500
//      中斷計數 Count = 1s / 2ms = 0.5K = 500

// 計算 interrupt 發生中斷的次數
int interrupt_count = 0;

void config_gpio()
{
    // 將 RC0 設置為輸出
    TRISC0 = 0;
    
    // 將 RC0 初始化為 0
    RC0 = 0;    
}

void config_timer()
{    
    char PS = 0b101;  // set prescaler = 64
    char PSA = 0 << 3;  // assign prescaler to Timer
    char T0CS = 0 << 5;  // use internal clock (CLK0))
            
    OPTION_REG = T0CS | PSA | PS;
    
    // initial timer to 100, Timer will +1 from 100 to 255
    TMR0 = 100;
    
    // 開啟 Timer0
    TMR0IE = 1;
    
    // 允許外設發起中斷
    PEIE = 1;
    
    // 允許全局中斷
    GIE = 1;
}

void interrupt ISR()
{
  // 判斷是否是 Timer 的溢位中斷
  if (TMR0IF == 1)
  {
    // Timer 的溢位中斷發生後，進行清除/重設相關暫存器
      
    // 將 Timer 的初始值重置為 100
    TMR0 = 100;
    
    // 清除中斷旗標，才能接收下一個中斷
    TMR0IF = 0;
    
    // 中斷次數 +1
    interrupt_count++;
  }
  
  // 判斷中斷次數是否已達預定次數
  if (interrupt_count == 500)
  {
      interrupt_count = 0;
      
      // 將 LED 進行反向
      RC0 = ~RC0;
  }
}

void main(void) 
{
    config_gpio();
    config_timer();
    
    while(1){}
    
}
