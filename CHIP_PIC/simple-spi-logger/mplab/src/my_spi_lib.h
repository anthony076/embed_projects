
// 初始化
void SPI_Initialize_Master();
void SPI_Initialize_Slave();

// 寫入相關函數
void SPI_Write(char incoming);

// 讀取相關函數
unsigned SPI_Ready2Read();
char SPI_Read();