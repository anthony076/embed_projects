#include <xc.h>

// ==== For SSPSTAT ====
#define SAMPLE_AT_END 1 << 7
#define SAMPLE_AT_MIDDLE 0 << 7

#define TRANSMIT_ACTIVE_2_IDLE 1 << 6
#define TRANSMIT_IDLE_2_ACTIVE 0 << 6

// ==== For SSPCON ====
#define SPI_ENABLE 1 << 5
#define SPI_DISABLE 0 << 5

#define IDLE_HIGH 1 << 4
#define IDLE_LOW 0 << 4

#define SLAVE_SS_DISABLE 0x0101
#define SLAVE_SS_ENABLE 0x0100
#define MASTER_TMR2_OUTPUT 0x0011
#define MASTER_F64 0x0010 // master-mode, Fosc/64
#define MASTER_F16 0x0001 // master-mode, Fosc/16
#define MASTER_F4 0x0000  // master-mode, Fosc/4 = 20MHZ/4=5MHZ

// ==== master-spi-device 的初始化 ====
void SPI_Initialize_Master()
{
  TRISC5 = 0; // RC5=SDO=output
  TRISC3 = 0; // RC3=SCK=output, Master provide clk to slave
  
  // (CKE = transmit@Idle->Active) == (CPHA = sample@Idle->Active) 
  // CKE 與 CPHA 取樣邊緣的結果一致，在 proteus 要設置 sample-edge 為 Idle->Active
  // 因為 proteus 使用的是 CPHA-base 的 sample-edge
  SSPSTAT = SAMPLE_AT_MIDDLE | TRANSMIT_IDLE_2_ACTIVE; // SSPSTAT = 0b00000000;
  SSPCON = SPI_ENABLE | IDLE_LOW | MASTER_F4; //SSPCON = 0b00100000;
}

// ==== slave-spi-device 的初始化 ====
void SPI_Initialize_Slave()
{
  TRISC5 = 0; // RC5=SDO=output
  TRISC3 = 1; // RC3=SCK=input, receive clk from master

  SSPSTAT = SAMPLE_AT_MIDDLE | TRANSMIT_IDLE_2_ACTIVE; // SSPSTAT = 0b00000000;
  SSPCON = SPI_ENABLE | IDLE_LOW | SLAVE_SS_ENABLE; //SSPCON = 0b00100100;
}

// ==== 判斷 SSPBUF 是否已滿，SSPBUF 已滿才能讀取數據 ====
unsigned SPI_Ready2Read()
{
    char bf = 0b00000001;

    if (SSPSTAT & bf)
        return 1;
    else
        return 0;
}

// ==== 從 SSPBUF 讀取完成接收的數據 ====
char SPI_Read()
{
    // wait if BF=0, buffer not full
    while ( !SSPSTATbits.BF );

    return SSPBUF; 
}

// ==== 將數據寫入 SSPBUF ====
void SPI_Write(char incoming)
{
    SSPBUF = incoming;
}