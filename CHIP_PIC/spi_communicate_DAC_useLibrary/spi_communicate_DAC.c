// ==== Pin assignment ====
//     RB1 = CS, RC3 = clock, RC5 = MOSI
//     external osc 12MHZ

// ==== init ADC ====

#include <stdio.h>
#include <stdlib.h>

#define _SELECT_SLAVE 0
#define _DISELECT_SLAVE 1

sbit cs at PORTB.B1; // Chip-select-bit

void DAC_Output(unsigned int valueDAC)
{
     char temp;

     cs = _SELECT_SLAVE;

     // ==== Send HIGH Byte ====
     // step1��shift high-byte to low-byte, then keep low-byte only

     // 0d1776 =         0b 0000 0110 1111 0000
     // 0d1776 >> 8 =    0b 0110        4bit (shift high-byte to low-byte)
     // & 0x0f           0b 0000 1111        (keep low-byte only)
     //             =    0b 0000 0110   4bit -> 8bit
     temp = (valueDAC >> 8) & 0x0F;

     // step2��get setting-value and put into new-high-byte
     // then use | to combine setteing and low-byte above

     // low-byte from step1 =           0b 0000 0110
     // setting-value =                 0b 0011 0000 = 0x30 (see doc)
     // --------------------
     // OR operation                    0b 0011 0110 = 0x36
     //                                    ^^^^ ^^^^
     //                                setting   origin-high-byte
     temp |= 0x30;
     SPI1_Write(temp);

     // ==== Send LOW Byte ====
     // since spi only sent 8 bit at a time,
     // so, 0b00000110 11110000 will keep 0x11110000 = 0xF0 only

     temp = valueDAC;
     SPI1_Write(temp);

     cs = _DISELECT_SLAVE;
}

void main()
{
     // set CS to output
     TRISB.B1 = 0;
     PORTB.B1 = 0;

     // set RC3(SCK) and RC5(SDO) Output
     TRISC.B3 = 0;
     TRISC.B5 = 0;

     SPI1_Init_Advanced(_SPI_MASTER_OSC_DIV4, _SPI_DATA_SAMPLE_MIDDLE, _SPI_CLK_IDLE_LOW, _SPI_LOW_2_HIGH);

     while (1)
     {
          DAC_Output(2048);
          delay_ms(1000);
          DAC_Output(4095);
          delay_ms(1000);
     }
}