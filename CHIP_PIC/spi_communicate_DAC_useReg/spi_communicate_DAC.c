// ==== Pin assignment ====
//     RB1 = CS, RC3 = clock, RC5 = MOSI
//     external osc 12MHZ

// ==== init ADC ====

#include <stdio.h>
#include <stdlib.h>

#define _SELECT_SLAVE 0
#define _DISELECT_SLAVE 1

sbit cs at PORTB.B1;          // Chip-select-bit
sbit buf_full at SSPSTAT.B0; // Buffer-full-bit

void write_no_read(unsigned int dacValue)
{
     char lowByte, highByte;

     cs = _SELECT_SLAVE;

     // ==== Send HIGH Byte ====
     // step1shift high-byte to low-byte, then keep low-byte only

     // 0d1776 =         0b 0000 0110 1111 0000
     // set SSP[3:0] = 0000, F
     // & 0x0f           0b 0000 1111        (keep low-byte only)
     //             =    0b 0000 0110   4bit -> 8bit
     highByte = (dacValue >> 8) & 0x0F;

     // then use | to combine setteing and low-byte above

     // low-byte from step1 =           0b 0000 0110
     // setting-value =                 0b 0011 0000 = 0x30 (see doc)
     // --------------------
     // OR operation                    0b 0011 0110 = 0x36
     //                                    ^^^^ ^^^^
     //                                setting   origin-high-byte
     highByte |= 0x30;

     // send data to slave
     SSPBUF = highByte;
     while(buf_full);      // check and wait the command is sent to slave
     SSPBUF;               // make sure to read SSPBUF, in osrder to incease counter
                           // make sure to read SSPBUF no matter excute a read / write command
     delay_us(3);          // wait for MCU operation

     // ==== Send LOW Byte ====
     // since spi only sent 8 bit at a time,
     // so, 0b00000110 11110000 will keep 0x1111 0000=0xF0 only

     lowByte = dacValue & 0x0F;
     
     // send data to slave
     SSPBUF = lowByte;
     while(buf_full);      // check and wait the command is sent to slave
     SSPBUF;               // make sure to read SSPBUF, in osrder to incease counter
                           // make sure to read SSPBUF no matter excute a read / write command
     delay_us(3);          // wait for MCU operation
     
     cs = _DISELECT_SLAVE;
}


void initSPI()
{
     // enable SSP module(power-up)
     SSPCON.SSPEN = 1;

     // set clock-idle-level = LOW
     SSPCON.CKP = 0;
     
     // set Fosc / 4
     SSPCON.SSPM3 = 0;
     SSPCON.SSPM2 = 0;
     SSPCON.SSPM1 = 0;
     SSPCON.SSPM0 = 0;

     // sample at middle of data output time
     SSPSTAT.SMP = 0;
     
     // raising-edge( 0->1 )
     SSPSTAT.CKE = 0;
}


void main()
{
     // set CS to output
     TRISB.B1 = 0;
     PORTB.B1 = 0;

     // set RC3(SCK) and RC5(SDO) Output
     TRISC.B3 = 0;
     TRISC.B5 = 0;

     initSPI();

     while (1)
     {
          write_no_read(2048);
          delay_ms(1000);

          write_no_read(4095);
          delay_ms(1000);
     }
}