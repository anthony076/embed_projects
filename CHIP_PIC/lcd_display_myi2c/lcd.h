
// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    1    0    N    F    0    0   Function Set
// 0    0    0    0    1    0    1    0    0    0   =   0x28
#define _SET_LINE_DOT 0x28

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    0    1    D    C    B   Display on/off Controll
// 0    0    0    0    0    0    1    1    0    0   =   0x0c
#define _SET_DISPLAY_CONTROL 0x0c

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    0    0    1    I/D  S   Entry Mode Set
// 0    0    0    0    0    0    0    1    1    0   =   0x06
#define _SET_ENTRY_MODE 0x06

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    1   S/C  R/L   -    -  Cursor/Display Shift
// 0    0    0    0    0    1    1    0    0    0   =   0x18
#define _SHIFT_DISPLAY_LEFT 0x18

// RS  R/W  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
// 0    0    0    0    0    1   S/C  R/L   -    -  Cursor/Display Shift
// 0    0    0    0    0    1    1    1    0    0   =   0x1c
#define _SHIFT_DISPLAY_RIGHT 0x1c

#define _CLEAR_DISPLAY 0x01

// Pin define on PCF8574
// P7 P6 P5 P4 P3 P2 P1 P0
// D7 D6 D5 D4 x  x  En RS
//
// define RS pin on/off
#define _SELECT_COMMAND_REG 0x00
#define _SELECT_DATA_REG 0x01

// define EN pin on/off
#define _EN_ON 0x02
#define _EN_OFF 0x00

#define _FIRST_LINE 0x80
#define _SECOND_LINE 0xc0

// define by PCF8574 @ datasheet-p5
// since A0-A2 are connected to GND��therefore the slave address become 0b0010000 = 0x40
#define _I2C_ADDR 0x40

typedef unsigned char uchar_t;

typedef enum
{
    COMMAND,
    DATA,
} LCD_REGISTER_TYPE;

typedef enum
{
    LEFT,
    RIGHT,
} SHIFT_DIRECTION;

void _write_4byte(uchar_t chunk, LCD_REGISTER_TYPE location);
void _write_8byte(uchar_t byte, LCD_REGISTER_TYPE location);

void clear_lcd();
void home();
void shift(SHIFT_DIRECTION direction);
void print(char *text);

void lcd_init();