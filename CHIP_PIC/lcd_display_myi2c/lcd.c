
#include "lcd.h"

int i;

// ======================
// Send 4byte data into Instruction-Reg or Display-Data-Reg
// @param chunk: 4 bytes of DATA, could be high-order-4bytes or low-order-4bytes
// @param location: the register where the data will be sent to
// ======================
static void _write_4byte(uchar_t chunk, LCD_REGISTER_TYPE location)
{

    if (location == COMMAND)
    {
        I2C1_Wr((chunk << 4) | _EN_ON | _SELECT_COMMAND_REG);
        I2C1_Wr((chunk << 4) | _EN_OFF | _SELECT_COMMAND_REG);
    }
    else
    {
        I2C1_Wr((chunk << 4) | _EN_ON | _SELECT_DATA_REG);
        I2C1_Wr((chunk << 4) | _EN_OFF | _SELECT_DATA_REG);
    }
}

// ======================
// Send 8byte data into Instruction-Reg or Display-Data-Reg
// @param byte: comple 8 bytes Command or Data.
// @param location: the register where the data will be sent to
// ======================
static void _write_8byte(uchar_t byte, LCD_REGISTER_TYPE location)
{

    if (location == COMMAND)
    {
        // send high-order-bytes
        _write_4byte(byte >> 4, COMMAND);

        // send low-order-bytes
        _write_4byte(byte & 0x0F, COMMAND);
    }
    else
    {
        // send high-order-bytes
        _write_4byte(byte >> 4, DATA);

        // send low-order-bytes
        _write_4byte(byte & 0x0F, DATA);
    }

    // ==== take 8byte into 4byte + 4byte, and send twice ====
}

extern void clear_lcd()
{
    _write_8byte(_CLEAR_DISPLAY, COMMAND);
    delay_ms(5);
}

// ======================
// Set cursor back to home
// ======================
extern void home()
{
    _write_8byte(_FIRST_LINE, COMMAND);
    delay_ms(5);
}

extern void shift(SHIFT_DIRECTION direction)
{
    if (direction == LEFT)
    {
        _write_8byte(_SHIFT_DISPLAY_LEFT, COMMAND);
    }
    else
    {
        _write_8byte(_SHIFT_DISPLAY_RIGHT, COMMAND);
    }

    delay_ms(5);
}

// ======================
// initial lcd procedure
// ======================
extern void lcd_init()
{
    I2C1_Start();
    I2C1_Wr(_I2C_ADDR);

    // wait for lcd power on
    delay_ms(15);

    //step1-3, Fake 8bit-mode Function Set, for initial only
    for (i = 0; i <= 2; i++)
    {
        _write_4byte(0b0011, COMMAND);
        delay_ms(5);
    }

    // step4, Fake 4bit-mode Function Set, , for initial only
    _write_4byte(0b0010, COMMAND);
    delay_ms(5);

    // step5, Real Function Set, N=2, F=0
    _write_8byte(_SET_LINE_DOT, COMMAND);
    delay_ms(5);

    // step6, Display on/off Control
    _write_8byte(_SET_DISPLAY_CONTROL, COMMAND);
    delay_ms(5);

    // step8, Entry mode set
    _write_8byte(_SET_ENTRY_MODE, COMMAND);
    delay_ms(5);

    // step7, Clear LCD
    clear_lcd();
}

// ======================
// Print text on LCD
// ======================
extern void print(char *text)
{
    while (*text != '\0')
    {
        _write_8byte(*text++, DATA);
    }
}