## EEPROM/FLASH SPEC
- Up to 8K x 14 words of FLASH Program Memory
  - 4K: PIC16F873 / PIC16F874
  - 8K: PIC16F876 / PIC16F877

- Up to 256 x 8 bytes of EEPROM Data Memory
  - 128: PIC16F873 / PIC16F874
  - 256: PIC16F876 / PIC16F877

- Low power, high speed CMOS FLASH/EEPROM

## EEPROM 的使用
- EEPROM 使用到的暫存器
  - EEDATA暫存器(10Ch): EEPROM `Data` Register `Low` Byte，要寫入EEPROM的數據
  - EEDATH暫存器(10Eh): EEPROM `Data` Register `High` Byte，要寫入EEPROM的數據
  - 14bit 的 EEPROM位址，EEDATH5:EEDATH0 + EEDATA7:EEDATA0

  - EEADR暫存器(10Dh): EEPROM `Address` Register `Low` Byte，要存取的EEPROM位址
  - EEADRH暫存器(10Fh): EEPROM `Address` Register `High` Byte，要存取的EEPROM位址
  - 13bit 的 EEPROM位址，EEADRH4:EEADRH0 + EEADR7:EEADR0

  - EECON1暫存器(18Ch): EEPROM 功能控制暫存器1
  - EECON2暫存器(18Dh): EEPROM 功能控制暫存器2
  
  - EEIE位 @ PIE2暫存器(08Dh)的bit4: EEPROM Write Operation Interrupt Enable
  - EEIF位 @ PIR2暫存器(00Dh)的bit4: EEPROM Write Operation Interrupt Flag bit

- EECON1/EECON2暫存器的使用
  - EEPGD位，指定要存取 EEPROM 的位置是 data-memory區 或 flash-program-memory區
    - EEPGD = 0，存取 EEPROM 的 data-memory 區
    - EEPGD = 1，存取 EEPROM 的 flash-program-memory區
    - 注意，進行讀寫時，無法變更 EEPGD 的值
  
  - WRERR位，EEPROM 寫入錯誤的 Flag ，用來檢視寫入操作是否完成
    - WRERR = 1，寫入錯誤發生，MCLR=0 或 WDT-Reset 引起的寫入錯誤
    - WRERR = 0，寫入操作完成，沒有錯誤發生

  - WREN位，開啟 EEPROM 的寫入功能
    - WREN = 1，允許寫入 EEPROM
    - WREN = 0，禁止寫入 EEPROM

  - WR位，寫入開始控制位
    - WR = 1，開始寫入操作
    - WR = 0，寫入操作已完成
    - WR 只能由軟體設置為1，無法透過軟體將 WR設置為0，寫入完成後，會自動被設置為0

  - RD位，讀取開始控制位
    - RD = 1，開始讀取操作
    - RD = 0，寫入操作已完成
    - RD 只能由軟體設置為1，無法透過軟體將 RD設置為0，寫入完成後，會自動被設置為0

- `寫入`操作流程 (write-eeprom-data-memory)
  - step1，判斷前一次的寫入操作是否已經完成
    - 方法1，判斷 WR位是否清除為0
    - 方法2，判斷 EEIF位是否被設置為1 > 手動將 EEIF 清除為0

  - step2，透過 EEADR暫存器，寫入要存取的eeprom位址
  - step3，透過 EEDATA暫存器，寫入要寫入eeprom的數據

  - step4，將 EEPGD位 設置為 0，表示要存取 eeprom-data-memory

  - step5，將 WREN位 設置為 1，開放 EEPROM 的寫入功能

  - step6，若已開啟中斷，將中斷關閉，避免中斷引發的寫入錯誤

  - step7，執行寫入操作的特定指令
    - 7-1，將 55h 寫入 EECON2，
    - 7-2，將 AAh 寫入 EECON2，
    - 7-3，將 WR 設置為 1，開始進行寫入

  - step8，若需要中斷通知，啟用中斷
  - step9，將 WREN位設置為 0，關閉 EEPROM 的寫入操作，
    避免前次寫入未完成，又新增新的寫入

  - step10，判斷前一次的寫入操作是否已經完成
    - 方法1，判斷 WR位是否清除為0
    - 方法2，判斷 EEIF位是否被設置為1 > 手動將 EEIF 清除為0

- `讀取`操作流程 (read-eeprom-data-memory)
  - step1，透過 EEADR暫存器，寫入要讀取的 eeprom 位址
  - step2，將 EEPGD位 設置為 0，表示要存取 eeprom-data-memory
  - step3，將 RD 設置為 1，開始進行讀取
  - step4，從 EEDATA暫存器讀取已經獲取的內容

## 範例
- [internal-eeprom](internal_eeprom/)

## 參考
- [EEPROM Memory (Internal)](https://deepbluembedded.com/eeproms-internal-in-pic-microcontrollers/)
- [How to Save Data using EEPROM in PIC16F877A Microcontroller](https://circuitdigest.com/microcontroller-projects/using-pic-microcontroller-eeprom)