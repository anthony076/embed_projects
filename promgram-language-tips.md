- 將整數值轉換為字串
  ```c
  // 使用 內建的 itoa()
  // 詳見 CHIP_PIC\internal_eeprom\mplab\src\use-bultin.c
  #include <stdlib.h>
    ...
  
  int value = 1234;
  char svalue[10]; 

  itoa(svalue, value, 10);
  Lcd_Print_String(svalue);
  ```

- 取得 high/low byte
  ```c
  char full-8-bits = 0x12;
  char high-4-bits = (full-8-bits & 0xF0) >> 4 ;
  char low-4-bits = full-8-bits & 0x0F;
  ```

- 設置 reg 的推薦方法
```c
#define XX_ENABLE = 1 << 5  // XX-bit @ AA-reg-bit5
#define XX_DISABLE = 0 << 5

#define YY_ENABLE = 1 << 3  // YY-bit @ AA-reg-bit3
#define YY_DISABLE = 0 << 3

// 利用此方式可以清楚看出，XX位被設置為開啟，YY位被設置為關閉
// 不好的寫法，AA = 0b0010_0000，無法一眼看出設置值
AA = XX_ENABLE | YY_DISABLE
```

- violate 關鍵字
  - 作為指令關鍵字，確保本條指令不會因編譯器的優化而省略，
    且要求每次都小心地重新讀取這個變量的值，而不是使用保存在寄存器裡的備份

  - 使用場景
    - 常用於多線程訪問和修改的變量
    - 一個中斷服務子程序中會訪問到的非自動變量

  - [Ref](https://blog.csdn.net/siyuedeyu2525/article/details/52850034)