## 編譯器的選擇
- mikroC，
  - PIC、PIC32、dsPIC/PIC24、
  - AVR、
  - 8051
  - 支持 STMicroelectronics、Texas Instruments 和基於 Microchip 的 ARM® Cortex 微控制器

- mplab，Microchip 出的新版的IDE，
  - 支援 8bit-PIC / 8bit-AVR 晶片
  - 支援 16bit-PIC24 / 16bit-dsPIC 晶片
  - 支援 32bit-SAM(ARM) / 32bit-PIC32(MIPS) 晶片
  - 支援 asm 編譯器 (pic-as)，[參考](https://www.youtube.com/playlist?list=PLtuqBdbsL-DuQB1DQAowWUEhTdI4KCVIZ)

- [pic32-rs](https://github.com/kiffie/pic32-rs)，以 rust 編寫 PIC32 的 MCU

## 嵌入式基礎
- [ADC](Module/ADC.md)
- [GPIO基礎](Module/gpio.md)
- [震盪器基礎](Module/osc.md)

- [通訊協定基礎](Module/Communication.md)
  - [SPI 晶片用通訊協定](Module/SPI.md)
  - [I2C 晶片用通訊協定](Module/I2C.md)
  - [USB 晶片用通訊協定](Module/usb.md)
  - [mipi，手機用通訊協定](Module/mipi.md)
  - [can，車用通訊協定](Module/can.md)
  - [modbus，工業用通訊協定](Module/modbus.md)

## IDE 的使用
- IDE 模板
  - [mikroc-ide模板](templates/mikroc_template/)
  - [mplab-ide模板](templates/mplab_template/)
  - [vscode模板](templates/vscode_template/)

- MPLAB X IDE
  - [使用 vscode 執行 mplab 專案](IDE/pic_in_vscode.md)
  - [使用 MplabCodeConfigurator(MCC)，快速配置和使用內建Library](IDE/mplab_mcc_usage.md)
  - [debug 的使用](IDE/mplab_debug_usaage.md)
   
## CHIP 的使用
- PIC
  - [PIC 配置 (configuration-bit)](CHIP_PIC/common_config_bit.md)
  - [gpio 基礎](CHIP_PIC/gpio.md)
  - [adc 的使用](CHIP_PIC/adc.md)
  - [interrupt 的使用](CHIP_PIC/interrupt.md)
  - [timer 的使用](CHIP_PIC/timer.md)
  - [uart 的使用](CHIP_PIC/uart.md)
  - [spi 的使用](CHIP_PIC/MSSP_spi.md)
  - [i2c 的使用](CHIP_PIC/MSSP_i2c.md)
  - [內建eeprom的使用](CHIP_PIC/internal_eeprom_flash.md)

- ATMega  
- STM32
- ARM

## Peripheral 和 Sensor
- DAC，數位類比轉換器
  - [使用 MCP4921 晶片 - 使用 SPI 內建庫讀取](CHIP_PIC/spi_communicate_DAC_useLibrary/)
  - [使用 MCP4921 晶片 - 使用 reg 讀取](CHIP_PIC/spi_communicate_DAC_useLibrary/)
  
- LM35，溫度感測晶片
  - [使用內建庫讀取LM35](CHIP_PIC/read_analog_LM35/)
  - [透過 reg 讀取LM35](CHIP_PIC/read_analog_LM35/)

- LCD，液晶顯示
  - [HD44780U](Module/LCD_HD44780U.md)

- RTC，時間管理晶片
  - [pcf8563](CHIP_PIC/i2c-rtc-pcf8563/)
  - [ds1307](https://www.youtube.com/watch?v=tu4vGNo6mvc)

- EEPROM
  - [eeprom(FM24C64)使用範例](https://microcontrollerslab.com/i2c-communication-pic-microcontroller/)

- 基於 i2c 的 IO 擴展
  - 透過 i2c 可以並聯多個 slave 的特性，可將 8bit 設置為同一組IO port
  - IO 擴展晶片透過 i2c 序列接收後，將收到的 8bit 透過IO擴展晶片的並口輸出
  - [以IO擴展晶片(pcf8574)控制LCD](https://www.youtube.com/watch?v=OStlKCcOObg)
 
## 範例
- PIC
  - LED
    - [簡易LED](CHIP_PIC/simple_led/)
    - [七段顯示器](CHIP_PIC/7_segment/)

  - LCD
      - [LCD顯示](CHIP_PIC/lcd_display/)
      - [LCD顯示-i2c](CHIP_PIC/lcd_display_myi2c/)
      - [LCD顯示-自定義lcd庫](CHIP_PIC/lcd_display_mylib/)
    
  - Motor
    - [DC馬達控制](CHIP_PIC/control_dc_motor/)
    - [步進馬達控制](CHIP_PIC/control_step_motor/)

  - Analog
    - [讀取 Analog](CHIP_PIC/read_analog/)
    - [讀取 Analog，以LM35為例](CHIP_PIC/read_analog_LM35/)
    - [讀取 Analog，以LM35為例，直接從 Reg 讀取](CHIP_PIC/read_analog_LM35_byReg/)
    - [讀取 Analog，以LM35為例，直接從 Reg 讀取，改用16F88實現](CHIP_PIC/read_analog_LM35_byReg_16F88/)

  - SPI  
    - [SPI使用範例，以DAC元件為例，使用SPI庫](CHIP_PIC/spi_communicate_DAC_useLibrary/)
    - [SPI使用範例，以DAC元件為例，直接從Reg讀取](CHIP_PIC/spi_communicate_DAC_useReg/)

  - I2C
    - [檢視 i2c-master 發送的指令](i2c-master-only/mplab/)
    - [使用i2c與RTC通訊，顯在LCD顯示結果](i2c-rtc-pcf8563/mplab/)
    - [PIC18F4520+RTC(DS1307)](https://www.youtube.com/watch?v=tu4vGNo6mvc)

  - Others
    - [ESP8266+LCD](CHIP_PIC/esp8266-lcd/)  
    - [寫入/讀取 PIC 內部的 EEPROM](CHIP_PIC/internal_eeprom/)
    - [利用RF進行PIC與PIC之間的通訊](https://circuitdigest.com/microcontroller-projects/pic-to-pic-communication-using-rf-module)
    - [PIC+Relay](https://circuitdigest.com/microcontroller-projects/pic16f877a-relay-control-tutorial)
    - [PIC+IR]([123](https://circuitdigest.com/microcontroller-projects/build-your-own-ir-remote-decoder-using-tsop-and-pic-microcontroller))
    - [PIC+SD-Card](https://circuitdigest.com/microcontroller-projects/save-and-store-data-in-sd-card-using-pic-microcontroller)
    - [PIC+RTC](https://circuitdigest.com/microcontroller-projects/pic16f877a-ds3231-rtc-digital-clock)

- AVR(ATMega)

- STM32(ARM-based)

  
## 推薦網站
- [projects for PIC/Arduino/ESP8266/MikroC/CCS C/MPLAB](https://simple-circuit.com/)
- [Microcontroller projects](https://circuitdigest.com/)
- [STM32/ESP32/PIC @ DEEPBLUE MBEDDED](https://deepbluembedded.com/)
- [PIC projects @ DEEPBLUE MBEDDED](https://deepbluembedded.com/pic-programming-tutorials/)
- [PIC + AVR + ARM + 8051 @ Electronicwings](https://www.electronicwings.com/)
- [All About Circuit](https://www.allaboutcircuits.com/?utm_source=forum)
- [PIC範例集 @ techetrx](https://techetrx.com/category/pic-microcontroller/)，有範例代碼 + proteus
- [PIC範例集 @ ](https://techetrx.com/category/pic-microcontroller/)
- [PIC by asm 範例集 @ y2b-Binder Tronics](https://www.youtube.com/playlist?list=PLtuqBdbsL-DuQB1DQAowWUEhTdI4KCVIZ)
- [PIC + ESP 範例集](https://bugworkshop.blogspot.com/search/label/DIY%20-%20PIC)


