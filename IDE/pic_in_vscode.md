## 使用 vscode 執行 mplab 專案
- step1，確認 `專案目錄 > .vscode > c_cpp_properties.json` 指向正確路徑，
  此路徑用於 vscode 的 C/C++ 套件，指向正確路徑才能使 C/C++ 套件正常工作

- step2，修改 `專案目錄 > build.bat` 指向正確路徑，
  此路徑指向 MPLAB-IDE 內建 make.exe 的位置，透過 make.exe 執行專案中的 makefile

- step3，使用 MPLAB-IDE 開啟專案，並成功編譯一次，
  MPLAB-IDE 會主動更新 makefile，使 makefile 的內容保持正確的設定

- step4，使用 vscode 編輯代碼，透過 build.bat 調用編譯器的執行檔進行編譯

## Ref
- [VSCode + 微控制器 PIC系列的開發環境](https://jacky19680526.blogspot.com/2020/03/vscode-1-pic.html)