## 使用MCC，MplabCodeConfigurator 快速配置和使用內建Library

- 使用場景
  - 透過 GUI 介面可快速配置 PIC晶片
  - 透過 MCC 配置PIC晶片的模組，會產生該模組對應的內建庫

- 限制
  - 不適用所有的PIC晶片，太舊的PIC晶片有可能不支援，例如，PIC16F877A
  - 若使用 MCC 自動產生的內建庫，有可能會產生未使用的函數，增加 hex 的體積

## 範例，以MCC配置 18F26K20 晶片，和 i2c 內建庫的使用
- step1，安裝 MCC plugin
  
  - Tools > Plugins

    <img src="doc/mcc/install-mcc-1.png" width=50% height=auto>

  - Available-Plugs > 輸入 mcc > 勾選 install > 點擊 install

    <img src="doc/mcc/install-mcc-2.png" width=80% height=auto>

  - 啟動 MCC
    - 方法1，Tools > Embeded > MCC
    - 方法2，Windows > MCC

    <img src="doc/mcc/install-mcc-3.png" width=80% height=auto>

- step2，建立 project

- step3，第一次啟動 MCC
  
  選擇 MCC 的版本 > 視情況添加額外的支援 > 點擊 finish

  <img src="doc/mcc/initiate-mcc.png" width=80% height=auto>

- 設置模塊基礎
  - 預設的基礎模塊包含
    - `必要模塊1`，pin-module，用於設置GPIO
    - `必要模塊2`，system-module，用於設置PIC晶片硬體
      - 設置 OSCILLATOR
      - 設置 WDT
    - `必要模塊3`，interrupt-module，用於設置中斷

  - 兩種設置方法
    - 以下兩種方法擇一
    - EasySetup頁面，透過功能進行配置
    - Registers頁面，透過 Reg 進行配置

- step4，配置 PIC 晶片的 clock
  
  <img src="doc/mcc/config-i2c-1.png" width=80% height=auto>

- step5，配置選用模塊，以 i2c 為例

  選擇 mssp 模組
  
  <img src="doc/mcc/config-i2c-2.png" width=60% height=auto>

  透過 GUI 配置 i2c

  <img src="doc/mcc/config-i2c-3.png" width=80% height=auto>

  完成配置後，按下 Generate 鈕，會自動產生配置和內建的 Library

  <img src="doc/mcc/config-i2c-4.png" width=80% height=auto>

  自動產生的內建庫

  <img src="doc/mcc/generated-builtin-library.png" width=80% height=auto>

- step6，透過 pin-manager，以GUI的方式檢視GPIO 的使用狀況

  <img src="doc/mcc/pin-manager.png" width=80% height=auto>
  
## Ref
- [Config with MPLAB Code Configurator](https://www.youtube.com/watch?v=1M1n8oEw9zk)