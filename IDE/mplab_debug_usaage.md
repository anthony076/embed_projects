## 使用 mplab-ide 進行 debug

- 設置 simulator 時的頻率，需與代碼一致

  <img src="doc/debug/set-osc-as-code.png" width=80% height=auto>

- 添加監視變數
  - 方法1，透過 Windows > Debugging > Watch 視窗添加
  - 方法2，直接選取變數 > 右鍵 > New Watch

  <img src="doc/debug/add-variable-watch.png" width=80% height=auto>

- 添加中斷點

  <img src="doc/debug/add-breakpoint.png" width=60% height=auto>

- 啟用 IO-View

  <img src="doc/debug/io-view.png" width=80% height=auto>

## 手動觸發GPIO接腳狀態 (stimulus 的使用)
- step1，開啟 Stimulus 視窗
  > window > Simulator > stimulus

- step2，添加要手動觸發的接腳
  
  <img src="doc/debug/set-stimulus-1.png" width=70% height=auto>
  
- step3，設置中斷點和監看變數

  <img src="doc/debug/set-stimulus-2.png" width=80% height=auto>

- step4，啟用 simulator > 手動點擊 Fire > Continue(F5)

  <img src="doc/debug/set-stimulus-3.png" width=80% height=auto>

## 在 simulator 中使用 printf
- 概述
  
  在 simulator 中，當調用 stdlib 的 printf() 時，會調用 putc() 並將字串寫入 TX，
  此時，simulator 會捕獲 uart 的傳輸，並顯示於 uart output 的視窗中

- [完整範例](use_print_in_simulator/)

- 使用流程
  - 測試環境: 
  - step1，配置專案屬性
    - 1-1，啟用`Uart IO`
      production > set project configuration > customize
      
      <img src="doc/debug/set-printf-1.png" width=80% height=auto>

      simulator > oscillation Options > UART1 IO Options
      
      <img src="doc/debug/set-printf-2.png" width=80% height=auto>

      勾選 Enable Uart1 IO > 選擇 Windows
      
      <img src="doc/debug/set-printf-3.png" width=80% height=auto>
    
    - 1-2，設置與代碼一致的 osc
    
      <img src="doc/debug/set-osc-as-code.png" width=80% height=auto>
    
  - step2，在代碼中的設置

    - 2-1，引入 stdio 庫，才能調用 printf()
      ```c
      #include <stdio.h>
      ```

    - 2-2，手動實現 putc()
      ```c
      void putch(char data) {
        
        // wait until the transmitter is ready
        while( !PIR1bits.TXIF){
          continue; 
        }          
        
        // send one character
        TXREG = data;
      }
      ```
    
    - 2-3，配置 uart
      ```c
      void uart_init(){
          // 設置 baudRate
          SPBRG = 25;
          
          // 設置 Tx
          //TXSTA = EIGHT_BIT_TRANSMISSION | TX_ON | ASYNC_MODE | HIGH_SPEED_ON; 
          TXSTA = 0b00100100;
        
          // 設置 Rx
          //RCSTA = SERIAL_PORT_ON | EIGHT_BIT_RECEPTION ;
          RCSTA = 0b10000000;
      }
      ```

  - step3，初始化 uart，並調用 printf 的代碼
    ```c
    int main(void) {
        uart_init();
        
        printf("Hello World!\n");
    }
    ```

  - step4，添加 breakpoint 後，啟用 simulator，

    注意，當調用 printf() 才會調用 putc()，並將輸出傳遞給 tx
    tx 有輸出時，simulator 才會顯示 uart-window，並將 printf()的字串顯示

    <img src="doc/debug/get-printf-on-uart-output.png" width=80% height=auto>

## Ref

- [Debugging and Using the Simulator](https://www.youtube.com/watch?v=O4IpwgWhqLY)

- [stimulus 的使用](https://youtu.be/O4IpwgWhqLY?t=326)

- printf in simulator
  - [printf 的使用 @ 官方文檔](https://microchipdeveloper.com/xc8:console-printing)

  - [printf 的使用 @ 官方文檔](https://microchipsupport.force.com/s/article/How-to-Configure-printf)

  - [printf 使用範例](https://www.teachmemicro.com/serial-usart-pic16f877a/)

  - [use printf in simulator](https://www.youtube.com/watch?v=ReAHPnx5ePw)