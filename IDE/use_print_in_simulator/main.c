// config
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define _XTAL_FREQ 4000000

// TXSTA-register

#define NINE_BIT_TRANSMISSION 1 << 6
#define EIGHT_BIT_TRANSMISSION 0 << 6

#define TX_ON 1 << 5
#define TX_OFF 0 << 5

#define SYNC_MODE 1 << 4
#define ASYNC_MODE 0 << 4

#define HIGH_SPEED_ON 1 << 2
#define HIGH_SPEED_OFF 0 << 2

// RXSTA-register

#define SERIAL_PORT_ON 1 << 7
#define SERIAL_PORT_OFF 0 << 7

#define NINE_BIT_RECEPTION 1 << 6
#define EIGHT_BIT_RECEPTION 0 << 6

#define SINGLE_RECEIVE_ON 1 << 5
#define CONTINUOUS_RECEIVE_ON 1 << 4

#include <xc.h>
#include <stdio.h>

// bug: from https://www.teachmemicro.com/serial-usart-pic16f877a/
// 
//void putch(char data){
//    while(TRMT){
//        TXREG = data;
//    }
//}

// from official website
void putch(char data) {
    while( !PIR1bits.TXIF)          // wait until the transmitter is ready
        continue;
    TXREG = data;                     // send one character
}

void uart_init(){
    // 設置 baudRate
    SPBRG = 25;
    
    // 設置 Tx
    //TXSTA = EIGHT_BIT_TRANSMISSION | TX_ON | ASYNC_MODE | HIGH_SPEED_ON; 
    TXSTA = 0b00100100;
   
    // 設置 Rx
    //RCSTA = SERIAL_PORT_ON | EIGHT_BIT_RECEPTION ;
    RCSTA = 0b10000000;
}

int main(void) {
    uart_init();
    
    printf("Hello World!\n");
}
