void main()
{
     // set PORTB direction, 0=output, 1=input
     TRISB = 0x00;

     // set PORTB initial value
     PORTB = 0x00;

     while (1)
     {
          PORTB.B0 = 1;
          delay_ms(300);

          PORTB.B0 = 0;
          delay_ms(300);
     }
}