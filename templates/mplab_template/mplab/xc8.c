
#include <xc.h>
#include "xc8.h"
#include <stdlib.h>
#include <stdio.h>

int main(void) {
    TRISBbits.RB0 = 0;
    OSCCON = 0x76;
    
    while(1)
    {
        LATBbits.LATB0 = ~LATBbits.LATB0;
        
        for (int countDelay=0; countDelay <20; countDelay++)
        {
            __delay_ms(50);
        }
    }
    return EXIT_SUCCESS;
}
