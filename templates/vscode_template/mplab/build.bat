@echo off

set Make="C:\Program Files\MPLABX\gnuBins\GnuWin32\bin\make.exe"

%Make% build

copy /Y dist\default\production\mplab.production.hex .\pic-esp8266-lcd.hex

rmdir /S /Q build dist