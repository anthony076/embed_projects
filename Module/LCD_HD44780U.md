## 概述
- Hitachi HD44780 的 LCD 控制晶片

- 支援 最多可顯示 80 個字符，常見 8*1、16*2、20*2、20*4 四種格式的 LCD 模組，
  例如，LCD-1602、LCD-1602A、LCD-2004、LCM-1602

- 常用接腳
  - VSS，電源接地
  - VCC，電源，
  - VEE，對比度調整，Analog-Input
  - RS，選擇 HD44780 的暫存器，
    - RS=0，選擇命令暫存器(IR)，
    - RS=1，選擇數據暫存器(DR)，
  
  - RW，讀寫控制接腳
    - RW=0，LCD 執行寫入操作
    - RW=1，LCD 執行讀取操作
  
  - E，LCD 數據傳輸關閉/致能
    - E=0，LCD 數據傳輸關閉，將 PORT 的數據或指令傳遞給 LCD
    - E=1，LCD 數據傳輸致能，將 PORT 的數據或指令傳遞給 LCD

  - DB0 - DB7，數據位，注意，DB0-DB3，不能用於 4bit-mode

- 使用流程
  - `step1`，設置操作模式 (8bit-mode/4bit-mode)

  - `step2`，配置 LCD
    - 根據最高位1的位置，決定配置哪一個指令
    - 根據 LCD 實體狀況進行配置

  - `step3`，寫入 LCD
    3-1，設置讀寫和操作對象
      - 寫入LCD指令: RS=0(Instruction)，RW=0(write)，
      - 寫入LCD數據: RS=1(data)，RW=0(write)，
    3-2，將數據或指令寫入 PORT
    3-3，EN=1，開啟傳輸
    3-4，delay，等待LCD處理完成
    3-5，EN=0，關閉傳輸

## 功能塊
- 功能方塊圖

  <img src="HD44780U/block-diagram.png" width=80% height=auto>

- IR (Instruction-Register)，負責接收指令

  參考，[指令的使用](#指令的使用)

- DR (Data-Register)，
  - `暫時保存`要寫入 DDRAM(data-RAM) 或 CGRAM(character generator-RAM) 的字數據
  
    透過 DB0-DB7 傳入的數據，會先寫入 DR，再根據指令，選擇要寫入 DDRAM 或寫入 CGRAM，例如，執行 SetCGRAMAddr 的指令後再寫入DR的數據，會被寫入 CGRAM 中

- 在 EPROM 中的 DDRAM / CGROM / CGRAM
  - 注意，CGRAM / CGROM `保存的是 8bit-CharacterCode`，不是字符的點陣資料
    
    CGRAM或CGROM儲存的是 `8bit-CharacterCode`，每個 8bit-CharacterCode 對應一筆 40bit 或 50bit 的點陣資料，是`儲存在EPROM的數據區中`，CharacterCode 和點陣資料的`對應關係`，是由 HD44780 自動處理
    
    EPROM 可以被規劃為不同的分區
    - 用來保存 8bit-CharacterCode 的 CGRAM區(可擦除) 和 CGROM區(不可擦除)
    - 用來保存 `點陣資料的數據區`，依樣區分 CGRAM區 和 CGROM區
    - 用來保存顯示資料的 DDRAM 區
  
  - `DDRAM`， Display-Data-RAM
    - 用於`儲存顯示數據`或 `要儲存到 CGRAM 的一般數據`
      
    - 顯示數據有80個儲存空間(0d80=0x4f)，每個儲存空間為 1Byte長，
      將要顯示於LCD的字符，寫入此 80 個儲存空間中，
      
    - 實際顯示的字符(可視窗口)，依照硬體規格決定，
      - 例如，16*1 的 LCD，在 80 個儲存空間中，只顯示其中的 16 個(可視窗口的大小)， 
      - 例如，8*1 的 LCD，在 80 個儲存空間中，只顯示其中的 8 個(可視窗口的大小)，

      - 未顯示的儲存位置，透過 shift 可移動可視窗口的位置，
      ，使游標或畫面看起來再移動的樣子
      
    - `DDRAM記憶體位址` 和 `LCD顯示位置` 的對應關係
      <img src="HD44780U/relation_between_ddram_and_display.png" width=80% height=auto>

    - 雙行顯示時，使用不同的 DDRAM 記憶體位址
      <img src="HD44780U/two-lines-display-use-diff-addr.png" width=60% height=auto>

  - `CGROM`， Character-Generator-ROM
    - 用於保存240個`內建字符`的 CharacterCode，每個 CharacterCode 占用 8-bits
    - 內建字符對應的點陣資料，在 EPROM 中共占用
      - 208 個 5*8 大小的字符 (208*40=8320 bits)
      - 32 個 5*10 大小的字符 (32*50=1600 bits)
      - 總共 208+32=240個字符，總共占用 8320+1600 = 9920-Bits = 1240-Bytes

  - `CGRAM`，Character-Generator-RAM
    - 用於保存8個`自訂義字符`的 CharacterCode，每個 CharacterCode 占用 8-bits
    - 實際上，CGRAM 在 EPROM 中占用 16 個儲存空間，但只有8個儲存空間可供使用者使用

- BusyFlag，確保前一個指令已執行完畢

  透過 BusyFlag 確認前一次指令執行完畢後，再進行傳送下一個指令
  ```mermaid
  flowchart LR
  b1[發送指令] --> b2[檢查] --> b3[發送指令]
  ```

- Address-Count(AC)，用於指示 CGRAM 或 DDRAM 當前的位址，7bit 長

## 指令的使用
- 透過 RS/RW/DB0-DB7 接腳，讀寫指令/數據
  - 透過 `RS(Register-Select)接腳`，選擇IR暫存器或DR暫存器
    RS=0=IR，RS=1=DR，

    透過 `RW(Read-Write)接腳`，選擇要進行 Read 或 Write 的操作
    RW=0=Write，RW=1=Read，

    透過 `DB4-DB7(4bit)` 或 `DB0-DB7(8bit)`，傳輸指令參數(for IR)或數據內容(for DR)

    | RS  | RW  | Operation                                |
    | --- | --- | ---------------------------------------- |
    | 0   | 0   | 寫入指令                                 |
    | 0   | 1   | 讀取 BusyFlag(@DB7) 和 AC 的值(@DB0-DB6) |
    | 1   | 0   | 寫入數據到 DR                            |
    | 1   | 1   | 從 DR 讀取數據                           |
  
- 指令格式
  ```
  // 設置指令時，DB7 - DB0 用於設置指令需要的參數
  RS  RW  DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  ```

- `清除螢幕指令`，ClearDisplay，DB0=1
  - 將空白的 CharacterCode=0x20 寫入 DDRAM
  - 將 DDRAM 的位址設為 0x0，AC = 0，
  - 關閉 shift 和 Display
  - 將 I/D 設置為1
    
  ```
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    0    0    0    0    0    1
  ```

- `返回游標指令`，ReturnHome，DB1=1
  ```
  // 將 DDRAM 的 AC 設置為 0x0，指向 DDRAM 的第一個位置
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    0    0    0    0    1    x
  ```

- `字元進入方式指令`，EntryMode，DB2=1，有2個參數位置
  - 設置完成後，新字符的讀取/寫入到CGRAM/DDRAM後，會使游標或畫面移動
  - I/D = 新字符寫入後的移動方向，Increment = AC + 1 = 向右移動，
    讀寫CGRAM/DDRAM都會使游標依設定方向移動

  - S = Screen = 新字元進入後後，是否移動螢幕
    - S = 0，關閉螢幕移動，只移動游標
    - S = 1，開啟螢幕移動，不移動游標
      - 讀取 DDRAM 時，畫面不移動
      - 讀寫 CGRAM 時，畫面不移動
  
  ```
  // I/D = 0 = Increments，游標向右移動，
  // S = 0 = 關閉螢幕移動
  // S = 1 = 開啟螢幕移動，
  //    ID = 0，螢幕向右移動，游標不動
  //    ID = 1，螢幕向左移動，游標不動

  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    0    0    0    1    I/D    S
  ```

- `開關螢幕指令`，DisplayOnOff，DB3=1，有3個參數位置
  - D = Display On/OFF
  - C = Cursor On/OFF
  - B = Blanking On/OFF
  ```
  // D = 0，關閉 Display
  // C = 0，關閉 Cursor
  // B = 0，關閉 Blinking
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    0    0    1    D    C    B
  ```

- `偏移指令`，Cursor AND display shift，DB4=1，有4個參數位置，實際只使用2個
  - 可選擇`只移動 Cursor` 或是` Cursor+畫面一起移動`
    
    利用此方法移動畫面，`不需要讀寫 display-data`，

    與 EntryMode 指令不同，此指令在畫面移動後，LCD 顯示位置和對應的DDRAM記憶體位址不會有變化，
    此指令移動游標或畫面，都不會改變 DDRAM 的內容

  - 用於雙行LCD時，若第一行超過顯示位置，會自動移到第二行

  - S/C= Screen(1) / Cursor(0)，選擇游標或游標+畫面進行移動
  - R/L= Right(1) / Left(0)

    | S/C | R/L | Function                     |
    | --- | --- | ---------------------------- |
    | 0   | 0   | 自動將游標位置，向左移動1位  |
    | 0   | 1   | 自動將游標位置，向右移動1位  |
    | 1   | 0   | 自動將游標+畫面，向左移動1位 |
    | 1   | 1   | 自動將游標+畫面，向右移動1位 |
  
  ```
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    0    1    S/C  R/L    x    x
  ```

- `功能設置指令`，FunctionSet，DB5=1，有5個參數位置，，實際只使用3個
  - DL = DataLength
  - N = 0 = 單行顯示，N = 1 = 雙行顯示
  - F = 0 = 5*8點陣，F = 1 = 5*10點陣，注意，此選項只在單行顯示時有效
  
  ```
  // DL=0=4bit-mode (使用 DB4-DB7)，DL=1=8bit-mode (使用 DB0-DB7)
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    0    1    DL   N    F    x    x
  ```

- `設置CGRAM位址指令`，SetCGRAMAddr，DB6=1，有6個參數位置
  - 設置 CGRAM 的存取位置，設置後，該位置的內容透過 DDRAM 寫入
  - 詳見，[自定義字符](#自訂義字符-定義-charactercode-和-characterpattern) 
  ```
  // ACG = Address of CGRAM
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 0    1    ACG  ACG  ACG  ACG  ACG  ACG
  ```

- `設置DDRAM位址指令`，SetDDRAMAddr，DB7=1，有7個參數位置
  ```
  // ADD = Address of DDRAM
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 1    ADD  ADD  ADD  ADD  ADD  ADD  ADD
  ```

- `讀取Busyflag/AC指令`，ReadGBusyflagAC，
  - BF=1，指令執行中
  - BF=0，指令執行完畢，可接受新的指令
  ```
  // AC = content of AC
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    1  | BF   AC   AC   AC   AC   AC   AC   AC
  ```

- 注意，寫入 IR/DR 的延遲時間
  - 若`將 RW 接腳接地`，會使得 MCU 只能對LCD寫入命令，但`無法讀取LCD`目前執行的狀態，
    此時，可透過手動添加延遲的方式，讓傳輸順利完成，詳見 Table6 指令執行時間 @ datasheet
  
  - 寫入`DR`時，開啟傳輸(EN=1)和開啟傳輸(EN=0)之間，至少需要等待 `40us` 以上，避免第一個字元無法顯示

  - 寫入`IR`時，開啟傳輸(EN=1)和開啟傳輸(EN=0)之間，至少需要等待 `800us` 以上，確保命令執行完畢

## 設置游標位置(將游標移動到指定位置)
- 需要使用設置DDRAM位址的指令(SetDDRAMAddr)
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 1    ADD  ADD  ADD  ADD  ADD  ADD  ADD

- LCD螢幕第1行，對應的DDRAM位址為
  0     1     2     3     4     5     6     7
  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07

  要移動游標到LCD螢幕第1行第3個位置(0x03) 為例
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 1    0    0    0    0    0    1    1       = 0x83

- LCD螢幕第2行，對應的DDRAM位址為
  0     1     2     3     4     5     6     7
  0x40  0x41  0x42  0x43  0x44  0x45  0x46  0x47
    
  要移動游標到LCD螢幕第2行第3個位置(0x43) 為例
  RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  0    0  | 1    1    0    0    0    0    1    1       = 0xC3

## 偏移字符 (Shift)
- 偏移分為兩種，
  - Cursor-Shift，游標偏移(畫面不動)，寫入新字符後，AC = AC +/- 1，使得游標左移或右移
  - Display-Shift，畫面偏移(游標不動)，寫入新字符後，游標不動，`LCD 顯示位置` 和 `對應的DDRAM記憶體位址`，全都進行右移或左移一位
    ```
    移動前，LCD對應的內存位址
        顯示位置    01    02    03    04    05    06    07    08
        內存位址  0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07

    畫面左移1位，LCD對應的內存位址
        顯示位置    01    02    03    04    05    06    07    08
        內存位址  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08

    畫面右移1位，LCD對應的內存位址
        顯示位置    01    02    03    04    05    06    07    08
        內存位址  0x4f  0x00  0x01  0x02  0x03  0x04  0x05  0x06
    ```

- 偏移字符有兩種方式，
  - 方法1，透過 EntryMode 指令的 `I/D參數` 和 `S參數`，會修改 AC 或 display-data，並造成游標或畫面的移動
  - 方法2，透過 Cursor/display Shift 指令的 `S/C參數` 和 `R/L參數`，不會修改 DDRAM-Address

## LCD 初始化 
- HD44780U 可以操作在兩種模式，4-bit/8bit Mode
  - 4bit-mode
      - 使用 DB4-DB7 的接腳， DB0-DB3 接地  
      - 1Byte 的數據需要`分兩次傳送`，先傳送 high-4-bits，再傳送 low-4-bits，
        傳送兩次後，透過 BusyFlag 和 AC 確認 HD44780 已執行完畢
      - 讀取時，也是先讀取 high-4-bits，再讀取 low-4-bits
    
- HD44780U的上電初始化
  - 上電後，HD44780U 會自動執行初始化，並執行以下的指令

  - step1，清除 Display

  - step2，功能初始化設置為 
    - DL=1，8bit傳輸介面
    - N=0，單行顯示
    - F=0，5*8點陣

  - step3，Display 開關控制
    - D=0，Display 關閉
    - C=0，Cursor 關閉
    - B=0，Blinking 關閉

  - step4，設置模式
    - I/D=0，記憶體位址每次增加1
    - S=0，no shift
  
  - 注意，以上是HD44780U預設的初始化值，`使用者根據需求，需要重新進行設置`

- (選用)進入8-bit-mode 的初始化程序
  - 要使用 8bit-mode，需要特定的指令序列
    詳見，[Figure23@datasheet-p45]()

  - step1，上電後等待 `55ms` (VCC到達2.7V後等待`40ms` + VCC到達4.5V後等待`15ms`)

  - step2，發送`第1個0x30`，將 DB5=DB4=1，並`等待須超過 4.1ms`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - step3，發送`第2個0x30`，將 DB5=DB4=1，並`等待須超過 100us`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - step4，發送`第3個0x30`，將 DB5=DB4=1，`等待沒有特殊限制`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - 已進入 8bit-mode

- (選用)進入4-bit-mode 的初始化程序
  - 要使用 4bit-mode，`必須先經過特殊啟動序列後`，才會進入 4-bit-mode，否則預設使用 8-bit-mode，
    詳見，[Figure24@datasheet-p46]()

  - step1，上電後等待 `55ms` (VCC到達2.7V後等待`40ms` + VCC到達4.5V後等待`15ms`)

  - step2，發送`第1個0x30`，將 DB5=DB4=1，並`等待須超過 4.1ms`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - step3，發送`第2個0x30`，將 DB5=DB4=1，並`等待須超過 100us`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - step4，發送`第3個0x30`，將 DB5=DB4=1，`等待沒有特殊限制`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    1    *    *    *    *    = 0x30
    ```  

  - step5，發送`0x20`，將 DB5=1，DB4=0，`等待沒有特殊限制`
    ```
    RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    0    0  | 0    0    1    0    *    *    *    *    = 0x20
    ```

  - 已進入 4bit-mode，之後使用者客製化的8bit指令，都需要分兩次傳送

- 8bit-mode 初始化精簡版
  - step1，delay 55ms
  - step2，DB5=DB4=1 + delay 4.1ms
  - step3，DB5=DB4=1 + delay 100us
  - step4，DB5=DB4=1
  - (已進入8bit-mode)
  - step5，Function-Set 指令
  - step6，Entry-Mode 指令
  - stepN，其他客製化指令

- 4bit-mode 初始化精簡版
  - step1，delay 55ms
  - step2，DB5=DB4=1 + delay 4.1ms
  - step3，DB5=DB4=1 + delay 100us
  - step4，DB5=DB4=1
  - step5，DB5=1，DB4=0
  - (已進入4bit-mode，以下指令需發送兩次)
  - step6，Function-Set 指令
  - step7，Entry-Mode 指令
  - stepN，其他客製化指令

## 存取 CGROM 中的內建字符 (顯示字符 b)
- 以讀取 CGROM 中的 b 為例
  
  注意，下圖的8bit不是地址，而是 CharacterCode 和字符的對照表，
  和 [ASCII Table](https://upload.wikimedia.org/wikipedia/commons/d/dd/ASCII-Table.svg) 是一致的

  字符 b 的 CharacterCode = 0b0110_0010 = 0x62
  <img src="HD44780U/code-and-pattern-map.png" width=80% height=auto>

- 字符 b 點陣圖保存在 EPROM 中的實際位址
  - EPROM 有 12 根位址線，A11-A0
  - A11-A4，高8位，指向 CharacterCode
  - A3-A0，低4位，指向點陣圖 nth-row 的位址，共有16位，每個位址對應  nth-row 的點鎮數據
    ```
    (CharacterCode)                         (index)                 (CharacterPattern-row-data)
    A11  A10  A09  A08  A07  A06  A05  A04 | A03  A02  A01  A00   | DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0 
    0    1    1    0    0    0    1    0   | 0    0    0    0     | 0   0   0   1   0   0   0   0
    ```
    - (Addr = 8bit-CharacterCode + 3bit-index)
    - Address=0b01100010_000=0x620，保存字符b，第1行的點陣值=0b000_10000=0x10
    - Address=0b01100010_001=0x621，保存字符b，第2行的點陣值=0b000_10000=0x10
    - Address=0b01100010_010=0x622，保存字符b，第3行的點陣值=0b000_10110=0x16
    - Address=0b01100010_011=0x623，保存字符b，第4行的點陣值=0b000_11001=0x19
    - Address=0b01100010_100=0x624，保存字符b，第5行的點陣值=0b000_10001=0x11
    - Address=0b01100010_101=0x625，保存字符b，第6行的點陣值=0b000_10001=0x11
    - Address=0b01100010_110=0x626，保存字符b，第7行的點陣值=0b000_11110=0x1E
    - Address=0b01100010_111=0x627，保存字符b，第8行的點陣值=0b000_10000=0x10

- 要顯示內建字符 b，不需要手動指定 EPROM 的記憶體位址，`只需要將 CharacterCode 寫入 DR 中`，
  HD44780 會根據 DR 中寫入的 CharacterCode，自動讀取點陣資料，並顯示於LCD螢幕上

  將 CharacterCode 寫入 DR，例如，將 0x62 寫入 DR，發送的指令如下，
  RS  RW  | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
  1   0   | 0    1    1    0    0    0    1    0

## 自定義字符 (定義 CharacterCode 和 CharacterPattern)
- 儲存空間  
  - CGROM 中，共保存`240個`內建字符的 CharacterCode
  - CGRAM 中，共保存`8個`自定義字符的 CharacterCode
  - CharacterCode 和點陣資料實際的保存位址，是由 HD44780 自動處理

- LCD 上每個字元，實際上是由 (width=5)*(height=8)，
  或 (width=5)*(height=10) 的點陣所組成
  - 對 5*8點陣，第8-Row是游標顯示位置
  - 對 5*10點陣，第10-Row是游標顯示位置

- 要自訂義字符，需要將 5*8點陣圖(CharacterPattern)拆分成8筆，每筆長度為5bits的數據，
  - step1，透過 `SetCGRAMAddr指令`，指定8筆5bit長的點陣圖資料，要保存在哪一個 CharacterCode 的位置中
    > 八個可用的 CharacterCode 為 0x00、0x01、...、0x07
  
  - step2，透過RS/RW的接腳和8bit的數據接腳(實際上只使用5bit，未使用的位補0)，連續寫入 DR 中，
    HD44780 會`將寫入的數據，保存在指定的CGRAM位址中`
  
- 範例，自定義字符 (自定義點陣圖) 範例，
  - step1，透過 `SetCGRAMAddr指令`，將自定義字符的`CharacterCode`保存為 0x01
    ```
    // 透過 SetCGRAMAddr指令指定 CharacterCode，
    // 注意，SetCGRAMAddr指令只使用 DB0-DB5 來指定位址
    //   RS   RW | DB7  DB6  DB5  DB4  DB3  DB2  DB1  DB0
    //   0    0  | 0    1    ACG  ACG  ACG  ACG  ACG  ACG

    // 因為CGRAM只有8個位址可供使用者使用，因此指定CharacterCode，只有 DB2-DB0 是有效的 
    (寫入IR) (SetCGRAMAddr)   (未使用)        (指定自定義字符的CharacterCode)
    RS  RW  | DB7  DB6      | DB5  DB4  DB3 | DB2  DB1  DB0
    0   0   | 0    1        | 0    0    0   | 0    0    1
    ```

  - step2，將自定義的 5*8 點陣圖(CharacterPattern)，拆分成 8 筆 5bit (DB4-DB0)的數據`寫入DR`，
    - 和讀取CGROM不同，自定義字符只使用 3bit 定義，CharacterCode 
      因此，自定義字符`實際儲存在 EPROM 的位址`，只使用 6bit (3bit-CharacterCode + 3bit-index)

      (未使用)                       (CharacterCode)     (index)       (CharacterPattern-row-data)
      A11  A10  A09  A08  A07  A06 | A05  A04  A03 | A02  A01  A00   | DB7 DB6 DB5 DB4 DB3 DB2 DB1 DB0 
      0    0    0    0    0    0   | 0    0    1   | 0    0    0     | 0   0   0   1   0   0   0   0

    - 每寫入一筆，AC 會自動+1，
    - RS=1=選擇DR，RW=0=寫入，代表寫入DR
    - 以下為自定義的字符的點陣圖
    
      ```
      (寫入DR)    (未使用)           (自定義點陣圖)
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   0    1    1    1    0
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    1    0    1    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      RS  RW  |   DB7  DB6  DB5  |   DB4  DB3  DB2  DB1  DB0
      1   0   |   0    0    0    |   1    0    0    0    1
      ```

  - 上述自定義的點陣圖資料，在 EPROM 中實際的保存位址為
    - (Addr = 5bit-未使用 + 3bit-CharacterCode + 3bit-index)
    - Address=0b00000_001_000=0x008，保存自定義字符，第1行的點陣值=0b000_01110=0x0E
    - Address=0b00000_001_001=0x009，保存自定義字符，第2行的點陣值=0b000_11011=0x1B
    - Address=0b00000_001_010=0x00A，保存自定義字符，第3行的點陣值=0b000_10001=0x11
    - Address=0b00000_001_011=0x00B，保存自定義字符，第4行的點陣值=0b000_10001=0x11
    - Address=0b00000_001_100=0x00C，保存自定義字符，第5行的點陣值=0b000_10001=0x11
    - Address=0b00000_001_101=0x00D，保存自定義字符，第6行的點陣值=0b000_10001=0x11
    - Address=0b00000_001_110=0x00E，保存自定義字符，第7行的點陣值=0b000_10001=0x11
    - Address=0b00000_001_111=0x00F，保存自定義字符，第8行的點陣值=0b000_10001=0x11

  - 讀取自訂義字符
    透過 SetCGRAMAddr指令，指定自訂義字符的 CharacterCode
    ```
    RS  RW  | DB7  DB6      | DB5  DB4  DB3 | DB2  DB1  DB0
    0   0   | 0    1        | 0    0    0   | 0    0    1
    ```

## 讀寫 CGRAM / DDRAM
- 寫入數據到 CGRAM 或 DDRAM
  數據寫入後，會根據 EntryMode 的設置，將 AC +/- 1

- 在執行讀取指令前，必須先設置要讀取的位址，

- 連續讀取DDRAM的數據時，若設置游標移動，則從DDRAM讀取完第一筆後，讀取位址就會因游標移動而被設置，
  此時，讀取第二筆數據前，就不需要再設置讀取的位址，游標移動同樣會設置讀取地址

- 在執行 EntryMode 指令`移動 Display`後，不會自動設置Address，因此，在讀取DDRAM的數據前，需要再設置讀取的位址

- 寫入指令到 CGRAM 或 DDRAM，AC 的值會自動 +/-1，此時，AC 對應位址的記憶體內容，就無法再透過 read 指令讀出，

  此時，正確讀取數據的方式，是 SetCGRAMAddr / SetDDRAMAddr 重新設置讀取位址，
  或是透過 EntryMode 移動游標指令重新設置讀取位址 (只對 DDRAM 有效)，
  並在 read 指令送出後，執行 read 指令

## 範例代碼
- 初始化範例
  ```c
  void lcd_init(void)
  {
      // ==== 設置 4bit/8bit mode ====
      //  進入 4bit-mode: 0x03 -> 0x03 -> 0x03 -> 0x02
      //  進入 4bit-mode: 0x03 -> 0x03 -> 0x03
      __delay_ms(15);
      
      lcd_cmd(0x03);
      __delay_ms(5);
      
      lcd_cmd(0x03);
      __delay_ms(5);
      
      lcd_cmd(0x03);
      __delay_ms(5); 
      
      lcd_cmd(0x02);
      __delay_ms(5);
      
      // ==== 配置 LCD ====

      // 0x28 = 0b0010_1000 = 0b001(DL)_(N)(F)**
      //    DB5=1=代表Function-Set 指令
      //    DB4=0=4bit-mode
      //    DB3=N=1=雙行顯示
      //    DB2=F=0=5*8
      lcd_cmd(0x28);       // 4 bit interface length. 2 rows enabled.
      __delay_ms(5);

      // 0x10 = 0b0001_0000 = 0b0001_(S/C)(R/L)**
      //    DB4=1=代表 Cursor AND display shift 指令
      //    DB3=SC=1=移動滑鼠，畫面不移動
      //    DB2=RL=1=向左移動
      lcd_cmd(0x10);
      __delay_ms(5);
      
      // 0x0C = 0b0000_1100 = 0b0000_1(D)(C)(B)
      //    DB3=1=代表DisplayOnOff指令
      //    DB2=D=1=DisplayOn     開啟顯示
      //    DB1=C=1=CursorOn      開啟游標
      //    DB0=B=0=BlikingOff    關閉閃爍
      lcd_cmd(0x0c);       // Enable display. Cursor off.
      __delay_ms(5);
      
      // 0x06 = 0b0000_0110 = 0b0000_01(I/D)(S)
      //    DB2=1=代表EntryMode指令
      //    DB1=I/D=1=Increments，向右移動
      //    DB0=S=0=ScreenOff=只移動游標，不移動螢幕
      lcd_cmd(0x06);      // Increment cursor position after each byte 
      __delay_ms(5);
      
      // 清除螢幕
      lcd_cmd(0x01);      // clear display
      __delay_ms(5);
  }
  ```

- 寫入命令範例
  ```c
  void enable(void)
  {
      LCD_E = 1;
      __delay_ms(5);
      LCD_E = 0;
  }

  void lcd_cmd(unsigned char c)
  {
      LCD_REG = IR;
      LCD_RW = WRITE;
      
      // 設置並傳送高四位
      //   - PORTB & 0x0f，將 PORTB 的 RB7-RB4 設置為0，使 RB7-RB4 的輸出可改變 
      //   - 0xf0 & c，取 c 的高4位，
      //   - OR 運算: 用於設置 PORTB 的 RB7-RB4，因為 RB7-RB4 被設置為0，OR 運算可改變 RB7-RB4 的值
      PORTB = (PORTB & 0x0f) | (0xf0 & c);
      enable();
      
      // 設置並傳送低四位
      //   - PORTB & 0x0f，將 PORTB 的 RB7-RB4 設置為0，使 RB7-RB4 的輸出可改變 
      //   - (0x0f & c) << 4，取 c 的低4位後，將低4位移動到高4位
      //   - OR 運算: 用於設置 PORTB 的 RB7-RB4，因為 RB7-RB4 被設置為0，OR 運算可改變 RB7-RB4 的值
      PORTB = (PORTB & 0x0f) | ((0x0f & c) << 4) ;
      enable();
  }
  ```

- 寫入數據範例
  ```c
  void lcd_data(unsigned char d)
  {
      LCD_REG = DR;
      LCD_RW = WRITE;
      
      PORTB = (PORTB & 0x0f) | (0xf0 & d);
      enable();
      
      PORTB = (PORTB & 0x0f) | ((0x0f & d) << 4) ;
      enable();
  }
  ```

## 範例
- LCD Library，
  - [範例1](../CHIP_PIC/LIBRARY_lcd_HD44780U/lcd-1.c)
  - [範例2](../CHIP_PIC/LIBRARY_lcd_HD44780U/lcd-2.c)，推薦
  - [範例3](../CHIP_PIC/LIBRARY_lcd_HD44780U/lcd-3.c)
  - [範例4](../CHIP_PIC/LIBRARY_lcd_HD44780U/lcd-4.c)，推薦

- Full Project，
  - [使用i2c與HD44780通訊](../CHIP_PIC/lcd_display_myi2c/)
  - [esp8266+lcd](../CHIP_PIC/esp8266-lcd/mplab/)
  - [使用i2c與RTC通訊，顯在LCD顯示結果](../CHIP_PIC/i2c-rtc-pcf8563/mplab/)

- 第三方 LCD Library
  - [pic-xc8-lcd-library](https://github.com/magkopian/pic-xc8-lcd-library)
  - [hd44780 based lcd library for PIC16](https://extremeelectronics.co.in/pic16f877a-tutorials/setup-and-use-of-lcd-library-for-pic/)
  - [LCD Library for 8-bit PIC](https://github.com/Trionium/PIC-Libraries/tree/master/LCD)

## Ref
- [Hitachi HD44780 LCD controller](https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller)
- [hd44780](https://github.com/duinoWitchery/hd44780/tree/master/examples)
- [Arduino 控制 HD44780 點陣文字LCD熒幕模組](https://hkgoldenmra.blogspot.com/2019/10/hd44780lcd.html)
- [LCD 16×2 (LM016L)](https://embeddedcenter.wordpress.com/ece-study-centre/display-module/lcd-16x2-lm016l/)
- [lcd-interfacing-pic16f877a-microcontroller/](https://microcontrollerslab.com/lcd-interfacing-pic16f877a-microcontroller/)