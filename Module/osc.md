## 震盪器基礎

- 震盪器的種類
  - 石英震盪器(quartz-crystal-resonator 或 Xtal)，穩定，精度高
  - 陶瓷諧振器(Ceramic_resonator)，便宜，精度低

- 石英振盪器基礎
  - 原理
    
    運用石英晶體上的電極對一顆被適當切割並安置的石英晶體施以電場時，晶體會產生變形。這就是逆壓電效應。當外加電場移除時，石英晶體會恢復原狀並發出電場，因而在電極上產生電壓。這樣的特性造成石英晶體在電路中的行為，類似於某種電感器、電容器、與電阻器所組合成的RLC電路。組合中的電感電容諧振頻率則反映了石英晶體的實體共振頻率。

  - 石英晶體振盪器有三種切割方式，切割的形式影響輸出的波形
    - x-cut，
      - 垂直於電氣軸(x-axis) 進行切割
      - 常用於產生縱波(longitudinal-wave)，能量傳遞方向與粒子移動方向平行
  
    - y-cut，
      - 垂直於機械軸(y-axis) 進行切割
      - 常用於產生橫波(transverse-wave)，能量傳遞方向與粒子移動方向垂直
  
    - AT-cut
      - 與光線軸(z-axis)成35度角進行切割
      - 常見，受溫度影響造成的誤差更小

    - BT-cut
      - 與光線軸(z-axis)成-45度角進行切割
      - 不常見，受溫度影響造成的誤差更大

## Ref

- [震盪器 on wiki](https://zh.m.wikipedia.org/zh-tw/%E7%9F%B3%E8%8B%B1%E6%99%B6%E4%BD%93%E8%B0%90%E6%8C%AF%E5%99%A8)

- [石英元件基礎說明](https://www.seraphim.com.tw/manager_admin/upload_file/seraphim/638/16221846381.pdf)

- [Piezoelectric oscillator](https://www.youtube.com/watch?v=Yn1OrH4lwwY)