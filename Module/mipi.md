## 介紹
- MIPI = Mobile Industry Processor Interface，行動產業處理器介面

  手機內部硬體和軟體介面的標準化，提供了全面的規範組合，用於連接行動設備中的晶片組和外圍設備

## Ref
- [MIPI自學筆記](https://zhuanlan.zhihu.com/p/92682047)

- [MIPI A-PHY：MIPI汽车系统和IOT的基石](https://zhuanlan.zhihu.com/p/149406412)

- [MIPI C-PHY Overview](https://www.graniteriverlabs.com/zh-tw/technical-blog/application-notes-mipi-c-phy)

- [MIPI D-PHY Overview](https://www.graniteriverlabs.com/zh-tw/technical-blog/mipi-d-phy-overview)
- [芯片测试——MIPI D-PHY](https://zhuanlan.zhihu.com/p/499089639)

- [mipi-i3c list](https://blog.csdn.net/yinuoheqian123/category_9222116.html)
- [mipi-i3c-I3C總線技術概述](https://blog.csdn.net/yinuoheqian123/article/details/105820030)
