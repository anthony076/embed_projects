## SPI 概述
- SPI傳輸基本概念

  Master 將要傳輸的數據寫入Buffer中，待開始傳輸條件(CS、CPOL、CPHA)滿足後，透過`移位暫存器`將數據 1bit-by-1bit 的傳輸給 Slave， Slave 透過`移位暫存器`接收 Master 的指令數據，

  和 Master 相同的方式，Slave 將要回復的數據寫入 Buffer，透過`移位暫存器`將數據 1bit-by-1bit 的傳輸給 Master

  當 Master 接收的數據到達8bit後，就會將數據轉存到 Buffer 中，等待CPU讀取數據
  
  <img src="spi\serial-transport-and-shift-reg.gif" width=50% height=auto>

- 進行傳輸(Transmit)時，
  - 對輸出數據的處理: 將會保存在移位暫存器的輸出數據，逐次將數據移出(shift-out)到SDO輸出接腳，
  - 對輸入數據的處理: 將對 SDI輸入接腳的數據進行取樣(sample)，

    取樣是為了電氣隔離，避免直接將輸入電壓直接傳遞給內部電路，也避免電壓的轉換

- Master-mode 和 Slave-mode 的差別
  - MCU 一般擔任 Master 的腳色，負責發送命令給 slave-spi-device，由 Slave 根據命令回應給 Master
  - Master-mode 會主動發出命令，再接收 slave 傳遞的 response，所以 Master 是先寫再收
  - Slave-mode 等待 master 的命令，再發送 response，所以 Slave 是先收再寫

- SPI 四種工作模式，決定傳輸開始或傳輸結束的時機 / 決定處理數據的時機
  - 注意，Master 根據 Slave (通常是 sensor) 的通訊需求決定工作模式，Slave 的 datasheet 會有通訊的SPEC

  - `CPOL，Clock-Polarity`，極性，決定CLK在Idle狀態下的的極性是0或1
    - CPOL=1，clock 高電位時為 Idle-State
    - CPOL=0，clock 低電位時為 Idle-State

  - `CPHA，Clock-Phase`，相位，決定 transmit-edge/sample-edge 的方向
    - transmit-edge = 觸發SDO/SDI`開始傳輸`的CLK邊緣方向，
    - sample-edge = `對SDI進行取樣`的CLK邊緣方向`

    - transmit-edge 和 sample-edge 是相反的
      
      CPHA 定義的是 transmit-edge，但 `transmit/sample-edge 兩者是相反的`，
      因此，transmit-edge 確定後，sample-edge 也就確定下來了

    - CPHA=0，在 clock 由 Active 轉換為 Idle 時，觸發數據(SDO/SDI)開始傳輸，Idle 轉換為 Active 時，對輸入數據(SDI)進行取樣  

    - CPHA=1，在 clock 由 Idle 轉換為 Active 時，觸發數據(SDO/SDI)開始傳輸，Active 轉換為 Idle 時，對輸入數據(SDI)進行取樣

  - CPOL 和 CPHA 一起定義的四種工作模式
    - <font color="red"> 注意，PIC晶片中，CKE 和 CPHA 都是定義數據開始傳輸的clock邊緣方向(transmit-edge)，但兩者方向相反，CKE 和 CPHA 的 sample-edge 結果一致</font>

      > CPHA=0，active to idle

      > CKE=0，idle to active

      > 在使用第三方的SPI 工具時，使用的 CPHA base 的 sample-edge，CKE = CPHA-sample-edge

    - mode0，CPOL(CKP)=Idle=`0`，CPHA=`0`=數據開始輸出的clock邊緣的方向=`Active->Idle`
      ```
      // CPHA-base
      數據開始傳輸(CPHA): 在 clk 由 Active(1) -> Idle(0)，下緣時，觸發數據(SDO/SDI)開始傳輸，
      輸入採樣: 在 clk 由 Active(1) <- Idle(0)，上緣時，對輸入數據(SDI)進行取樣
      
      // CKE-base
      數據開始傳輸(CKE): 在 clk 由 Active(1) <- Idle(0)，上緣時，觸發數據(SDO/SDI)開始傳輸，
      輸入採樣: 在 clk 由 Active(1) -> Idle(0)，下緣時，對輸入數據(SDI)進行取樣  
      ```

    - mode1，CPOL(CKP)=Idle=`0`，CPHA=`1`=數據開始輸出的clock邊緣的方向=`Idle->Active`
      ```
      // CPHA-base
      數據開始傳輸(CPHA): 在 clk 由 Idle(0) -> Active(1)，上緣時，觸發數據(SDO/SDI)開始傳輸
      輸入採樣: 在 clk 由 Idle(0) <- Active(1)，下緣時，對輸入數據(SDI)進行取樣
      
      // CKE-base
      數據開始傳輸(CKE): 在 clk 由 Idle(0) <- Active(1)，下緣時，觸發數據(SDO/SDI)開始傳輸
      輸入採樣: 在 clk 由 Idle(0) -> Active(1)，上緣時，對輸入數據(SDI)進行取樣  
      ```

    - mode2，CPOL(CKP)=Idle=`1`，CPHA=`0`=數據開始輸出的clock邊緣的方向=`Active->Idle`
      ```
      // CPHA-base
      數據開始傳輸(CPHA): 在 clk 由 Active(0) -> Idle(1)，上緣時，觸發數據(SDO/SDI)開始傳輸，
      輸入採樣: 在 clk 由 Active(0) <- Idle(1)，下緣時，對輸入數據(SDI)進行取樣
      
      // CKE-base
      數據開始傳輸(CKE): 在 clk 由 Active(0) <- Idle(1)，下緣時，觸發數據(SDO/SDI)開始傳輸，
      輸入採樣: 在 clk 由 Active(0) -> Idle(1)，上緣時，對輸入數據(SDI)進行取樣  
      ```

    - mode3，CPOL(CKP)=Idle=`1`，CPHA=`1`=數據開始輸出的clock邊緣的方向=`Idle->Active`
      ```
      // CPHA-base
      數據開始傳輸(CPHA): 在 clk 由 Idle(1) -> Active(0)，下緣時，觸發數據(SDO/SDI)開始傳輸
      輸入採樣: 在 clk 由 Idle(1) <- Active(0)，上緣時，對輸入數據(SDI)進行取樣
      
      // CKE-base
      數據開始傳輸(CKE): 在 clk 由 Idle(1) <- Active(0)，上緣時，觸發數據(SDO/SDI)開始傳輸
      輸入採樣: 在 clk 由 Idle(1) -> Active(0)，下緣時，對輸入數據(SDI)進行取樣  
      ```
  
  - CPHA-base 的 edge 差異
    
    <img src="spi\spi-4mode-idle-CPOL-CPHA.jpg" width=80% height=auto>

    CKE-base 的 edge 差異

    <img src="spi\spi-4mode-idle-CPOL-CKE.png" width=60% height=auto>

- 範例，從 slave-spi-device 的時序圖，決定 CPOL 和 CPHA 參數

  <img src="spi\find-CPOL-CPHA-from-waveform.jpg" width=80% height=auto>

  - step1，決定 CPOL (決定 idle-level)

    從 Figure 4-1 的紅字1可以看出，當CS產生變化時，SCK處於IDLE狀態，此時SCK的電位為0，
    因此，CPOL應設置為0，代表IDLE的電位為0，Active的電位為1

  - step2，決定 CPHA (決定edge)
    
    `觸發數據開始輸出的CLK邊緣`和`觸發輸入數據採樣的CLK邊緣`是相反的，
    因此，找到 SDO 開始/結束輸出的CLK邊緣的中間位置，就是輸入採樣的CLK邊緣，
    起始輸出/結束輸出的CLK邊緣是同方向的，中間位置則是反向的

    從 Figure 4-1 的紅字2可以看出，X 標記的位置，是傳輸開始和傳輸結束的區間，兩者是同向的，都是在 CLK 下緣時觸發，
    X 的中間位置則是反向的，在 CLK 上緣時觸發，該位置對應的CLK邊緣，就是觸發輸入採樣的CLK邊緣(上緣)

    因此，輸入採樣在 CLK 上緣 (0->1)，且 idle 為 0，則 CLK 0->1，可替換為 idle -> active，
    可以得到，CPHA=0，選擇工作在 mode0

  - step3，決定輸入取樣的時機

    輸入取樣的時機可以選擇在 `SDO輸出周期的中間位置`，也可以在 `SDO輸出周期的結束位置`，
    
    檢查時序圖中，觸發`SDO數據`和`SDI數據`開始傳送的位置，是否有相位差決定，
    若無相位差，選擇中間取樣，若有相位差，選擇結束位置取樣

    <img src="spi\spi-sdi-sample-timing.png" width=80% height=auto>

    比較 Figure 4-1 的 SDO 和 Figure 4-2 的 SDI，可以發現 兩者是同相位的，因此選擇中間觸發輸入採樣