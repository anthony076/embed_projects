## ADC 基礎
- ADC 將輸入的連續類比電壓，轉換為 nbit 的數位值 (2^n)
  - AD轉換 `是類比值對數位值的映射`，映射後的數位值以二進制表示
    - 對照表

      | 類比值 | 數位值 |
      | ------ | ------ |
      | 0V     | 0000   |
      | 0.625V | 0001   |
      | 1.250V | 0010   |
      | 1.875V | 0011   |
      | 2.500V | 0100   |
      | 3.125V | 0101   |
      | 3.750V | 0110   |
      | 4.375V | 0111   |
      | 5.000V | 1000   |
      | 5.625V | 1001   |
      | 6.250V | 1010   |
      | 6.875V | 1011   |
      | 7.500V | 1100   |
      | 8.125V | 1101   |
      | 8.750V | 1110   |
      | 9.375V | 1111   |
      | 10.00V | 10000  |

    - 解析度計算
      
      輸入電壓 10 映射到數位值 2^4 ，

      解析度 = 類比電壓最大值 / 數位最大值 = 10v / 16 = 0.625
    
    - 計算數位值
      
      解析度相當於間距長，數位值中，每一階的間距都應該是一樣的，

      resolution = 10v / 16 = 5v / 數位值

      數位值 =  16 * 5 / 10 = 8 = 1000

  - 類比值和數位值具有一定的映射關係，因此波形不變

    <img src="adc/waveform-compare.png" width=50% height=auto>

  - 數位值的位數越多，解析度越高

    <img src="adc/resolution.png" width=80% height=auto>

  - 採樣頻率越高，失真度越低，
    根據 Nyquist-Sampling-Theorem，採樣頻率(fs)應該是輸入頻率(fmax)的兩倍，
    即 `fs >= 2 * fmax`

    <img src="adc/sample-rate-should-twice-than-input.png" width=80% height=auto>

- ADC 轉換流程

  <img src="adc/adc-conver-flow.png" width=80% height=auto>

  - 功能塊1，採樣電路，

    ADC 轉換時，應避免直接取用輸入的類比電壓，避免外部電壓對內部電路的影響，
    因此，通常會使輸入類比電壓對內部電容器進行充電，由電容器對輸入類比電壓進行採樣，採樣電壓 == 輸入類比電壓

    採樣的時間稱為獲取時間(Acquisition Time)，確保採樣電容充電到輸入電壓的最小時間

  - 功能塊2，保持電路
    - ADC 轉換時，需要時間進行量化和編碼，因此採樣後需要將採樣電壓保持一段時間，
      此時間稱為`保持週期`，此時間用於數位值的轉換

    - 進行數位值的轉換時，會透過 switch 外部電路斷開，必免受外部電路影響

    - 確保取樣到的電壓不是雜訊造成的

  - 功能塊3，量化和解碼電路
    - AD轉換可以由以下電路實現
    - [Single-Slope-ADC](#single-slope-adc-原理)
    - dual-Slope-ADC
    - [Flash-ADC](#flash-adc)
    - Digital-Ramp-ADC
    - [Successive-Approximation-ADC(SAR-ADC)](#sar-adc)
    - Tracking-ADC
    - Slope-ADC(integrating-ADC)
    - Delta-Sigma-ADC

## Single-Slope-ADC 原理
- Circuit
  
  比較器的輸出，是由積分器輸出電壓和 Vin 的比較得到，

  <img src="adc/single-slope-adc-circuit.png" width=80% height=auto>

  - 積分器輸出電壓 < Vin，比較器輸出 1，計數器開始計數，
  - 積分器輸出電壓 == Vin，比較器輸出 0，計數器停止計數，

  <img src="adc/single-slope-adc-convert.png" width=80% height=auto>

  RESET 開關，在比較器輸出為0時會自動閉合，用於清除電容器上的電荷，
  確保下一次轉換前，電容器是空的
  <img src="adc/single-slope-adc-circuit-with-switch.png" width=80% height=auto>

  R 和 C 的值，會影響充電的斜率，進一步影響積分器達到輸出電壓的時間，
  R 和 C `會隨著時間和溫度而變化`，造成 single-slope-adc 的精度不佳
  
  <img src="adc/charge-slope-and-rc.png" width=50% height=auto>

## Flash-ADC 
<img src="adc/flash-adc.png" width=50% height=auto>

## SAR-ADC
- 用於低成本/中高分辨率應用
- SAR ADC 的分辨率範圍為 8 - 18 位，採樣速度高達每秒 5 兆樣本 (Msps)
- 小尺寸和低功耗，常用於便攜式電池供電儀器的原因
- SAR-ADC 電路
  
  <img src="adc/sar-adc-circuit.png" width=80% height=auto>

- S/H，Sample-and-Hold-Circuit，[採樣和保持電路](https://circuitdigest.com/electronic-circuits/sample-and-hold-circuit-op-amp)
  - 採樣週期
    
    在採樣週期，輸入電壓會`對採樣電容充電`，使兩者相同，
    採樣電壓需要與輸入電壓一致，避免失真
  
  - 保持週期
    
    在保持週期，採樣電壓會輸入 SAR-ADC 的電路，進行數位值的轉換

  - 採樣週期和保持週期是交替進行的

    採樣和保持是交地進行的，並由開關控制 採樣週期和保持週期的開關，
    <img src="adc/sample-and-hold-waveform.png" width=80% height=auto>

    實際電路

    <img src="adc/sample-and-hold-circuit.png" width=80% height=auto>

  
- 原理，採用二分搜尋法
  - 以參考電壓 Vref = 10v，4bit ADC 為例

    resolution = 10v / 2^4 = 0.625v

    | 類比值 | 數位值 |
    | ------ | ------ |
    | 0V     | 0000   |
    | 0.625V | 0001   |
    | 1.250V | 0010   |
    | 1.875V | 0011   |
    | 2.500V | 0100   |
    | 3.125V | 0101   |
    | 3.750V | 0110   |
    | 4.375V | 0111   |
    | 5.000V | 1000   |
    | 5.625V | 1001   |
    | 6.250V | 1010   |
    | 6.875V | 1011   |
    | 7.500V | 1100   |
    | 8.125V | 1101   |
    | 8.750V | 1110   |
    | 9.375V | 1111   |
    | 10.00V | 10000  |

  - 將SAR輸出的最高位設置為1，得到 1000，經過 DAC 得到比較電壓5V
  - 輸入電壓 5.2v > 比較電壓5V，比較器輸出1，SAR 第4位保持為1，並將SAR第3位設值為1
  
  - SAR輸出為1100，經過 DAC 得到比較電壓7.5V
  - 輸入電壓 5.2v < 比較電壓7.5V，比較器輸出0，SAR 第3位變更為0，並將SAR第2位設值為1

  - SAR輸出為1010，經過 DAC 得到比較電壓6.25V
  - 輸入電壓 5.2v < 比較電壓6.25V，比較器輸出0，SAR 第2位變更為0，並將SAR第1位設值為1
  
  - SAR輸出為1001，經過 DAC 得到比較電壓5.625V
  - 輸入電壓 5.2v < 比較電壓5.625V，比較器輸出0，SAR 第1位變更為0

  - 得到最後的數位值結果 1000 = 5v
  
## Ref  
- [Match the Right ADC to the Application](https://www.digikey.tw/zh/articles/match-the-right-adc-to-the-application)
- [ADC架構](https://www.sciencedirect.com/science/article/pii/S2589208820300077#fig0001)
- [ADC and DAC (Analog to Digital And Digital to Analog Converters)](https://www.youtube.com/playlist?list=PLwjK_iyK4LLCnW-df-_53d-6yYrGb9zZc)
  
- Flash-ADC
  - [Flash ADC (Parallel ADC) and Half-Flash ADC Explained](https://www.youtube.com/watch?v=NASkjo7s8f4)
  - [Flash ADC](https://www.allaboutcircuits.com/textbook/digital/chpt-13/flash-adc/)

- SAR-ADC
  - [Successive-Approximation-ADC](https://www.allaboutcircuits.com/textbook/digital/chpt-13/successive-approximation-adc/)
  - [Successive Approximation ADC Explained](https://www.youtube.com/watch?v=h0CGtr4SC9s)
  - [How does Successive Approximation (SAR) ADC Work](https://circuitdigest.com/article/how-does-successive-approximation-sar-adc-work-and-where-is-it-best-used)
  - [SAR ADC驅動電路設計](http://news.eeworld.com.cn/mp/DigiKey/a135939.jspx)

- Oether-ADC
  - [Digital-Ramp-ADC](https://www.allaboutcircuits.com/textbook/digital/chpt-13/digital-ramp-adc/)
  - [Tracking-ADC](https://www.allaboutcircuits.com/textbook/digital/chpt-13/tracking-adc/)
  - [Slope-ADC(integrating-ADC)](https://www.allaboutcircuits.com/textbook/digital/chpt-13/slope-integrating-adc/)
  - [Delta-Sigma-ADC](https://www.allaboutcircuits.com/textbook/digital/chpt-13/delta-sigma-adc/)
