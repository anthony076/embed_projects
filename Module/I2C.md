## i2c 硬體特性
- 3種操作模式
  - Master-mode，主動模式
  - Multi-Master-mode，多主動模式，`可連接多個 Master`
  - Slave-mode，被動模式，`可連接多個 Slave`

- 5種傳輸速度

  | 模式                          | 速度        |
  | ----------------------------- | ----------- |
  | 標準模式(standar-mode)        | 100K-bits/s |
  | 快速模式(fast-mode)           | 400K-bits/s |
  | 強化快速模式(fast-mode-plus)  | 1M-bits/s   |
  | 高速模式(high-speed):         | 3.4M-bits/s |
  | 超高速模式(ultra-high-speed): | 5M-bits/s   |
  
- 為什麼需要上拉電阻(pull-high)
  
  參考，[GPIO 為什麼需要 pull-high 電阻](gpio.md#gpio-為什麼需要-pull-high-電阻)

  <img src="i2c/i2c-why-pull-high-res.png" width=50% height=auto>
  
  <br>
  
  所有i2c裝置(master/slave)的SCL或SDA的GPIO接腳，都是連接到內部的 
  `CMOS開源極(CMOS-Open-Drain)`或是`TTL開集極(TTL-Open-Source)` 的電晶體，
  
  此兩種方式在內部`電晶體導通`時，都會將 `SCL/SDA 的位準拉低為 0`，
  在`電晶體未導通`時，SCL/SDA 為`浮接狀態`，由外部決定狀態，

  在 i2c 的規範中，Idle 的狀態為 HIGH，為避免SCL/SDA處於浮接狀態，
  因此需要將SCL/SDA加上電阻，讓浮接狀態的SCL/SDA強制為 HIGH

  (選用) [抗干擾用的 Rs](https://blog.csdn.net/yinuoheqian123/article/details/105745503)

  i2c 設備依現況的需要可選擇加裝一對抗干擾用的電阻 Rs，
  尤其是當 SCL/SDA  的內部佈線經過特殊干擾源時

- `BUS電容值`+`上拉電阻`對訊號的影響
  - 受 raising-time 影響的訊號

    <img src="i2c/i2c-pullup-and-raising-time.png" width=80% height=auto>

  - `影響1`，SCL/SDA 無法達到預期電壓

    BUS 的電容值(C)和上拉電阻(R)形成一個RC充電電路，會影響 SCL/SDA 訊號的上升時間(raising-time)，長的上升時間會導致 SCL/SDA 無法充電到預期的電壓，

  - `影響2`，減少 HIGH 訊號的有效時間

    即使可以達到 HIGH 的判定電壓，上升時間的增加，也會壓縮到 HIGH 的有效時間

- i2c 的電氣特性
  - 圖解

    <img src="i2c/i2c-eletrical-signal.png" width=90% height=auto>

  - 通訊基礎，i2c 以 `Wired-AND` 的方式連接在一起
    - `Wired`: 二組或多組的接線直接就接在一起
    - `AND`: 連接在一起的的接線具有 AND 的效果
    - `Wired-AND`
      - 所有接腳都輸出 HIGH 時，才能在接線上量測到 HIGH
      - 只要有一個接腳輸出 Low，在接線上就可以量測到 Low

    - 根據 Wired-AND 的性質，可以得到 i2c 具有以下的特性
      - 所有i2c裝置都可以偵測BUS的狀態，
      - 任何i2c裝置都`可以自由控制BUS輸出為LOW`，但無法控制 BUS 輸出 HIGH
      - 必須`所有i2c裝置都輸出為HIGH`，才能使 BUS 輸出 HIGH

  - `BUS狀況1`，Idle狀態
    - SCL、SDA 都是 HIGH

  - `BUS狀況2`，Start或Stop的控制訊號，
    - SCL = 1，若 SDA 電位有變化，則視為是控制訊號
      - SDA為下緣(1->0): Start 訊號
      - SDA為上緣(0->1): Stop 訊號
  
  - `BUS狀況3`，Data或Ack的傳輸狀態
    - SCL = 0，SDA 可以改變狀態 (可以改變數據內容)，用於設置 Data 值
    - SCL = 1，SDA 狀態不變，SDA必需保持訊號穩定，以便接收端讀取(栓鎖)資料

    - 傳輸 Ack-bit
      - Ack=0，傳輸完成
      - Ack=1，傳輸未完成，傳輸出現錯誤
    
    - 注意，接收端讀取狀態時`沒有明確定義讀取時機`，

      只要在 SCL = 1 的時間內，Slave 都可以讀取，i2c 只定義可讀取/不可讀取的狀態，
      相較之下，SPI 規定必須在 clock 上緣或下緣時讀取

  - i2c的接腳，是雙向傳輸的接腳，發送端輸出數據的同時，也會等待接收端ACK的輸入，
    
    以 slave 回覆數據給 master 為例 (slave為發送端，master為接收端)，

    <img src="i2c/i2c-output-and-input-at-same-time.png" width=80% height=auto>
    
    - 注意，
      
      在一次完整的傳輸過程( master發送指令 + master接收回覆的數據 )中，
      `發送端輸出數據後，會等待接收端輸入訊號，確認接收端收到後，才會進行下一筆數據的傳輸`，
      而`不是等到輸出結束之後再接收`

    - i2c 沒有明確定義`讀取的時機`，由對方狀態決定讀取的時機
    
      因此，接收方(可能是Master或Slave) 接收數據的同時，也需要輸入接收狀態給對方，才能讓對方執行後續的動作

    - `狀況1`，Master發送指令(發送端)，Slave接收指令(接收端)，
      
      Master 收到 Slave 輸入的 Ack = 0，資料已正確收到，

      Master 收到 Slave 輸入的 Ack = 1，資料未正確收到，
      此時，Master 需要主動發出 Stop 控制訊號，來停止 BUS 上的傳輸

    - `狀況2`，Slave發送回覆(發送端)，Master接收指令(接收端)，

      Slave 收到 Master 輸入的 Ack = 0，資料已正確收到，Slave 準備發送下一筆資料

      Slave 收到 Master 輸入的 Ack = 1，資料未正確收到，
      Slave 只能做到通知功能，無法進一步的處理
    
  - 利用`輸入緩衝器`偵測 BUS 的輸入狀態
    
    <img src="i2c/i2c-use-input-buffer-to-detect-bus-status.png" width=70% height=auto>

    為什麼需要偵測BUS的狀態，

    - 對參與傳輸的裝置
      
      一般晶片的GPIO可以在輸入或輸出之間切換，但i2c使用的接腳，是輸出`同時`也是輸入，
      
      因此，`在輸出訊號的同時，需要透過輸入緩衝器偵測BUS的狀態`，  
      
      在數據輸出的同時，透過`輸入緩衝器`偵測BUS的狀態，可以判斷接收端是否將 SDA 的位準拉低為0 (ACK-bit)
      
    - 對未參與傳輸的裝置
  
      用於判斷BUS上是否有傳輸在進行

- 訊號變化率(SlewRate)和雜訊之間的抉擇
  - 為什麼需要 高訊號變化率(Fast-SlewRate)
    
    Slew-Rate，指訊號由1<->0的速度，Slew-Rate 越高，輸出的波形越不容易失真，但是越容易受到產生EMI的雜訊，
    且 SlewRate 越高，代表 i2c 裝置可以工作在更快的工作頻率
  
    由 i2c 的電氣特性可以得知，i2c 裝置需要對 SCL 訊號進行偵測，才能判斷何時改變 SDA 接腳的狀態，
    因此，需要一個`足夠快的上升時間(i2c規範最大的上升時間為 1000ns)和下降時間`，才能夠正確判斷SCL/SDA的位準，
    但是 Fast-SlewRate 同樣會帶來一些副作用
  
  - `副作用1`，容易產生EMI影響電路

    快速的SlewRate容易在轉換的瞬間，因電路的寄生效應產生的高頻雜訊(ringing)，
    此高頻雜訊是電路的EMI來源，容易干擾電路其他元件的運行
    
    <img src="i2c/i2c-noise-ringing-by-high-slew-rate.png" width=70% height=auto>
    
    參考，[EMI的來源](https://www.edn.com/edge-rate-control-speeds-circuits/) 和 
    [How to use slew rate control for EMI reduction](https://training.ti.com/engineer-it-how-use-slew-rate-control-emi-reduction)

  - `副作用2`，SCL/SDA走線之間的耦合干擾(crosstalk)
    
    高頻信號對相鄰的走線之間產生的干擾

    <img src="i2c/i2c-crosstalk-at-fast-slewrate.png" width=80% height=auto>

  - 透過降低SlewRate可以減少信號反射引起的震鈴現象(ringing)，並限制信號線之間的串擾(crosstalk)，
    但是會增加上升/下降時間

- 上拉電阻(pullup) 的計算
  - i2c 是 open-drain 的形式，
    - 要輸出 low 時，使電晶體on，使輸出電壓接地
    - 要輸出 high 時，`使電晶體off，使輸出外部電路決定`，

  - `LOW-state` 的上拉電阻，最小電阻值的計算

    <img src="i2c/pullhigh-LOW-state.png" width=80% height=auto>
    
    - Rp 太小，沒有多餘的電阻可以吸收多餘的電流，造成輸出的電位較高，使得 LOW 狀態的條件無法滿足
    - Rp 越高，可以吸收的電流越多，Rp 上的壓降越高，使輸出電位為LOW的電壓越低
    - Rp 越高，使用`較低的電流`，就可以使輸出電位轉換為 LOW

  - `HIGH-state` 的上拉電阻，最大工作頻率的計算

    <img src="i2c/pullhigh-HIGH-state.png" width=100% height=auto>

    - Hight-state 實際上是將內部的電晶體關閉，輸出電壓為 Vcc 對 bus 寄生電容的充電電壓

    - 寄生電容 (Cbus) 的組成為 輸入引腳電容 和 走線電容
    
    - Rp 越高，輸出電位由 LOW 轉換為 HIGH 的電位不足，
      或要獲取足夠的 HIGH 電位就要延長充電時間，而導致 i2c 的工作頻率下降


- 電壓位準的轉換 (level-shifting)
  - 一般電壓位準的轉換方式

    <img src="i2c/general-level-shift.png" width=70% height=auto>

  - 為什麼 i2c 不適用於一般的位準轉換方式
    - i2c 在輸出數據時，同時也是輸入數據
    - i2c 沒有明確的訊號指定傳輸方向的轉變
    - 由於 Wired-AND 的特性，i2c 輸出 high，bus 上不一定是 high，只有所有人都是 high，bus 的電位才是 high

  - 適用於 i2c 裝置的位準轉換

    <img src="i2c/i2c-level-shift.png" width=70% height=auto>

    - 注意，MOSFET 的 source 端需要接在低壓側，drain 端需要接在高壓側，

    - `狀況1，兩側都為 HIGH`
      - Vg = Vs = 3.3v，Vgs = Vg - Vs = 0 V，TR1 和 TR2 都是 off
      - Vd=5v > Vs=3.3V，逆偏壓，因此 TR1 和 TR2 都是 off

    - `狀況2，5V 側為 LOW，驅動 3.3V`
      - Vd = LOW = 0V，Vs > Vd，順偏壓，d 和 s 之間的 diode 導通，使得 Vs 電壓下降，
      - Vs 電壓下降，使得 Vgs > Vth，MOSFET 導通，5V 側將 3.3 側 bus 的電壓拉低，直到兩側的電壓相同的低電位

    - `狀況3，3.3V 側為 LOW，驅動 5V`
      
      Vs = LOW = 0v，使得 Vgs > Vth，MOSFET 導通，
      3.3V 側將 5 側 bus 的電壓拉低，直到兩側的電壓相同的低電位

## i2c 通訊基礎
- 原則，任何人(Master/Slave)都可以將電位設置為LOW，但所有人都是 HIGH，bus 才變成 HIGH
  - 參考，`Wired-AND`是i2c通訊的基礎，見上述的`i2c的電氣特性`
  - 透過此方式，不像SPI，`不需要明確規定傳輸和取樣的時機`，
  - 透過此方式，參與傳輸的接收端，可以`主動變更bus的狀態`，讓傳送端可以知道接收的狀況，
  - 透過此方式，不參與傳輸的其他裝置，都可以`讀取bus的狀態是否為LOW`，得知是否傳輸正在進行中，避免發生傳輸衝突

- i2c 的數據包格式
  - 通用格式

    <font color="red">Start</font> + Bit7 - Bit0 (共8bit) + <font color="blue">Ack</font> + <font color="red">Stop</font>
  
    - Start / Stop，只能由 Master 發起，
    - Ack，由接收方發起，接收方可以是 Master 或 Slave

  - 定址用格式

    <font color="red">Start</font> + A6 - A0 (共7bit) + <font color="green">RW</font> + <font color="blue">Ack</font> + <font color="red">Stop</font>

    - 當`傳送的數據為 Address 時`，bit-0 為 RW-bit，用來表示要進行的操作


- 指定 slave 的定址系統
  - 每次傳輸最少2byte，第1個byte必定是 Address
    
    master 發起傳輸的第一個byte是`用來點名的`

    因為 i2c 透過 address 來實現選擇特定 i2c-slave-device 的目的，
    
    因此，`i2c 最小的傳輸量為 2byte = (1byte-addr) + (1byte-data)`
    
  - slave-address，只有 i2c-slave-device 才需要有 Addr
  
    i2c-master-device 不需要 Addr，Addr 是用來呼叫 slave 裝置的，
    進行通訊時，未參與傳輸的 master 會檢測 bus 上是否有傳輸在進行，
    
    因此，bus上`同時間只會有一組傳輸在進行`，slave 不需要透過Addr來指定要接收的 master，只有發起傳輸的 master 會接收
  
  - 每個 i2c-slave 都可以會有唯一的 Address，最多可以有 128 個 i2c-slave-device

  - slave-address 格式，

    - 7-bit-address / 8-bit-address

      | 定址方式      | Start | bit7 | bit6 | bit5 | bit4 | bit3 | bit2 | bit1 | bit0(RW) | ACK |
      | ------------- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | -------- | --- |
      | 7-bit-address | Start | A    | A    | A    | A    | A    | A    | A    | 手動添加 | ACK |
      | 8-bit-address | Start | A    | A    | A    | A    | A    | A    | A    | RW       | ACK |

      7-bit-address 和 8-bit-address 的差異
      - 7-bit-address = (start) + (bit7 - `bit 1`) + (ACK)，`未包含 bit0(RW)`，需要手動加上
      - 8-bit-address = (start) + (bit7 - `bit 1`) + (bit0-RW) + (ACK)，`已包含 bit0(RW)`，不需手動加上
      - 本質上，7-bit-address和8-bit-address的數據格式完全相同，差別在是否有包含 bit0(RW)
      - 例如，
        ```
        7-bit-address = 0x20 = 0b_010_0000，需要手動加上 bit0(RW)，變成 0b_010_0000_0 = 0x40
        8-bit-address = 0x20 = 0b_0010_0000，不需要手動加上 bit0(RW)，
        ```

    - 10-bit-address
      - 10-bit-address 是 7-bit-address 的延伸，與 7-bit-Address 兼容
      - 10-bit-address 會將位址分成兩次傳送
        - 第1次傳送，`11110 + A9 + A8 + RW`，其中，11110 用來標記是 10-bit-address
        - 第2次傳送，`A7 - A0`

- `通訊過程` - 總覽
  - 分為`單次讀寫`和`連續讀寫`，
  
  - `階段1`，master 發送指令給 slave，
    - master 發起 START 狀態，通知 slave 開始進行傳輸

    - `master 發送第1個byte的數據(發送 slave-address + RW=0)`
      - 若 master 收到 Ack=0 (from slave)，代表 slave 有效，master 繼續傳輸第2個byte的數據
      - 若 master 收到 Ack=1 (from slave)，代表 slave 無效，master 發起 STOP 停止傳輸
    
    - `master 發送第2個byte的數據(發送指令)` 
      - (單次傳輸) 若 master 收到 Ack=0 (from slave)，代表 slave 已接收數據，master `將發起 STOP 停止傳輸`
      - (連續傳輸) 若 master 收到 Ack=0 (from slave)，代表 slave 已接收數據，master `將會繼續傳輸`

      - (單次傳輸) 若 master 收到 Ack=1 (from slave)，代表 slave `未接收數據`，master 發起 STOP 停止傳輸
      - (連續傳輸) 若 master 收到 Ack=1 (from slave)，代表 slave `已超收數據`，master 發起 STOP 停止傳輸 
  
  - `階段2`，master 從slave接收數據，
    - master 發起 START 狀態，通知 slave 開始進行傳輸

    - `master 發送第1個byte的數據(發送 slave-address + RW=1) `
      - 若 master 收到 Ack=0 (from slave)，代表 slave 有效，master 繼續傳輸第2個byte的數據
      - 若 master 收到 Ack=1 (from slave)，代表 slave 無效，master 發起 STOP 停止傳輸
    
    - `slave 發送第2個byte的數據(回覆數據)` 
      - (連續傳輸) 若 slave 收到 `Ack=1` (from master)，`master 讀取未結束`，還會讀取更多數據 

      - (單次/連續傳輸) 若 slave 收到 Ack=0 (from master)，代表 master 已經讀取完所有資料，
        master 將發起 STOP 停止傳輸

- `通訊過程` - 單次讀寫詳解
  - 圖解

    <img src="i2c/i2c-single-transaction-diagram.png" width=80% height=auto>
  
  - 在SCL=0時，master設置SDA數據，在SCL=1時，slave讀取SDA數據

  - `階段1`，master 寫入指令給 slave
    - step1，master 的 SCL 開始輸出訊脈波訊號，
      - master 以 START 狀態發起傳輸
      - 在 SDA 上依序設置 `slave-addr` + RW=0(寫入slave) 的數據
      - 在第8個SCL周期結束後，釋放SDA的驅動進入監聽，SDA-bus 會因為 pull-high 的硬體設計而維持為1
      - 注意，釋放SDA的驅動(SDA=1)，應該在 SCL 還是 LOW 之前進行，
        若在 SCL=1 之後才釋放，會變成是控制訊號(SCL = 1， SDA = 0 -> 1，為 STOP 訊號)

    - step2，slave 在第9個SCL周期開始，開始驅動SDA，並將SDA設置為0或1

    - step3，master 在第9個SCL周期監聽SDA的變化，並進行相應的處理
      - 若 Ack=0，slave 已經收到，master 繼續階段2的傳輸，開始傳輸指令數據給 slave
      - 若 Ack=1，slave 未收到，master 發出 STOP 狀態來結束傳輸
      
    - step4，master 收到ACK後，繼續輸出SCL，並在 SCL=1 時，在SDA上設置指令數據，共8個SCL週期
      - 在第8個SCL周期結束後，釋放SDA的驅動進入監聽，SDA因pull-high的硬體設計而維持為1

    - step5，slave 在第9個SCL周期開始，開始驅動SDA，並將SDA設置為0或1
      - 若 Ack=0，slave 已經收到，master 發出 STOP 狀態來結束傳輸
      - 若 Ack=1，slave 未收到，master 發出 STOP 狀態來結束傳輸
  
  - `階段2`，master 接收 slave 回覆的數據
    
    與`階段1`類似，都需要由 master 利用 START 狀態發起傳輸開始，
    並在第1個byte透過定址系統呼叫 slave ，確認 slave 是有效的，
    差別在於，`寫入第2個byte的裝置不同`

    在此階段中，slave 屬於`被動端`，`即使收到 master 的狀態也無法進行處理`，
    因此，應該站在 master 的角度來看，

    master -> slave，`ACK=1`，master 還會繼續讀取更多資料，請 slave 繼續傳輸，

    master -> slave，`ACK=0`，master 已經接收完畢，準備發送 STOP 訊號給 slave

- `通訊過程` - 連續讀寫詳解

  <img src="i2c/i2c-continue-transaction-diagram.png" width=80% height=auto>

  連續讀寫在 master 不送出 STOP 狀態之間，該次傳輸就可以連續寫入數據(在驗證過slave-address有效之後)，
  - master 透過 ACK=1，告訴 slave 持續接收 master 的指令，或是 slave 持續回復數據給 master，
  - master 透過 ACK=0，告訴 slave 傳輸結束，即將要發起 STOP 狀態

  若 master 沒有遵守此協議，容易造成`當前傳輸的數據錯誤`，或是`下次傳輸的數據錯誤`

  - 當前傳輸的數據錯誤
    > master 發送 Ack=0 告知即將結束，卻沒有發送 STOP 狀態，持續輸出SCK，要求 slave 回復資料

  - 下一次傳輸的數據錯誤，
    > master 發送 Ack=1 告知繼續傳送，卻發送 STOP 停止傳輸

- 通訊實務
  - i2c-slave-device 的內部使用 EEPROM 或 SDRAM 來儲存數據，
    - EEPROM 使用 page 對記憶體空間進行管理，寫入時間較長
    - SDRAM 不使用 page 管理，可直接存取，寫入時間較短
    
  - EEPROM 需要指定記憶體的`索引(index)值`，以選擇要存取的暫存器
 
  - EEPROM 將記憶體切分成多個 page 進行管理，

    例如，未使用 page 管理前，
    256bytes 大小的記憶體，可以定址 0 - 255 個 index，每個 index 占用 1byte 記憶空間

    將 256bytes 切分為多個 page 進行管理，假設每個 page 占用 16 bytes，
    
    256bytes 可以被切分為 256 / 16 = 16 個 page，每個 page 可定址 16 個暫存器空間 (0x0 - 0xf)
    - 0x0 - 0xf = page-0
    - 0x10 - 0x1f = page-1
    - 其餘依此類推

  - EEPROM 對超出 page 邊界的處理

    在使用 page 的狀況下，若寫入的資料超過 page 的邊界，
    大部分的狀況會將位址重新設置為該 page 的`起始位置`後，再將數據寫入，
    例如，從 0xf 的 index 位置，寫入 2byte 的數據，1byte 會放在 0xf，1byte 會放在 0x0 的位置
    
  - EEPROM 寫入的等待時間  
    
    記憶體若使用 EEPROM，EEPROM 的寫入是以 page 為單位，因此寫入 1byte 和寫入一個 page 所花費的時間是一樣的，大約占數個 ms，
    因此，master 剛進行完寫入的操作後，一般需要等待 ms 寫入完成後，才能進行其他操作，

  - 實務範例

    <img src="i2c/i2c-communication-example.png" width=100% height=auto>

- 通訊範例

  <img src="i2c/i2c-communication-example2.png" width=80% height=auto>

## 時脈擴展 (clock-stretching)，用於慢速 slave 延長處理時間
- 為什麼需要時脈擴展
  
  SDA 是一個雙向的接腳，內部有`輸入緩衝器`可以在接腳`輸出數據`的同時，透過輸入緩衝器`偵測接收端輸入的狀態`，
  SCL 同樣是一個雙向的接腳，可以讓 master 在輸出時脈的同時，由`慢速的slave`控制是否`延長處理時間`

- clock-stretching 的操作方式
  - 圖解

    <img src="i2c/i2c-clock-stretching-by-slave.png" width=80% height=auto>

  - 必須是由慢速的`i2c-slave-device`發起
  
  - slave 在 SCL 為 LOW 時，`主動`使 `SCL` 的訊號維持在低位準
    
    因為 Wired-AND 的電氣特性，
    - bus 必須在所有人都輸出1，bus 才會為1
    - 只要有一人輸出0，bus 就會維持為0
    
    slave 拉低 bus 上的位準，雖然 master 的 CLK 將電路設置為1，
    但因為 slave 維持在0，最後 bus 仍然會維持在0

  - master 的輸入緩衝器監聽 SCL 的狀態

    為了確認 slave 是否處於忙碌中，master 會利用 SCL 內部的輸入緩衝器
    來監聽 SCL 的狀態，master 會在每一個放開 SCL 的瞬間，
    監聽 SCL 是否有隨著 master 的放開而回復為 HIGH，
    若 SCL 因 slave 維持為 LOW，就暫停時脈的輸出，直到 SCL 恢復為 HIGH
    
  - 等待的時間限制
    
    等待時間並沒有特殊的限制，i2c-spec 只有速度的上限，沒有定義下限
    (SCL 被拉低會降低速度)，但沒有下限同樣會造成問題，
    因此，i2c的衍生協定(SMBus)中會加入了 time-out 的機制，來限制最低速度

- clock-stretching 的時機
  - 在 標準模式、快速模式、及快速模式則每一個 bit 的 SCL 都可以被 clock-stretching

  - 在 高速模式，只容許在 ACK/NACK bit 的位置上被 Clock Stretch

## 多個 master 的仲裁機制 (bus-arbitration)
- 為什麼需要仲裁

  若只有1個 master + 多個 slave 時，不會有需要仲裁的機會，仲裁只會發生在有多個 master 的狀況，

  仲裁是 master 上的機制，用於在 master 在發送數據時，如何得知 bus 上有其他傳輸存在，並自行停止傳輸

- 多 master 通訊的基本原則，先聽後說

  bus 上有 START 狀態，bus 就會進入忙碌的狀態，直到 STOP 狀態維持一小段時間後，bus 才會恢復閒置，
  因此，在多個 master 的網路中，master 會`監聽 bus 上的 START 和 STOP 狀態`，並確定` bus 處於閒置狀態`時，才會發起新的傳輸

  即使遵守`先聽後說`的原則，仍然有機會面臨`兩個 master 都確認 bus 是閒置狀態`，
  但幾乎是同時發起新的傳輸而造成了衝突，此時就需要有解決衝突的機制，仲裁會經過以下兩個過程
  - 過程1，SCL的時脈同步 (clock-sychronize)
  - 過程2，SDA的數據仲裁 (bus-arbitration)

- `過程1，SCL的時脈同步 (clock-sychronize)`

  在仲裁前，因為每個master都有自己獨立的clock，每個 clock 都有不同的速度，
  因次仲裁前需要進行 clock 的同步，才有比較的基礎，

  在進行時脈同步時，master 內部會有兩個用於時脈同步的暫存器，
  分別用來記錄 `bus 持續為 LOW 的時間(low-time)`，和 `bus 持續為 HIGH 的時間(high-time)`

  - `low-time` = master中，拉低時間最長的贏
    
    兩個 master 同時將 SCL 的腳位拉低，就會`開始記錄 bus 為 LOW 的時間`，
    根據 Wired-AND 的特性，只有有一方輸出0，bus的電位就是0，

    若有其中一個 master `提早釋放 SCL 的控制`，使得 SCL 的輸出為 HIGH，`並不會改變 bus 電位`，
    直到所有的 master 都釋放對 SCL 的控制 (所有裝置都輸出1)，bus 才會為1，
    
    當 bus 由 LOW 變成 HIGH，結束紀錄，兩個時間差即為 low-time 的值

  - `high-time` = master中，拉高時間最短的贏

    當 bus 由 LOW 變成 HIGH，就會開始紀錄 bus 為 HIGH 的時間，
    根據 Wired-AND 的特性，所有的裝置都輸出1，bus的電位才是1，

    若有其中一個 master `提早開始對 SCL 進行控制`，使得 SCL 的輸出為 LOW，`並立即改變 bus 電位`，
    當 bus 由 HIGH 變成 LOW，結束紀錄，兩個時間差即為 high-time 的值

  - 圖解 low-time 和 high-time

    <img src="i2c/i2c-clk-sync.png" width=80% height=auto>

  都過上述時脈同步的機制，所有的 master 都會得到`相同的 low-time 和 high-time`，
  並從下一個 SCL 脈波開始，所有人都使用此時間來產生相同速度的 clock

  - 注意，一但`有任何一個 master 將 SCL-bus 拉低為 LOW`，所有的 master 都會進入時脈同步的狀態，
    並開始計數 low-time 和 high-time

- `過程2，SDA的數據仲裁 (bus-arbitration)`

  - 利用 master-SDA 的狀態 和 SDA-bus 狀態 比較
  
    每個 master 會在 SCL=0 時，利用SDA接腳變更bus上的數據，`在 SCL=1 時`，保持SDA的數據不動，讓 slave 可以讀取數據，
    此時 master 也會透過輸入緩衝器，監聽 SDA-bus 的電位狀態

    在 SCL=1時，master 會將 SDA-bus 和自己的SDA狀態進行比較，
    - 若 bus 上沒有其他傳輸在進行，則 `master-SDA == SDA-bus` 的值，
    - 若 `master-SDA != SDA-bus` 的值，表示 bus 上有其他傳輸在進行，
  
      由其他的 master 造成 SDA-bus 和自己SDA(master-SDA)的`狀態不一致`，
      此時，master 就會知道 bus 上有其他傳輸正在進行，且仲裁的結果輸了，才會造成狀態不一致的結果
    
    若`仲裁沒有出結果`或是`仲裁沒有出現輸贏`，master 就挺過此次的仲裁，才會繼續傳送下一個 SDA 的數據

  - 仲裁不會在第一次比對就產生結果，仲裁的輸贏總是發生在master釋放對SDA的控制時 

    master 會在每一個 SCL=1 的時候，檢查 SDA-bus 的狀態，因此`仲裁會在每一次 SCL=1 時發生`，
    但必須是 `master-SDA = 1 / SDA-bus = 0` 或 `master-SDA = 0 / SDA-bus = 1`，兩者的狀態相反時，master 才會得知自己仲裁失敗，
    `master-SDA = 1 / SDA-bus = 1` 或 `master-SDA = 0 / SDA-bus = 0`，狀態是一致的，
    即使傳輸已經發生了衝突，但 master 無法透過比較得知出現差異，因此仲裁就不會有結果
    
    但根據 Wired-AND 的原則，當 master-SDA = 0 時，SDA-bus 的電位就會被拉低，
    因此，`master-SDA = 0 / SDA-bus = 1`的狀況並不會發生，因此仲裁的結果只會出現在 `master-SDA = 1 / SDA-bus = 0`，

    也就是當 master 釋放對 SDA 的控制，不拉低 SDA-bus 的電位，使 SDA-bus 電位維持在1時，
    此時，`SDA-bus 的電位由其他的 i2c 裝置決定`，因此，SDA-bus 的狀態才會產生差異，
    也往往是出現仲裁出現結果的地方

  - 仲裁輸了之後的處理方式
    
    輸掉仲裁的 master，應該關閉 SDA 的驅動電路，停止在繼續傳送數據

  - 圖解master衝突的仲裁過程

    <img src="i2c/i2c-bus-arbitration.png" width=80% height=auto>

- 仲裁在未出現結果前，會在每一個 SCL=1 時，對 master-SDA 和 SDA-bus 進行比較，依序會比較

  1. 比較地址，低位址 贏 高位址，因為低位址在高位為0
  2. 比較寫/讀位，寫操作 贏 讀操作，因為寫操作為 0，讀操作為1
  3. 比較晶片指令，0 優先 或 數值小的優先
  4. 比較資料，0 優先 或 數值小的優先

## I2C 的保留字節
- 並非所有的 i2c-slave-address 都可以拿來做為定址使用，
  部分 address 的值為保留為特殊用途的值

- 保留字節
  | address        | RW-bit | meaning                 |
  | -------------- | ------ | ----------------------- |
  | 0000_000 (0x0) | 0      | 呼叫所有 i2c-devices    |
  | 0000_000 (0x0) | 1      | START byte              |
  | 0000_001 (0x1) | X      | CBUS address            |
  | 0000_010 (0x2) | X      | reserved                |
  | 0000_011 (0x3) | X      | reserved                |
  | 0000_1XX       | X      | hs-mode master code     |
  | 1111_1XX       | 1      | device ID               |
  | 1111_0XX (0x0) | X      | 10-bit slave addressing |

## I2C 的缺點
- i2c 沒有校驗位，無法驗證數據的正確性，

- 雖然有 ACK 的機制，但 i2c 容易受雜訊干擾，雖然有收到 Ack，但有可能是雜訊引起的誤動作

- i2c 的接線長度，會增加電路的寄生電容，導致時間常數增加 (TimeConstance = R * C)，
  時間常數的增加，代表[需要更長的時間將 LOW 轉換為 HIGH 的狀態](上拉電阻的計算)，
  也代表工作頻率或 SlewRate 的下降

## Ref
- [i2c協議研讀](https://blog.csdn.net/yinuoheqian123/category_9222116.html)

- [i2c SPEC @ NXP](https://www.nxp.com/docs/en/user-guide/UM10204.pdf)

- [i2c on Wiki](https://en.wikipedia.org/wiki/I%C2%B2C)

- [i2c基礎 @ 成大資工](http://wiki.csie.ncku.edu.tw/embedded/I2C)

- [i2c timing SPEC](https://www.analog.com/en/technical-articles/i2c-timing-definition-and-specification-guide-part-2.html)

- [I2C通信協議詳解+實務分析，以 stm32 為例](https://blog.csdn.net/Hello_STM32/article/details/111086472?spm=1001.2101.3001.6650.13&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-13-111086472-blog-105737432.t0_layer_eslanding_s&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-13-111086472-blog-105737432.t0_layer_eslanding_s&utm_relevant_index=21)

- 上升電阻的計算
  - [上升電阻的計算](https://www.ti.com/lit/an/slva689/slva689.pdf)

  - [I2C Bus 提昇電阻之計算](https://magicjackting.pixnet.net/blog/post/180304702)

- Slew Rate Control
  - [How to debug i2c through waveform analysis](https://www.ti.com/lit/an/slyt770/slyt770.pdf?ts=1658498321878&ref_url=https%253A%252F%252Fwww.google.com%252F)

  - [Improving EMI with true slew rate control](https://training.ti.com/improving-emi-true-slew-rate-control)

  - [Edge-rate control speeds circuits](https://www.edn.com/edge-rate-control-speeds-circuits/)

  - [Crosstalk](https://www.oldfriend.url.tw/SI_PI/ansys_ch_crosstalk.html)
