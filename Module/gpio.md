## GPIO 基礎
- gpio 對輸入的要求

- gpio 對輸出的要求

- gpio 的類型
  - push-pull
  - open-drain(CMOS) 或 open-collector(TTL)，例如，i2c

## push-pull GPIO
- 適用於一個輸出對一個輸入，或一個輸出對多個輸入的的點對點傳輸，
- 一個輸出推動多個輸入，最大的限制是，同時只能有一個輸出

- 優缺點
  - 優點，適合高速，可以達到 20MHZ
  - 缺點，若一對多的傳輸，就需要額外的接腳做 chip-select

## open-drain GPIO
- 把多個輸出直接接在一起，變成 WIRE-AND 的效果
- 適合訊號容易發生碰撞的協定

- 優缺點
  - 缺點，適合低速，i2c 的高速模式為 3.4Mbps
  - 優點，需要的接腳少

## GPIO 的施密特觸發輸入模式
- 使用場景

  施密特觸發電路（ 簡稱）是一種波形整形電路，用於將任何輸入的波形整形為方波，
  防止噪聲幹擾電路的正常工作，常用於 遙控接收線路，傳感器輸入電路

  在一些單片機，引腳的輸入模式若標記為為施密特觸發輸入(Schmidt-triggerInput)模式，
  代表任何波形的信號輸入該接腳後，都會經過施密特觸發器整形為方波或脈波後，才輸入到MCU內部的暫存器
  
- 施密特觸發器 == 具有回授功能的比較器

  <font color=blue>比較器的問題</font>

  雖然比較器也可以將波形整形為脈波輸出，但輸入電壓在臨界電壓處遇到雜訊時，
  
  因為雜訊的存在造成電壓頻繁的轉換，造成波形的不穩定

  <img src="gpio/comparators-issue.png" width=50% height=auto>

  透過在比較器中添加回授後，形成施密特觸發器

  <img src="gpio/schmidt-circuit.png" width=50% height=auto>

  施密特觸發器有兩個參考電壓，
  - 輸入 > 高參考電壓，輸出低電壓
  - 輸入 < 低參考電壓，輸出高電壓
  - 高參考電壓 > 輸入 > 低參考電壓，輸出沒有變化

  <img src="gpio/schmidt-rule.png" width=50% height=auto>

  高參考電壓的計算

  <img src="gpio/high-ref.png" width=50% height=auto>

  低參考電壓的計算

  <img src="gpio/low-ref.png" width=50% height=auto>

  因為雜訊的干擾都是小幅度的變化(比參考電壓小)，
  由於高低參考電壓的存在，使得施密特觸發器具有抗干擾的能力，
  在兩個參考電壓之間的雜訊會被抑制

## Ref
- [施密特觸發器的工作原理](https://www.youtube.com/watch?v=fueir0v8-jo)