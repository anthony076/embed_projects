## 四種傳輸模式
- 單工，線路上的訊號只能做單向傳送
- 半雙工，線路上的訊號可以雙向傳送 , 但是不能同時傳送
- 全雙工，線路上的訊號可以同時雙向傳送
- 同步，傳送端和接收端共用同一個CLOCK

## 串列傳輸的佈局方式 (topology)
- `Bus-Topology 總線連接`

  <img src="communication/bus-topology.png" width=40% height=auto>

  - 沒有主從的區別，所有的 device 都共用 bus 總線
  - device 可直接呼叫要通訊的對象，或利用廣播的方式，與所有成員進行通訊
  - 若有 device 離線，其他成員仍然可以進行通訊
  - 例如，CAN 

- `Star-Topology 衛星連接`

  <img src="communication/star-topology.png" width=40% height=auto>

  - 只有一個 Master，其餘所有的 device 都是 Slave
  - 只有 Master 可以與所有的 Slave 通訊，
  - 若 Master 離線，所有的通訊都會中斷

- `Peer-to-Peer Topology 點對點連接`

  <img src="communication/peer2peer-topology.png" width=40% height=auto>

  - 只有兩個 device 直接連接並通訊，沒有 Master/Slave 的區分
  - 例如，UART
  
## 非同步串列傳輸
- `非同步傳輸`，傳輸的兩端`都有自己的時脈產生電路`，產生相同的傳輸速率
  
  非同步傳輸允許 Master/Slave 使用自己的CLK，透過自己的CLK，調製出相近的傳輸速率(baudrate)，
  使用相同的傳輸速度，可以確定每個 bit 的傳輸時間一致

  雖然使用相同的傳輸速度，但不同時脈模組產生的`實際傳輸速度是有差異的`，
  在不使用 start/stop 的狀況下，此差異就有可能會造成數據傳輸的錯誤，例如，

  <img src="communication/why-need-start-stop-bit.png" width=80% height=auto>

  因此，非同步傳輸需要`Start位`和`Stop位`給接收端，
  透過標記位的解析，即使有些微的時間差，也可以利用Start位/Stop位找到數據的正確位置 
  
  例如，[UART](uart.md)、CAN

- 非同步串列傳輸相關協定
  - UART 

    不適合長距離傳輸，常用單晶片外設之間的短距離通訊  
    訊號會受長度增加逐漸遞減，長距離也會使得訊號受周圍的高頻訊號干擾

  - RS232 / RS485
    - 長距離版本的非同步傳輸，具有負責交握的訊號線
    - 定義 0/1 的方式與 uart 不同，利用大電壓差抑制雜訊
      - 0 = +3v - +25v
      - 1 = -3v - -25v
    - 需要搭配電位轉換，才能將RS232的訊號給IC使用

  - CAN

## 同步串列傳輸
- `同步傳輸`，接收端沒有自己的時脈產生電路，Slave使用Master的CLK
  
  因為使用相同的 CLK 源，因此沒有同步的問題，

  例如，[SPI](MSSP_spi.md) 和 [I2C](MSSP_i2c.md)

## 非同步傳輸 和 同步傳輸的比較
- 同步傳輸，不需要使用 Start位/Stop位，改用相同的CLK進行同步
- 同步傳輸，不需要指定傳輸速度
- 同步傳輸，需要預先知道接收數據的長度，才能安排足夠數量的 CLK 供 Slave 傳輸數據

  非同步傳輸可以傳輸任意長度的數據，直到收到 Stop位
  
## 參考
- [RS232 UART 的差別](https://www.strongpilab.com/rs232-uart-difference/)

